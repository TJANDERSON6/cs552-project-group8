/// Group 8 -- Tyler J. Anderson, Ryuki Koda
/// CS552 Spring23
///
/// WISC-SP23 ISA ROL Test #1
///
/// Test to see if all allowable rotate values can be
/// generate the correct rotation.
///
/// Description:
/// 1. Access an array of cases in memory
/// 2. Rotate them 1 to 15 bits
/// 3. Compare to correct result in memory
/// 4. Enter failure label on failure, success
///    label on success
/// 5. Write Success or error code to memory. Returns
///    0 on success, 1 on error to addr 0x01ae (may
///    change depending on memory type)
/// 6. Halt processor

// Initial reg values
.reg_set:
	lbi	r0, 0		// r0 = 0
	lbi	r1, 0		// r1 = 0
	lbi	r2, 0		// r2 = 0
	lbi	r3, 0		// r3 = 0
	lbi	r4, 0		// r4 = 0
	lbi	r5, 0		// r5 = 0
	lbi	r6, 0		// r6 = 0
	lbi	r7, 0		// r7 = 0
.test1_start:
	lbi	r0, U.rot_array
	slbi	r0, L.rot_array		// r0 = &rot_array[0]
	lbi	r6, U.rot_array_end
	slbi	r6, L.rot_array_end	// r6 = &rot_array_end
	lbi	r4, U.chk_array
	lbi	r4, L.chk_array		// r4 = &chk_array[0]
.loop_data:
	seq	r5, r0, r6	// If at end of array,
	bnez	r5, .end_data	// branch to end of program
	ld	r1, r0, 0	// r1 = .rot_array[i]
	lbi	r3, 16		// r3 = 16 (for shift)
.loop_shf:
	beqz	r3, .end_shf	// Branch if all shifts complete
	addi	r3, r3, -1	// Deinc. shift (R3 = R3 -1)
	rol	r2, r1, r3	// Rotate bits
	jal	.chkrslt	// Check
	addi	r4, r4, 2	// Increment chk ptr
	j	.loop_shf	// Jump to next shift
.end_shf:
	addi	r0, r0, 2	// Increment ptr
	j	.loop_data	// Loop for .rot_array[i+1]
.end_data:
	j	.exit_success	// Store exit code and halt

	// Compare result to mem, jump to failure if NE
	// args: rslt = r2, &chk = r4
.chkrslt:
	ld	r5, r4, 0		// Load chk value
	seq	r5, r5, r2		// Is rslt == chk?
	beqz	r5, .exit_failure	// If not, test fails
	jr	r7, 0			// Else, return

/// Handle different test completed scenarios and halt on exit
.exit_success:
	lbi	r0, 0			// r0 = 0
	j	.finished
.exit_failure:
	lbi	r0, 1			// r0 = 1
.finished:
	lbi	r1, U.exit_status
	slbi	r1, L.exit_status	// r1 = &.exit_status
	st	r0, r1, 0		// .exit = 0 if success, else fail
	halt

	// Our values to test (Taken from system urandom device)
.rot_array:
	data	0x8489
	data	0xdbbe
	data	0x38d5
	data	0x6641
	data	0xe461
	data	0xb8d5
	data	0x6303
	data	0xdb3f
	data	0x6956
	data	0xca33

.rot_array_end:

	// Our golden values (Generated from golden.c)
.chk_array:
	data	0xc244
	data	0x6122
	data	0x3091
	data	0x9848
	data	0x4c24
	data	0x2612
	data	0x1309
	data	0x8984
	data	0x44c2
	data	0x2261
	data	0x9130
	data	0x4898
	data	0x244c
	data	0x1226
	data	0x913
	data	0x8489
	data	0x6ddf
	data	0xb6ef
	data	0xdb77
	data	0xedbb
	data	0xf6dd
	data	0xfb6e
	data	0x7db7
	data	0xbedb
	data	0xdf6d
	data	0xefb6
	data	0x77db
	data	0xbbed
	data	0xddf6
	data	0x6efb
	data	0xb77d
	data	0xdbbe
	data	0x9c6a
	data	0x4e35
	data	0xa71a
	data	0x538d
	data	0xa9c6
	data	0x54e3
	data	0xaa71
	data	0xd538
	data	0x6a9c
	data	0x354e
	data	0x1aa7
	data	0x8d53
	data	0xc6a9
	data	0xe354
	data	0x71aa
	data	0x38d5
	data	0xb320
	data	0x5990
	data	0x2cc8
	data	0x1664
	data	0xb32
	data	0x599
	data	0x82cc
	data	0x4166
	data	0x20b3
	data	0x9059
	data	0xc82c
	data	0x6416
	data	0x320b
	data	0x9905
	data	0xcc82
	data	0x6641
	data	0xf230
	data	0x7918
	data	0x3c8c
	data	0x1e46
	data	0xf23
	data	0x8791
	data	0xc3c8
	data	0x61e4
	data	0x30f2
	data	0x1879
	data	0x8c3c
	data	0x461e
	data	0x230f
	data	0x9187
	data	0xc8c3
	data	0xe461
	data	0xdc6a
	data	0x6e35
	data	0xb71a
	data	0x5b8d
	data	0xadc6
	data	0x56e3
	data	0xab71
	data	0xd5b8
	data	0x6adc
	data	0x356e
	data	0x1ab7
	data	0x8d5b
	data	0xc6ad
	data	0xe356
	data	0x71ab
	data	0xb8d5
	data	0xb181
	data	0xd8c0
	data	0x6c60
	data	0x3630
	data	0x1b18
	data	0xd8c
	data	0x6c6
	data	0x363
	data	0x81b1
	data	0xc0d8
	data	0x606c
	data	0x3036
	data	0x181b
	data	0x8c0d
	data	0xc606
	data	0x6303
	data	0xed9f
	data	0xf6cf
	data	0xfb67
	data	0xfdb3
	data	0xfed9
	data	0xff6c
	data	0x7fb6
	data	0x3fdb
	data	0x9fed
	data	0xcff6
	data	0x67fb
	data	0xb3fd
	data	0xd9fe
	data	0x6cff
	data	0xb67f
	data	0xdb3f
	data	0x34ab
	data	0x9a55
	data	0xcd2a
	data	0x6695
	data	0xb34a
	data	0x59a5
	data	0xacd2
	data	0x5669
	data	0xab34
	data	0x559a
	data	0x2acd
	data	0x9566
	data	0x4ab3
	data	0xa559
	data	0xd2ac
	data	0x6956
	data	0xe519
	data	0xf28c
	data	0x7946
	data	0x3ca3
	data	0x9e51
	data	0xcf28
	data	0x6794
	data	0x33ca
	data	0x19e5
	data	0x8cf2
	data	0x4679
	data	0xa33c
	data	0x519e
	data	0x28cf
	data	0x9467
	data	0xca33

.chk_array_end:

	// Some empty memory for offset
	halt
	halt

	// This is our program result
.exit_status:
	halt

	// More empty words
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
