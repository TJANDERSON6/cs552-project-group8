onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group System /proc_hier_pbench/DUT/p0/clk
add wave -noupdate -expand -group System /proc_hier_pbench/DUT/p0/rst
add wave -noupdate -expand -group System /proc_hier_pbench/DUT/p0/err
add wave -noupdate -expand -group Program /proc_hier_pbench/DUT/p0/fetchBlock/PC
add wave -noupdate -expand -group Program /proc_hier_pbench/DUT/p0/HD_Stall
add wave -noupdate -expand -group Program /proc_hier_pbench/DUT/p0/HD_Flush
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoFetchHalted
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoFetchPCAdded
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoFetchJumpSrc
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoFetchJump
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoHazardMemWrite
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoHazardRd
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/WbtoDecodeRd
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/WbtoDecodeWriteData
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/WbtoDecodeRegWrite
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoHazardRs
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoHazardRt
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoHazardALUSrc
add wave -noupdate -group Forwarding /proc_hier_pbench/DUT/p0/DecodetoHazardBranch
add wave -noupdate -group IF/ID /proc_hier_pbench/DUT/p0/IFID_Started
add wave -noupdate -group IF/ID /proc_hier_pbench/DUT/p0/IFID_Halted
add wave -noupdate -group IF/ID /proc_hier_pbench/DUT/p0/IFID_PCNext
add wave -noupdate -group IF/ID /proc_hier_pbench/DUT/p0/IFID_Instruction
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_Halted
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_RegWrite
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_Branch
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_JumpLink
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_JumpSrc
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_MemtoReg
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_MemRead
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_MemWrite
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_PCNext
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_ALUExt
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_ALUSRC
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_PCSrc
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_ALUOp
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_PCAdded
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_Func
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_ReadData1
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_ReadData2
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_Jump
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_ExtImm
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_Rs
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_Rt
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_Instruction
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_Rd
add wave -noupdate -group ID/EX /proc_hier_pbench/DUT/p0/IDEX_ZeroExt
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_Halted
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_RegWrite
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_JumpLink
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_JumpSrc
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_MemtoReg
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_MemRead
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_MemWrite
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_PCNext
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_ExecRslt
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_Rs
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_Rt
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_ReadData2
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_Rd
add wave -noupdate -group EX/MEM /proc_hier_pbench/DUT/p0/EXMEM_Instruction
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_Halted
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_RegWrite
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_JumpLink
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_JumpSrc
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_MemtoReg
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_MemRead
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_MemWrite
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_PCNext
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_ReadData
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_ExecRslt
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_Rs
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_Rt
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_Rd
add wave -noupdate -group MEM/WB /proc_hier_pbench/DUT/p0/MEMWB_Instruction
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/wbErr
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/memoryErr
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/executeErr
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/decodeErr
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/fetchErr
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/modErr
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/IFID_err
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/IDEX_err
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/EXMEM_err
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/MEMWB_err
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/boot_err
add wave -noupdate -group Error /proc_hier_pbench/DUT/p0/pRegErr
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 256
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1764 ns}
