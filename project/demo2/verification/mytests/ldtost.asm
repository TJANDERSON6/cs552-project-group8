// CS552 Spring '23
// Project Group 8
// Tyler J. Anderson and Ryuki Koda
//
// ldtost.asm
//
// Test MEM/MEM forwarding with a store after a load

.test_start:
	lbi	r0, U.data
	nop
	nop
	nop
	nop
	nop
	slbi	r0, L.data
	nop
	nop
	nop
	nop
	nop
	ld	r1, r0, 0		// r1 = 0x7034
	st	r1, r0, 8		// A[4] = 0x7034
	nop
	nop
	nop
	nop
	nop
	ld	r1, r0, 2		// r1 = 0x5234
	andni	r3, r3, 0		// r3 = 0 (Check puting op in b/w)
	st	r1, r0, 10		// A[5] = 0x5234
	nop
	nop
	nop
	nop
	nop
	ld	r1, r0, 4		// r1 = 0x1034
	st	r1, r0, 4		// Write to same address
	halt				// All done

.data:
	data	0x7034
	data	0x5234
	data	0x1034
	data	0x0001
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt
