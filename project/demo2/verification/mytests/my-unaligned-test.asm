// CS552 Spring 2023
// Group 8
// Tyler J. Anderson and Ryuki Koda
//
// my_unaligned-test.asm
// Test unaligned memory access handling
	
	.test_start:
	lbi	r0, 0x41
	lbi	r1, 0x41
	lbi	r2, 0x43
	lbi	r3, 0x41
	lbi	r4, 0x22
	slbi	r0, 0xf2	// r0 = 0x41f2
	slbi	r1, 0xcb	// r1 = 0x41cb
	slbi	r2, 0x11	// r2 = 0x4311
	slbi	r3, 0x23	// r3 = 0x4123
	slbi	r4, 0x22	// r4 = 0x2222
	.aligned:
	st	r4, r0, 0	// Test aligned access
	st	r4, r0, 2
	st	r4, r0, 4
	st	r4, r0, 6
	.unaligned:
	st	r4, r1, 0
	st	r4, r2, 0
	st	r4, r3, 0
	st	r4, r0, 1
	st	r4, r0, 3
	st	r4, r0, 5
