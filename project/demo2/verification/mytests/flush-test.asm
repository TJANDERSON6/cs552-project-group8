// CS552 Spring 2023
// Group 8
// Tyler J. Anderson and Ryuki Koda
//
// flush-test.asm
//
// Test that instructions flush properly on branch taken and
// do not flush on branch-not-taken

.test_start:
	lbi	r1, 1		// r1 = 1
	andni	r0, r2, 0	// Clear Register 0
	lbi	r2, 0xa		// r2 = 10
	lbi	r3, 0x5		// Value to rotate around
	nop
	nop
	nop
	nop
	nop
.loop:
	beqz	r1, .finish	// Loop while r0 < r2
	addi	r0, r0, 1	// r0 = r0 + 1
	roli	r3, r3, 2	// r3 = r3 <<rot 2
	nop
	nop
	nop
	nop
	nop
	slt	r1, r0, r2	// r0 < 10 ?
	nop
	nop
	nop
	nop
	nop
	rori	r4, r3, 1	// r4 = r3 >>rot 1
	j	.loop		// Return to head of loop
	addi	r0, r0, 0x14	// Shouldn't ever execute
	halt
	halt
	halt
	halt
	halt

.finish:
	slli	r3, r3, 4	// r3 = r3 << 4
	subi	r4, r4, 2	// r4 = r4 - 2
	halt			// End of test
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
