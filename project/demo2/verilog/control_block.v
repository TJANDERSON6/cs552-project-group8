/*
 * CONTROL BLOCK
 * CS552 Sp23 Group 8
 *
 * Single Cycle Processor control
 * 
 * Most instructions can be converted to control signals by
 * a logic combination of format type and their functional
 * class. Troublesome exceptions are as follows:
 * 
 * - SLBI
 * - LBI
 */

module control_block(
		     // Inputs
		     Clk, Rst, Instruction, InHalted,

		     // Outputs
		     RegDst, Func, ALUSRC, ZeroExt, MemtoReg,
		     ImmLen, RegWrite, JumpSrc, MemRead,
		     Jump, MemWrite, JumpLink, Branch, ALUExt,
		     ALUOp, OutHalted, Err
		     );

   input wire	     Clk;
   input wire	     Rst;
   input wire [15:0] Instruction;
   input wire	     InHalted;

   output wire [1:0] RegDst;
   output wire [1:0] Func;
   output wire	     ALUSRC;
   output wire	     ZeroExt;
   output wire	     MemtoReg;
   output wire [1:0] ImmLen;
   output wire	     RegWrite;
   output wire	     JumpSrc;
   output wire	     MemRead;
   output wire	     MemWrite;
   output wire	     JumpLink;
   output wire [1:0] ALUExt;
   output wire [1:0] ALUOp;
   output wire	     OutHalted;

   // Outputs that also serve as functional classes
   output wire	     Jump;
   output wire	     Branch;
   output wire	     Err;

	opcode_decoder opcode_decoder_inst (
					    .instruction(Instruction),
					    .RegDst(RegDst),
					    .ALUSrc(ALUSRC),
					    .MemtoReg(MemtoReg),
					    .RegWrite(RegWrite),
					    .MemRead(MemRead),
					    .MemWrite(MemWrite),
					    .Branch(Branch),
					    .ALUOp(ALUOp),
					    .PCEn(),
					    .Func(Func),
					    .ZeroExt(ZeroExt),
					    .ImmLen(ImmLen),
					    .JumpSrc(JumpSrc),
					    .Jump(Jump),
					    .JumpLink(JumpLink),
					    .ALUExt(ALUExt),
					    .Halted(),
					    .Dump(),
					    .err(Err)
					    );

   assign OutHalted = Rst ? 1'b0 : (~|Instruction[15:11]) | InHalted;

   // Halted will latch when HALT instruction is decoded and can only
   // be cleared by resetting the processor
   halt_latch halt_latch_inst (
			       .Clk(Clk),
			       .Rst(Rst),
			       .Opcode(Instruction[15:11]),
			       .Halted()
			       );

endmodule // control_block
