/*
   CS/ECE 552 Spring '23
  
   Filename        : execute.v
   Description     : This is the overall module for the execute stage of the processor.
*/
`default_nettype none
module execute (
		// inputs
		ALUExt, ALUSRC, ALUOp, PCAdded, Func, ReadData1, ReadData2, ExtImm, Rs, Rt, Instruction, ZeroExt,
		IDEX_MemWrite, IDEX_MemRead,
		IDEX_Rs, IDEX_Rt, EXMEM_RegWrite, MEMWB_RegWrite, EXMEM_Rd, MEMWB_Rd, EXMEM_PCAdded,
		EXMEM_ExecRslt, MEMWB_ExecRslt, WbtoDecodeWriteData,
		EXMEM_Rs, MEMWB_Rs, MEMWB_MemWrite,
		Rst, Clk, JumpLink, JumpSrc, EXMEM_JumpLink,
		EXMEM_MemWrite,
		// outputs
		ExecRslt, ExecutetoHazardUnaligned, err
		);

	input wire [1:0] ALUExt;
	input wire	 ALUSRC;
	input wire [1:0] ALUOp;
	input wire [15:0] PCAdded;
	input wire [1:0]  Func;
	input wire [15:0] ReadData1;
	input wire [15:0] ReadData2;
	input wire [15:0] ExtImm;
	input wire [2:0]  Rs;
	input wire [2:0]  Rt;
	input wire [15:0] Instruction;
	input wire	  ZeroExt;
	input wire	  IDEX_MemWrite;
	input wire	  IDEX_MemRead;
	input wire [2:0]  IDEX_Rs;
	input wire [2:0]  IDEX_Rt;
	input wire	  EXMEM_RegWrite;
	input wire [15:0] EXMEM_PCAdded;
	input wire	  MEMWB_RegWrite;
	input wire [2:0]  EXMEM_Rd;
	input wire [2:0]  MEMWB_Rd;
	input wire [15:0] EXMEM_ExecRslt;
	input wire [15:0] MEMWB_ExecRslt;
	input wire JumpLink;
	input wire	  Rst;
	input wire	  Clk;
	input wire 	  EXMEM_MemWrite;
	input wire [15:0] WbtoDecodeWriteData;
	input wire	  ExecutetoHazardUnaligned;
	input wire [2:0]  EXMEM_Rs;
	input wire [2:0]  MEMWB_Rs;
	input wire	  MEMWB_MemWrite;
	input wire	  JumpSrc;
	input wire	  EXMEM_JumpLink;

	output wire [15:0] ExecRslt;
	output wire	   err;

	wire [15:0]	   ALUInA;
	wire [15:0] ALUInB;
	wire [14:0] carry; 
	wire zero; 
	wire cOut;
	wire [15:0] ALURslt; 
	wire [15:0] SetRslt;
	wire [15:0] BtrRslt; 
	wire [15:0] ShiftLoadRslt;
	wire A;
        wire B;

	// Combine errors from module
	assign err = 1'b0;

	// If accessing memory and address is bad, forward alarm
	assign ExecutetoHazardUnaligned = (ExecRslt[0] == 1'b1) &
					  (IDEX_MemWrite | IDEX_MemRead);

	// configure ALUInA and ALUInB
	// assign ALUInA = ReadData1; // TODO: Add dummy (or real) forwarding module
	// assign ALUInB = ALUSRC ? ExtImm : ReadData2; 

	RsForwarding RsForwarding_inst (
    .IDEX_Rs(IDEX_Rs),
    .EXMEM_RegWrite(EXMEM_RegWrite),
    .MEMWB_RegWrite(MEMWB_RegWrite),
    .EXMEM_MemWrite(EXMEM_MemWrite),
    .MEMWB_MemWrite(MEMWB_MemWrite),
    .EXMEM_Rd(EXMEM_Rd),
    .EXMEM_PCAdded(EXMEM_PCAdded),
    .IDEX_JumpSrc(JumpSrc),
    .EXMEM_JumpLink(EXMEM_JumpLink),
    .MEMWB_Rd(MEMWB_Rd),
    .EXMEM_Rs(EXMEM_Rs),
    .MEMWB_Rs(MEMWB_Rs),
    .EXMEM_ExecRslt(EXMEM_ExecRslt),
    .JumpLink(JumpLink), 
    .WbtoDecodeWriteData(WbtoDecodeWriteData),
    .ReadData1(ReadData1),
    .ALUInA(ALUInA)
);

	RtForwarding RtForwarding_inst (
    .IDEX_Rt(IDEX_Rt),
    .EXMEM_RegWrite(EXMEM_RegWrite),
    .MEMWB_RegWrite(MEMWB_RegWrite),
    .EXMEM_Rd(EXMEM_Rd),
    .MEMWB_Rd(MEMWB_Rd),
    .EXMEM_Rs(EXMEM_Rs),
    .MEMWB_Rs(MEMWB_Rs),
    .EXMEM_JumpLink(EXMEM_JumpLink),
    .EXMEM_PCAdded(EXMEM_PCAdded),
    .EXMEM_ExecRslt(EXMEM_ExecRslt),
    .MEMWB_ExecRslt(MEMWB_ExecRslt),
    .ReadData2(ReadData2),
    .ExtImm(ExtImm),
    .ALUSRC(ALUSRC),
    .WbtoDecodeWriteData(WbtoDecodeWriteData),
    .EXMEM_MemWrite(EXMEM_MemWrite),
    .MEMWB_MemWrite(MEMWB_MemWrite),
    .ALUInB(ALUInB)
); 
	

	// configure ALU block
	alu ALU(.ALUInA(ALUInA), .ALUInB(ALUInB), .ALUOp(ALUOp), .A(A), .B(B), .Func(Func), .zero(zero), .cOut(cOut), .ALURslt(ALURslt));
	
	// configure setcond block
	setcond SetCond(.zero(zero), .MSB(ALURslt[15]), .cOut(cOut), .A(A), .B(B), .control(Instruction[12:11]), .SetRslt(SetRslt)); 

	// configure bitreverse block
	bitreverse BitReverse(.ALUInA(ALUInA), .BtrRslt(BtrRslt));

	// configure shiftload block
	shiftload ShiftLoad(.ZeroExt(ZeroExt), .ALUInA(ALUInA), .ExtImm(ExtImm), .ShiftLoadRslt(ShiftLoadRslt));
	
	// configure executed result
	assign ExecRslt = ALUExt[1] ? (ALUExt[0] ? ShiftLoadRslt : BtrRslt) : (ALUExt[0] ? SetRslt : ALURslt); 

endmodule
`default_nettype wire
