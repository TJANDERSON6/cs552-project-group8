module opcode_decoder(
		      // inputs
		      instruction,

		      // outputs
		      RegDst,
		      ALUSrc,
		      MemtoReg,
		      RegWrite,
		      MemRead,
		      MemWrite,
		      Branch,
		      ALUOp,
		      PCEn,
		      Func,
		      ZeroExt,
		      ImmLen,
		      JumpSrc,
		      Jump,
		      JumpLink,
		      ALUExt,
		      Halted,
		      Dump,
		      err
		      );

	input wire [15:0] instruction;

	output reg [1:0]  RegDst;
	output reg	  ALUSrc;
	output reg	  MemtoReg;
	output reg	  RegWrite;
	output reg	  MemRead;
	output reg	  MemWrite;
	output reg	  Branch;
	output reg [1:0]  ALUOp;
	output reg	  PCEn;
	output reg [1:0]  Func;
	output reg	  ZeroExt;
	output reg [1:0]  ImmLen;
	output reg	  JumpSrc;
	output reg	  Jump;
	output reg	  JumpLink;
	output reg [1:0]  ALUExt;
	output reg	  Halted;
	output reg	  Dump;
	output reg	  err;

	wire [4:0]	  opcode;

	always @(opcode, instruction[1:0])
	  begin
		  casex(opcode)
		    5'h0:
		      // HALT
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h1;
			      Dump = 1'h1;
			      err = 1'h0;
		      end
		    5'h1:
		      // NOP
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h8:
		      // ADDI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h9:
		      // SUBI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h1;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'ha:
		      // XORI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h1;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h1;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'hb:
		      // ANDNI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h1;
			      Func[0] = 1'h1;
			      ZeroExt = 1'h1;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h14:
		      // ROLI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h1;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h15:
		      // SLLI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h16:
		      // RORI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h1;
			      Func[0] = 1'h1;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h17:
		      // SRLI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h1;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h10:
		      // st
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h1;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h11:
		      // ld
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h1;
			      RegWrite = 1'h1;
			      MemRead = 1'h1;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h13:
		      // stu
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h1;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h19:
		      // BTR
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h1;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1b:
		      // ADD, SUB, XOR, ANDN
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = instruction[1];
			      Func[0] = instruction[0];
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1a:
		      // SLL, SRL, ROL, ROR
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = instruction[1];
			      Func[0] = instruction[0];
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1c:
		      // SEQ
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1d:
		      // SLT
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1e:
		      // SLE
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1f:
		      // SCO
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'hc:
		      // BEQZ
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'hd:
		      // BNEZ
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'he:
		      // BLTZ
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'hf:
		      // BGEZ
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h18:
		      // lbi
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h1;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h12:
		      // slbi
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h1;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h1;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h4:
		      // J
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h1;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h1;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h5:
		      // JR
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h1;
			      Jump = 1'h1;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h6:
		      // JAL
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h1;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h1;
			      JumpLink = 1'h1;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h7:
		      // JALR
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h1;
			      Jump = 1'h1;
			      JumpLink = 1'h1;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h2:
		      // SIIC
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h3:
		      // NOP/RTI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    default:
		      begin
			      // Illegal operation
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end // case: default
		  endcase // casex (opcode)
	  end // always @ (instruction)

	assign opcode = instruction[15:11];

endmodule // opcode_decoder
