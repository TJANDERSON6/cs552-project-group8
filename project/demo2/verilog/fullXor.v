`default_nettype none
module fullXor (InA, InB, o);

    parameter OPERAND_WIDTH = 16;    
       
    input wire  [OPERAND_WIDTH -1:0] InA ; // Input wire operand A
    input wire  [OPERAND_WIDTH -1:0] InB ; // Input wire operand B
    output wire [OPERAND_WIDTH -1:0] o ; // Result of comput wireation

    xor2 xor16[15:0](.out(o), .in1(InA), .in2(InB));
	    

endmodule
`default_nettype wire
