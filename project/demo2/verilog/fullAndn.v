`default_nettype none
module fullAndn (InA, InB, o);

    	parameter OPERAND_WIDTH = 16;    
       
    	input wire  [OPERAND_WIDTH -1:0] InA ; // Input wire operand A
    	input wire  [OPERAND_WIDTH -1:0] InB ; // Input wire operand B
    	output wire [OPERAND_WIDTH -1:0] o ; // Result of comput wireation

	assign o = InA & (~InB);
endmodule
`default_nettype wire
