/* 
	CS/ECE 552 Spring '23

	Filename: RsForwarding .v 
	Description: This is a block for RsForwarding in execute block - (ALUInA).  
*/ 
`default_nettype none
module RsForwarding(
			// inputs
			IDEX_Rs, IDEX_JumpSrc, EXMEM_JumpLink,
			EXMEM_RegWrite, MEMWB_RegWrite, EXMEM_MemWrite,
			EXMEM_Rd, EXMEM_PCAdded, MEMWB_Rd, EXMEM_Rs,
		    	MEMWB_Rs, MEMWB_MemWrite,
			EXMEM_ExecRslt, WbtoDecodeWriteData, ReadData1, 
			JumpLink,	
			// outputs
			ALUInA
		); 

	// input bus
	input wire [2:0] IDEX_Rs;
	input wire	 IDEX_JumpSrc;
	input wire	 EXMEM_JumpLink;
	input wire EXMEM_RegWrite; 
	input wire MEMWB_RegWrite;
	input wire EXMEM_MemWrite;
	input wire MEMWB_MemWrite;
	input wire [15:0] EXMEM_PCAdded;
	input wire JumpLink;

	input wire [2:0] EXMEM_Rd; 
	input wire [2:0] MEMWB_Rd;
	input wire [2:0] EXMEM_Rs;
	input wire [2:0] MEMWB_Rs;

	input wire [15:0] EXMEM_ExecRslt; 
	input wire [15:0] WbtoDecodeWriteData; 
	input wire [15:0] ReadData1; 

	output wire [15:0] ALUInA; 

	wire [15:0]	   ExExRslt;
	wire [15:0]	   MemExRslt;
	wire		   isMemEx;
	wire		   isExEx;

	assign ALUInA = isExEx ? ExExRslt :
			(isMemEx ? MemExRslt : ReadData1);

	assign ExExRslt = EXMEM_JumpLink ? EXMEM_PCAdded : EXMEM_ExecRslt;
	assign MemExRslt = WbtoDecodeWriteData;

	assign isExEx = EXMEM_RegWrite & (
					  ((IDEX_Rs == EXMEM_Rd) & ~EXMEM_MemWrite) |
					  ((IDEX_Rs == EXMEM_Rs) & EXMEM_MemWrite)
					  );
	assign isMemEx = MEMWB_RegWrite & (
					   ((IDEX_Rs == MEMWB_Rd) & ~MEMWB_MemWrite) |
					   ((IDEX_Rs == MEMWB_Rs) & MEMWB_MemWrite)
					   );

endmodule 
`default_nettype wire 
