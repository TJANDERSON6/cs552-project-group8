/*
   CS/ECE 552 Spring '23
  
   Filename        : decode.v
   Description     : This is the module for the overall decode stage of the processor.
*/
`default_nettype none
module decode (
	       // Inputs: Pipeline
	       Halted, PCNext, InInstruction, Flush,
	       // Inputs: Forwarding
	       WbtoDecodeRd, WbtoDecodeWriteData, WbtoDecodeRegWrite,
	       // Inputs: Processor
	       Clk, Rst,

	       // Outputs: Pipeline
	       HaltedOut, RegWrite, Branch, JumpLink, JumpSrc, MemtoReg, MemRead, MemWrite,
	       ALUExt, ALUSRC, PCSrc, ALUOp, PCAdded, Func, ReadData1, ReadData2, Jump,
	       ExtImm, Rs, Rt, Rd, ZeroExt, OutInstruction,
	       // Outputs: Forwarding
	       DecodetoFetchHalted, DecodetoFetchPCAdded, DecodetoFetchJumpSrc, DecodetoFetchJump,
	       DecodetoHazardMemWrite, DecodetoHazardRd, DecodetoHazardRs, DecodetoHazardRt,
	       DecodetoHazardALUSrc, DecodetoHazardBranch,
	       // Outputs: Processor
	       Err
	       );

	input wire Halted;
	input wire [15:0] PCNext;
	input wire [15:0] InInstruction;
	input wire	  Flush;
	input wire [2:0]  WbtoDecodeRd;
	input wire [15:0] WbtoDecodeWriteData;
	input wire	  WbtoDecodeRegWrite;
	input wire	  Clk;
	input wire	  Rst;

	output wire	  HaltedOut;
	output wire	  RegWrite;
	output wire	  Branch;
	output wire	  JumpLink;
	output wire	  JumpSrc;
	output wire	  MemtoReg;
	output wire	  MemRead;
	output wire	  MemWrite;
	output wire [1:0] ALUExt;
	output wire	  ALUSRC;
	output wire	  PCSrc;
	output wire [1:0] ALUOp;
	output wire [15:0] PCAdded;
	output wire [1:0] Func;
	output wire [15:0] ReadData1;
	output wire [15:0] ReadData2;
	output wire	   Jump;
	output wire [15:0] ExtImm;
	output wire [2:0]  Rs;
	output wire [2:0]  Rt;
	output wire [2:0]  Rd;
	output wire	   ZeroExt;
	output wire	   DecodetoFetchHalted;
	output wire [15:0] DecodetoFetchPCAdded;
	output wire	   DecodetoFetchJumpSrc;
	output wire	   DecodetoFetchJump;
	output wire	   DecodetoHazardMemWrite;
	output wire [2:0]  DecodetoHazardRd;
	output wire [2:0]  DecodetoHazardRs;
	output wire [2:0]  DecodetoHazardRt;
	output wire	   DecodetoHazardALUSrc;
	output wire	   DecodetoHazardBranch;
	output wire	   Err;
	output wire [15:0] OutInstruction;

	// additional variables
	wire [2:0] WriteReg; 
	wire [15:0] SignExtend;
	wire [15:0] SignExtend8_16; 
	wire [15:0] SignExtend11_16; 
	wire [15:0] ZeroExtend5_16; 
	wire [15:0] ZeroExtend8_16; 	
	wire [15:0] SignExtendValue; 	// Selected Value from Sign Extension
	wire [15:0] ZeroExtendValue; 	// Selected Value from Zero Extension
	wire [1:0]  RegDst;
	wire [1:0]  ImmLen;
	wire [15:0] Instruction;
	wire	    BrRslt;

	// Error signals (Combine together for this module Err)
	wire	    rf_err;
	wire	    control_err;

	// Errors combined
	assign Err = rf_err | control_err;
	
	// configure write data
	assign Rd = RegDst[1] ? (RegDst[0] ? 3'h7 : Instruction[4:2]) : (RegDst[0] ? Instruction[10:8] : Instruction[7:5]);

        // Register file with 8 registers (r[7:0]) and 3 bit input
	rf_bypass  rf_inst (
			    .read1OutData(ReadData1),
			    .read2OutData(ReadData2),
			    .err(rf_err),
			    .clk(Clk),
			    .rst(Rst),
			    .read1RegSel(Instruction[10:8]),
			    .read2RegSel(Instruction[7:5]),
			    .writeRegSel(WbtoDecodeRd),
			    .writeInData(WbtoDecodeWriteData),
			    .writeEn(WbtoDecodeRegWrite)
			    );
	
	// configure extensions
	assign SignExtend = {{11{Instruction[4]}}, Instruction[4:0]};
	assign SignExtend8_16 = {{8{Instruction[7]}}, Instruction[7:0]};
	assign SignExtend11_16 = {{5{Instruction[10]}}, Instruction[10:0]};
	assign ZeroExtend5_16 = {11'b0, Instruction[4:0]};
	assign ZeroExtend8_16 = {8'b0, Instruction[7:0]};

	assign SignExtendValue = ImmLen[1] ? (ImmLen[0] ?  SignExtendValue : SignExtend11_16) : (ImmLen[0] ? SignExtend8_16 : SignExtend);
	assign ZeroExtendValue = ImmLen[0] ? ZeroExtend8_16 : ZeroExtend5_16; 
	assign ExtImm = ZeroExt ? ZeroExtendValue : SignExtendValue;

	// Control block to feed to all the other stages
	control_block control_block_inst (
					  .Clk(Clk),
					  .Rst(Rst),
					  .Instruction(Instruction),
					  .InHalted(Halted),
					  .RegDst(RegDst),
					  .Func(Func),
					  .ALUSRC(ALUSRC),
					  .ZeroExt(ZeroExt),
					  .MemtoReg(MemtoReg),
					  .ImmLen(ImmLen),
					  .RegWrite(RegWrite),
					  .JumpSrc(JumpSrc),
					  .MemRead(MemRead),
					  .Jump(Jump),
					  .MemWrite(MemWrite),
					  .JumpLink(JumpLink),
					  .Branch(Branch),
					  .ALUExt(ALUExt),
					  .ALUOp(ALUOp),
					  .OutHalted(HaltedOut),
					  .Err(control_err)
					  );

	// Adder for branches and jumps
	cla16b #(
		 .N(16)
		 ) cla16b_inst (
				.sum(PCAdded),
				.cOut(),
				.inA(PCNext),
				.inB(ExtImm),
				.cIn(1'b0)
				);

	// configure brcond block 
	brcond BrCond(.ALUInA(ReadData1), .control(Instruction[12:11]), .BrRslt(BrRslt));
	assign PCSrc = Branch & BrRslt;

	// If flushing, override instruction and decode a NOP (Only happens on B*Z Branch-Taken)
	assign Instruction = Flush ? 16'h0800 : InInstruction;
	assign OutInstruction = Instruction;

	// Assign forwarding signals for jumps and halts
	assign DecodetoFetchHalted = HaltedOut;
	assign DecodetoFetchPCAdded = PCAdded;
	assign DecodetoFetchJumpSrc = JumpSrc;
	assign DecodetoFetchJump = Jump;
	assign DecodetoHazardMemWrite = MemWrite;
	assign DecodetoHazardRd = Rd;
	assign DecodetoHazardRs = Rs;
	assign DecodetoHazardRt = Rt;
	assign DecodetoHazardALUSrc = ALUSRC;
	assign DecodetoHazardBranch = Branch;

	// Record operand reg numbers into pipeline for later forwarding logic
	assign Rs = Instruction[10:8];
	assign Rt = Instruction[7:5];

endmodule
`default_nettype wire
