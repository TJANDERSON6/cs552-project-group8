// Decode Test Bench
// CS552 Sp23
// Group 8

module decode_bench();
	// Golden values
	reg [1:0] RegDst;
	reg	  ALUSrc;
	reg	  MemtoReg;
	reg	  RegWrite;
	reg	  MemRead;
	reg	  MemWrite;
	reg	  Branch;
	reg [1:0] ALUOp;
	reg	  PCEn;
	reg [1:0] Func;
	reg	  ZeroExt;
	reg [1:0] ImmLen;
	reg	  JumpSrc;
	reg	  Jump;
	reg	  JumpLink;
	reg [1:0] ALUExt;
	reg	  Halted;
	reg	  Dump;
	reg	  err;

	wire [23:0] gbus; // For comparison

	// Received from decode module
	wire [1:0] RegDst_d;
	wire	   ALUSrc_d;
	wire	   MemtoReg_d;
	wire	   RegWrite_d;
	wire	   MemRead_d;
	wire	   MemWrite_d;
	wire	   Branch_d;
	wire [1:0] ALUOp_d;
	wire	   PCEn_d;
	wire [1:0] Func_d;
	wire	   ZeroExt_d;
	wire [1:0] ImmLen_d;
	wire	   JumpSrc_d;
	wire	   Jump_d;
	wire	   JumpLink_d;
	wire [1:0] ALUExt_d;
	wire	   Halted_d;
	wire	   Dump_d;
	wire	   err_d;
	wire [15:0] ReadData1_d;
	wire [15:0] ReadData2_d;
	wire [15:0] ExtImm_d;

	wire [23:0] dbus; // For comparison

	// Pass to decode
	reg [15:0] instruction, WriteData;
	wire [4:0] opcode;

	wire	   baderror;
	reg	   fail;
	reg	   testfail;
	wire [23:0] cdiff;

	wire	   clk;
	wire	   rst;
	wire	   crst;
	reg	   testrst;

	assign opcode = instruction[15:11];

	clkrst clkrst_inst (
			    .clk(clk),
			    .rst(crst),
			    .err(baderror)
			    );

	decode decode_inst (
			    .Instruction(instruction),
			    .WriteData(WriteData),
			    .Clk(clk),
			    .Rst(rst),
			    .ReadData1(ReadData1_d),
			    .ReadData2(ReadData2_d),
			    .ExtImm(ExtImm_d),
			    .RegDst(RegDst_d),
			    .Func(Func_d),
			    .ZeroExt(ZeroExt_d),
			    .MemtoReg(MemtoReg_d),
			    .ImmLen(ImmLen_d),
			    .RegWrite(RegWrite_d),
			    .JumpSrc(JumpSrc_d),
			    .MemRead(MemRead_d),
			    .Jump(Jump_d),
			    .MemWrite(MemWrite_d),
			    .JumpLink(JumpLink_d),
			    .Branch(Branch_d),
			    .ALUExt(ALUExt_d),
			    .ALUOp(ALUOp_d),
			    .Halted(Halted_d),
			    .PCEn(PCEn_d),
			    .Dump(Dump_d),
			    .Err(err_d),
			    .ALUSRC(ALUSrc_d)
			    );

	// Set the golden values to the current instruction
	always @(instruction)
	  begin
		  casex(opcode)
		    5'h0:
		      // HALT
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'h1;
			      Dump = 1'h1;
			      err = 1'h0;
		      end
		    5'h1:
		      // NOP
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'hx;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'hx;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h8:
		      // ADDI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h9:
		      // SUBI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h1;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'ha:
		      // XORI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h1;
			      Func[0] = 1'h0;
			      ZeroExt = 1'h1;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'hb:
		      // ANDNI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'h1;
			      Func[0] = 1'h1;
			      ZeroExt = 1'h1;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h14:
		      // ROLI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h15:
		      // SLLI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h1;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h16:
		      // RORI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h1;
			      Func[0] = 1'h0;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h17:
		      // SRLI
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h1;
			      Func[0] = 1'h1;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h10:
		      // st
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h1;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h11:
		      // ld
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h1;
			      RegWrite = 1'h1;
			      MemRead = 1'h1;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h13:
		      // stu
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h1;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h19:
		      // BTR
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h1;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1b:
		      // ADD, SUB, XOR, ANDN
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = instruction[1];
			      Func[0] = instruction[0];
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1a:
		      // SLL, SRL, ROL, ROR
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h1;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'h0;
			      Func[0] = 1'h0;
			      ZeroExt = 1'hx;
			      ImmLen[1] = instruction[1];
			      ImmLen[0] = instruction[0];
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1c:
		      // SEQ
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1d:
		      // SLT
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1e:
		      // SLE
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h1;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h1f:
		      // SCO
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h0;
			      ALUSrc = 1'h0;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'hc:
		      // BEQZ
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'hd:
		      // BNEZ
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'he:
		      // BLTZ
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'hf:
		      // BGEZ
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h18:
		      // lbi
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h1;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h12:
		      // slbi
		      begin
			      RegDst[1] = 1'h0;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'h0;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h1;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h1;
			      ALUExt[0] = 1'h1;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h4:
		      // J
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h1;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h1;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h5:
		      // JR
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h1;
			      Jump = 1'h1;
			      JumpLink = 1'h0;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h6:
		      // JAL
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h1;
			      ImmLen[0] = 1'h0;
			      JumpSrc = 1'h0;
			      Jump = 1'h1;
			      JumpLink = 1'h1;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h7:
		      // JALR
		      begin
			      RegDst[1] = 1'h1;
			      RegDst[0] = 1'h1;
			      ALUSrc = 1'h1;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h1;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h1;
			      ALUOp[1] = 1'h0;
			      ALUOp[0] = 1'h0;
			      PCEn = 1'h1;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'h0;
			      ImmLen[1] = 1'h0;
			      ImmLen[0] = 1'h1;
			      JumpSrc = 1'h1;
			      Jump = 1'h1;
			      JumpLink = 1'h1;
			      ALUExt[1] = 1'h0;
			      ALUExt[0] = 1'h0;
			      Halted = 1'h0;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    5'h2:
		      // SIIC
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'hx;
			      MemRead = 1'hx;
			      MemWrite = 1'hx;
			      Branch = 1'hx;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'hx;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'hx;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'hx;
			      Dump = 1'hx;
			      err = 1'h1;
		      end
		    5'h3:
		      // NOP/RTI
		      begin
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'h0;
			      MemRead = 1'h0;
			      MemWrite = 1'h0;
			      Branch = 1'h0;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'hx;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'h0;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'hx;
			      Dump = 1'h0;
			      err = 1'h0;
		      end
		    default:
		      begin
			      // Illegal operation
			      RegDst[1] = 1'hx;
			      RegDst[0] = 1'hx;
			      ALUSrc = 1'hx;
			      MemtoReg = 1'hx;
			      RegWrite = 1'hx;
			      MemRead = 1'hx;
			      MemWrite = 1'hx;
			      Branch = 1'hx;
			      ALUOp[1] = 1'hx;
			      ALUOp[0] = 1'hx;
			      PCEn = 1'hx;
			      Func[1] = 1'hx;
			      Func[0] = 1'hx;
			      ZeroExt = 1'hx;
			      ImmLen[1] = 1'hx;
			      ImmLen[0] = 1'hx;
			      JumpSrc = 1'hx;
			      Jump = 1'hx;
			      JumpLink = 1'hx;
			      ALUExt[1] = 1'hx;
			      ALUExt[0] = 1'hx;
			      Halted = 1'hx;
			      Dump = 1'hx;
			      err = 1'h1;
		      end // case: default
		  endcase // casex (opcode)
	  end // always @ (instruction)

	assign gbus = {RegDst,
		       ALUSrc,
		       MemtoReg,
		       RegWrite,
		       MemRead,
		       MemWrite,
		       Branch,
		       ALUOp,
		       PCEn,
		       Func,
		       ZeroExt,
		       ImmLen,
		       JumpSrc,
		       Jump,
		       JumpLink,
		       ALUExt,
		       Halted,
		       Dump,
		       err};

	assign dbus = {RegDst_d,
		       ALUSrc_d,
		       MemtoReg_d,
		       RegWrite_d,
		       MemRead_d,
		       MemWrite_d,
		       Branch_d,
		       ALUOp_d,
		       PCEn_d,
		       Func_d,
		       ZeroExt_d,
		       ImmLen_d,
		       JumpSrc_d,
		       Jump_d,
		       JumpLink_d,
		       ALUExt_d,
		       Halted_d,
		       Dump_d,
		       err_d};

	assign baderror = 1'b0;
	assign cdiff = gbus ^ dbus;
	assign rst = testrst | crst; // Allows rst after halt

	initial
	  begin
		  instruction = 16'h0800; // NOP
		  WriteData = 16'h0;
		  fail = 1'b0;
		  testfail = 1'b0;
		  #54000
		    begin
			    if (fail === 1'b0)
			      $display("TEST SUCCEEDED");
			    else
			      $display("TEST FAILED");
			    $finish;
		    end
	  end

	always @(posedge clk)
	  begin
		  testfail = 1'b0;
	  end

	always @(negedge clk)
	  begin
		  if (testrst)
		    begin
			    testrst = 1'b0;
		    end
		  else if (Halted)
		    begin
			    testrst = 1'b1;
		    end
		  else if (dbus != gbus)
		    begin
			    $display("ERROR: Control signals do not match: Opcode: 0x%x, Expected: b%b, Got: b%b, Failbits: b%b",
				     opcode, gbus, dbus, cdiff);
			    fail = 1'b1;
			    testfail = 1'b1;
		    end
		  // TODO test readdata
		  // Set instruction for next pass
		  instruction = $random;
		  // TODO Randomize write data
	  end

endmodule // decode_bench
