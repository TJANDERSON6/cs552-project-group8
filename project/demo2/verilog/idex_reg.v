module idex_reg(
		// Inputs
		InHalted,
		InRegWrite,
		InBranch,
		InJumpLink,
		InJumpSrc,
		InMemtoReg,
		InMemRead,
		InMemWrite,
		InPCNext,
		InALUExt,
		InALUSRC,
		InPCSrc,
		InALUOp,
		InPCAdded,
		InFunc,
		InReadData1,
		InReadData2,
		InJump,
		InExtImm,
		InRs,
		InRt,
		InInstruction,
		InRd,
		InZeroExt,
		rst,
		clk,
		wEn,

		// Outputs
		OutHalted,
		OutRegWrite,
		OutBranch,
		OutJumpLink,
		OutJumpSrc,
		OutMemtoReg,
		OutMemRead,
		OutMemWrite,
		OutPCNext,
		OutALUExt,
		OutALUSRC,
		OutPCSrc,
		OutALUOp,
		OutPCAdded,
		OutFunc,
		OutReadData1,
		OutReadData2,
		OutJump,
		OutExtImm,
		OutRs,
		OutRt,
		OutInstruction,
		OutRd,
		OutZeroExt,
		err
		);

	parameter N = 128;

	input 	wire InHalted;
	input 	wire InRegWrite;
	input 	wire InBranch;
	input 	wire InJumpLink;
	input 	wire InJumpSrc;
	input 	wire InMemtoReg;
	input 	wire InMemRead;
	input 	wire InMemWrite;
	input 	wire [15:0] InPCNext;
	input 	wire [1:0]  InALUExt;
	input 	wire	    InALUSRC;
	input 	wire	    InPCSrc;
	input 	wire [1:0]  InALUOp;
	input 	wire [15:0] InPCAdded;
	input 	wire [1:0]  InFunc;
	input 	wire [15:0] InReadData1;
	input 	wire [15:0] InReadData2;
	input 	wire	    InJump;
	input 	wire [15:0] InExtImm;
	input 	wire [2:0]  InRs;
	input 	wire [2:0]  InRt;
	input 	wire [15:0] InInstruction;
	input 	wire [2:0]  InRd;
	input 	wire	    InZeroExt;
	input wire	    rst;
	input wire	    clk;
	input wire		wEn; 

	output 	wire	    OutHalted;
	output 	wire	    OutRegWrite;
	output 	wire	    OutBranch;
	output 	wire	    OutJumpLink;
	output 	wire	    OutJumpSrc;
	output 	wire	    OutMemtoReg;
	output 	wire	    OutMemRead;
	output 	wire	    OutMemWrite;
	output 	wire [15:0] OutPCNext;
	output 	wire [1:0]  OutALUExt;
	output 	wire	    OutALUSRC;
	output 	wire	    OutPCSrc;
	output 	wire [1:0]  OutALUOp;
	output 	wire [15:0] OutPCAdded;
	output 	wire [1:0]  OutFunc;
	output 	wire [15:0] OutReadData1;
	output 	wire [15:0] OutReadData2;
	output 	wire	    OutJump;
	output 	wire [15:0] OutExtImm;
	output 	wire [2:0]  OutRs;
	output 	wire [2:0]  OutRt;
	output 	wire [15:0] OutInstruction;
	output 	wire [2:0]  OutRd;
	output 	wire	    OutZeroExt;
	output wire	    err;

	f_reg #(
		.N(N)
		)
	f_reg_inst (
		    .dOut(
			  {
			   OutHalted,
			   OutRegWrite,
			   OutBranch,
			   OutJumpLink,
			   OutJumpSrc,
			   OutMemtoReg,
			   OutMemRead,
			   OutMemWrite,
			   OutPCNext,
			   OutALUExt,
			   OutALUSRC,
			   OutPCSrc,
			   OutALUOp,
			   OutPCAdded,
			   OutFunc,
			   OutReadData1,
			   OutReadData2,
			   OutJump,
			   OutExtImm,
			   OutRs,
			   OutRt,
			   OutInstruction,
			   OutRd,
			   OutZeroExt
			   }
			  ),
		    .err(err),
		    .clk(clk),
		    .rst(rst),
		    .dIn(
			 {
	
			  InHalted,
			  InRegWrite,
			  InBranch,
			  InJumpLink,
			  InJumpSrc,
			  InMemtoReg,
			  InMemRead,
			  InMemWrite,
			  InPCNext,
			  InALUExt,
			  InALUSRC,
			  InPCSrc,
			  InALUOp,
			  InPCAdded,
			  InFunc,
			  InReadData1,
			  InReadData2,
			  InJump,
			  InExtImm,
			  InRs,
			  InRt,
			  InInstruction,
			  InRd,
			  InZeroExt
			  }
			 ),
		    .wEn(wEn)
		    );

endmodule // idex_reg
