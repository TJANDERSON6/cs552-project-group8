/*
    CS/ECE 552 Spring '23
    Homework #1, Problem 1

    2-1 mux template
*/
`default_nettype none
module mux2_1(out, inputA, inputB, sel);
    output wire  out;
    input wire  inputA, inputB;
    input wire  sel;

    // YOUR CODE HERE
	wire not_gate_1, and_gate_1, and_gate_2;
	not1 n1(.out(not_gate_1), .in1(sel)); 
	and2 a1(.out(and_gate_1), .in1(inputA), .in2(not_gate_1)); 
	and2 a2(.out(and_gate_2), .in1(inputB), .in2(sel)); 
	or2 o1(.out(out), .in1(and_gate_1), .in2(and_gate_2)); 
    
endmodule
`default_nettype wire
