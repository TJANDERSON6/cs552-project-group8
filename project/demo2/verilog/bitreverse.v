/*
   CS/ECE 552 Spring '23
  
   Filename        : bitreverse.v
   Description     : This is the module for the bitreverse block in Execute stage.
*/
`default_nettype none
module bitreverse (ALUInA, BtrRslt);

	input wire [15:0] ALUInA; 
	output wire [15:0] BtrRslt; 

	assign BtrRslt = {	ALUInA[0], ALUInA[1], ALUInA[2], ALUInA[3], 
				ALUInA[4], ALUInA[5], ALUInA[6], ALUInA[7], 
				ALUInA[8], ALUInA[9], ALUInA[10], ALUInA[11], 
				ALUInA[12], ALUInA[13], ALUInA[14], ALUInA[15]
				};

endmodule
`default_nettype wire