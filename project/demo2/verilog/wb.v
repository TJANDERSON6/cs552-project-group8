/*
   CS/ECE 552 Spring '23
  
   Filename        : wb.v
   Description     : This is the module for the overall Write Back stage of the processor.
*/
`default_nettype none
module wb (
	   // inputs
	   Halted, RegWrite, MemWrite, JumpLink, JumpSrc, MemtoReg, PCNext, ReadData, ExecRslt, Rs, Rd,
	   clk, rst, wEn,
	   // outputs
	   WbtoDecodeRd, WbtoDecodeWriteData, WbtoDecodeRegWrite,
	   err
	   );

	input wire Halted;
	input wire RegWrite;
	input wire MemWrite;
	input wire JumpLink;
	input wire JumpSrc;
	input wire MemtoReg;
	input wire [15:0] PCNext;
	input wire [15:0] ReadData;
	input wire [15:0] ExecRslt;
	input wire [2:0]  Rs;
	input wire [2:0]  Rd;
	input wire	  clk;
	input wire	  rst;
	input wire	  wEn; 

	output wire [2:0] WbtoDecodeRd;
	output wire [15:0] WbtoDecodeWriteData;
	output wire	   WbtoDecodeRegWrite;
	output wire	   err;

	// additional variables
	wire [15:0]	   wbData;
	wire		   Halt;	// Reference in bench to call $finish
	wire [15:0]		wbtoDecodeTmpData; 

	// Write data back to register with RF Bypass
	assign wbData = MemtoReg ? ReadData : ExecRslt; 
	assign wbtoDecodeTmpData = JumpLink ? PCNext : wbData;
        assign WbtoDecodeWriteData = wEn ? wbtoDecodeTmpData : WbtoDecodeWriteData; 		// unsure change	
	assign WbtoDecodeRegWrite = wEn ? RegWrite : 1'b0;					// unsure change
	assign WbtoDecodeRd = MemWrite ? Rs : Rd;

	// Nothing here to generate error, so just say we're OK
	assign err = 1'b0;
   
endmodule
`default_nettype wire
