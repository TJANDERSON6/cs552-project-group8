module ifid_reg (
		 // Inputs
		 InStarted,
		 InHalted,
		 InPCNext,
		 InInstruction,
		 wEn,
		 rst,
		 clk,
		 // Outputs
		 OutStarted,
		 OutHalted,
		 OutPCNext,
		 OutInstruction,
		 err
		 );
	parameter N = 128;

	input wire InStarted;
	input 	wire InHalted;
	input 	wire [15:0] InPCNext;
	input 	wire [15:0] InInstruction;
	input wire	    wEn;
	input wire	    rst;
	input wire	    clk;

	output wire	    OutStarted;
	output 	wire	    OutHalted;
	output 	wire [15:0] OutPCNext;
	output 	wire [15:0] OutInstruction;
	output wire	    err;

	f_reg #(
		.N(N)
		) f_reg_inst (
			      .dOut(
				    {
				     OutStarted,
				     OutHalted,
				     OutPCNext,
				     OutInstruction
				     }
				    ),
			      .err(err),
			      .clk(clk),
			      .rst(rst),
			      .dIn(
				   {
				    InStarted,
				    InHalted,
				    InPCNext,
				    InInstruction
				    }
				   ),
			      .wEn(wEn)
			      );
endmodule // ifid_reg
