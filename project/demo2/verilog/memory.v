/*
   CS/ECE 552 Spring '23
  
   Filename        : memory.v
   Description     : This module contains all components in the Memory stage of the 
                     processor.
*/
`default_nettype none
module memory (
	       // inputs
	       Halted, MemRead, MemWrite, ExecRslt, ReadData2, Rd,
	       MEMWB_ReadData, MEMWB_MemtoReg, MEMWB_Rd, MEMWB_ExecRslt,
	       MEMWB_RegWrite, MEMWB_MemWrite, MEMWB_Rs,
	       WbtoDecodeWriteData,
	       clk, rst,
	       // outputs
	       memStall,
	       ReadData,
	       err
	       );

	input wire 		Halted;
	input wire 		MemRead;
	input wire 		MemWrite;
	input wire [15:0] 	ExecRslt;
	input wire [15:0] 	ReadData2;
	input wire [2:0]  	Rd;
	input wire [15:0] 	MEMWB_ReadData;
	input wire	  	MEMWB_MemtoReg;
	input wire [2:0]  	MEMWB_Rd;
	input wire [15:0] 	MEMWB_ExecRslt;
	input wire	  	MEMWB_RegWrite;
	input wire	  	MEMWB_MemWrite;
	input wire [2:0]  	MEMWB_Rs;
	input wire [15:0] 	WbtoDecodeWriteData;
	input wire	  	clk;
	input wire	  	rst;

	output wire [15:0] 	ReadData;
	output wire	   	err;
	output wire 		memStall; 

	wire [15:0]	   	ReadData2Forwarded;
	wire		   	MemMemErr;
	wire		   MemErr;
	

	wire			CacheHit_Mem; 
	wire			Done; 
	wire [15:0]		ReadDataTmp;
	wire 			stall; 			// from cache

	// No way to generate an error yet
	assign err = MemMemErr | MemErr;

	// Mem/Mem forwarding module
	memmem_forwarding
	  memmem_forwarding_inst (
				  .EXMEM_ReadData2(ReadData2),
				  .EXMEM_Rd(Rd),
				  .EXMEM_MemWrite(MemWrite),
				  .EXMEM_ExecRslt(ExecRslt),
				  .MEMWB_ReadData(MEMWB_ReadData),
				  .MEMWB_MemtoReg(MEMWB_MemtoReg),
				  .MEMWB_Rd(MEMWB_Rd),
				  .MEMWB_ExecRslt(MEMWB_ExecRslt),
				  .MEMWB_RegWrite(MEMWB_RegWrite),
				  .MEMWB_MemWrite(MEMWB_MemWrite),
				  .MEMWB_Rs(MEMWB_Rs),
				  .WbtoDecodeWriteData(WbtoDecodeWriteData),
				  .Clk(clk),
				  .Rst(rst),
				  .Err(MemMemErr),
				  .ReadData2Out(ReadData2Forwarded)
				  );
  
	stallmem stallmem_mem	(
				.DataOut(ReadDataTmp), 
				.Done(Done), 
				.Stall(stall),				// Stall Signal - a stall for entire registers for proc 
				.CacheHit(CacheHit_Mem),
				.DataIn(ReadData2Forwarded), 
				.Addr(ExecRslt), 
				.Wr(MemWrite),
				.Rd(MemRead|MemWrite), 
				.createdump(Halted), 
				.clk(clk),
				.rst(rst),
				.err(MemErr)
				);

	assign memStall = ~Done & (MemWrite | MemRead);

	assign ReadData = memStall ? ReadData : ReadDataTmp; 

endmodule
`default_nettype wire
