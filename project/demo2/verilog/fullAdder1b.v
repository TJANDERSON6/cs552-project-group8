/*
    CS/ECE 552 Spring '23
    Homework #1, Problem 2
    
    a 1-bit full adder
*/
`default_nettype none
module fullAdder1b(s, cOut, inA, inB, cIn);
   output wire s;
   output wire cOut;
   input  wire inA, inB;
   input  wire cIn;

   wire	       nand_out;
   wire	       xor_out;
   wire	       ab_out;
   wire	       ac_out;
   wire	       bc_out;

   // YOUR CODE HERE
   // s = (inA ^ inB) ^ cIn
   // cOut = ((inA | inB) & cIn) | (inA & inB)
   // ((inA & cIn) | (inB & cIn)) | (inA & inB)
   xor2 xor2_half (
		   .out(xor_out),
		   .in1(inA),
		   .in2(inB)
		   );
   xor2 xor2_full (
		   .out(s),
		   .in1(xor_out),
		   .in2(cIn)
		   );
   and2 and2_ac (
		 .out(ac_out),
		 .in1(inA),
		 .in2(cIn)
		 );
   and2 and2_bc (
		 .out(bc_out),
		 .in1(inB),
		 .in2(cIn)
		 );
   and2 and2_ab (
		 .out(ab_out),
		 .in1(inA),
		 .in2(inB)
		 );
   or3 or3_out (
		.out(cOut),
		.in1(ac_out),
		.in2(bc_out),
		.in3(ab_out)
		);
endmodule
`default_nettype wire
