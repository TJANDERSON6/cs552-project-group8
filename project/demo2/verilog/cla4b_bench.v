/*
 * Test bench for 4 bit full adder module
 * CS552 - Tyler Anderson
 */

module cla4b_bench();  
   reg [3:0]  inA;
   reg [3:0]  inB;
   reg	      cIn;
   reg [3:0] s_expected;
   reg	     c_expected;
   reg [3:0] g;
   reg [3:0] p;
   reg [3:1] c;

   wire	      clk;
   wire [3:0] result;
   wire	      cOut;
   wire [3:0] gModule;
   wire [3:0] pModule;
   wire	      rst;
   wire	      err;

   clkrst this_clkrst(.clk(clk), .rst(rst), .err(err));
   
   cla4b cla4b_inst(.sum(result),
		    .cOut(cOut),
		    .inA(inA),
		    .inB(inB),
		    .cIn(cIn),
		    .g(gModule),
		    .p(pModule));

   always@(posedge clk)
     begin
	inA = $random;
	inB = $random;
	cIn = $random;
     end

   always@(negedge clk)
     begin
	{c_expected, s_expected} = inA + inB + cIn;
	g = inA & inB;
	p = inA | inB;

	c[1] = g[0] | (p[0] & cIn);
	c[2] = g[1] | (p[1] & g[1]) | (p[1] & p[0] & cIn);
	c[3] = g[2] + (p[2] & g[1]) | (p[2] & p[1] & g[0]) | (p[2] & p[1] & p[0] & cIn);

	// $display("Let's see if we see this message. Add is 0x%x and should be 0x%x",
		 // result, s_expected);
	if (s_expected !== result) $display ("ERRORCHECK Sum Error, (0x%x + 0x%x), cIn: 0x%x Sum: 0x%x, Golden Sum: 0x%x",
					     inA, inB, cIn, {cOut, result}, {c_expected, s_expected});
	if (c_expected !== cOut) $display ("ERRORCHECK Carry Error: Carry: 0x%x, Expected: 0x%x",
					   cOut, c_expected);
     end

   initial
     begin
	inA = 4'h0;
	inB = 4'h0;
	cIn = 1'h0;

	#3200
	  $finish;
     end
endmodule
