module regread_mux(out, encode, in);
   parameter N = 16;
   parameter P = 8;
   parameter E = 3;

   output wire [N-1:0] out;
   input wire [N*P-1:0]	in;
   input wire [E-1:0]	encode;

   wire [N*P-1:0]	shbus[E-1:0];

   assign shbus[0] = encode[0] === 1'b1 ? in >> 16 : in;
   assign shbus[1] = encode[1] === 1'b1 ? shbus[0] >> 32 : shbus[0];
   assign shbus[2] = encode[2] === 1'b1 ? shbus[1] >> 64 : shbus[1];
   assign out = shbus[2][N-1:0];
endmodule
