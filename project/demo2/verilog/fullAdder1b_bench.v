/*
 * Test bench for 1 bit full adder module
 * CS552 - Tyler Anderson
 */

module fullAdder1b_bench();  
   reg [2:0]  in;
   wire	      clk;
   wire	      result;
   wire	      cOut;
   reg [1:0] s_expected;
   wire	      rst;
   wire	      err;

   clkrst this_clkrst(.clk(clk), .rst(rst), .err(err));
   
   fullAdder1b fullAdder1b_inst(.s(result),
				.cOut(cOut),
				.inA(in[0]),
				.inB(in[1]),
				.cIn(in[2]));

   always@(posedge clk)
     begin
	in = $random;
     end

   always@(negedge clk)
     begin
	s_expected = in[0] + in[1] + in[2];
	$display("Let's see if we see this message. Add is 0x%x and should be 0x%x",
		 {cOut, result}, s_expected);
	if (s_expected !== {cOut, result}) $display ("ERRORCHECK Sum Error, Sum: 0x%x, Golden Sum: 0x%x",
						     {cOut, result}, s_expected);
     end

   initial
     begin
	in = 2'h0;
	#3200
	  $finish;
     end
endmodule
