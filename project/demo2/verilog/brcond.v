/*
   CS/ECE 552 Spring '23
  
   Filename        : brcond.v
   Description     : This is the module for the brcond block in Execute stage.
*/
`default_nettype none

module brcond(ALUInA, control, BrRslt); 
	
	input wire [15:0] ALUInA; 
	input wire [1:0] control;
	output wire BrRslt; 

	assign BrRslt = (control == 2'b00) ? ((~(|ALUInA) == 1'b1) ? 1'b1 : 1'b0) : (			// Rs == 0 
	       		(control == 2'b01) ? (((|ALUInA) == 1'b1) ? 1'b1 : 1'b0) : (			// Rs != 0
			(control == 2'b10) ? ((ALUInA[15] == 1'b1) ? 1'b1 : 1'b0) : (			// Rs < 0
			(control == 2'b11) ? ((ALUInA[15] == 1'b0) ? 1'b1 : 1'b0) : 1'b0))); 		// Rs > 0



endmodule
`default_nettype wire
