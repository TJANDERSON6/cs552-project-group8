module regwrite_decode(out, writeEn, writeRegSel);
   parameter N = 8; // Number of registers
   parameter RI = 3; // Number of encoding bits
   parameter NM = N -1;

   output wire [N-1:0] out;

   input wire	       writeEn;
   input wire [RI-1:0] writeRegSel;

   // For internal nets
   wire [N-1:0]	       sbus;
   wire [N-1:0]	       dbus[RI-1:0];

   assign sbus = {{NM{1'h0}}, writeEn};
   assign dbus[0] = writeRegSel[0] === 1'b1 ? sbus << 1 : sbus;
   assign dbus[1] = writeRegSel[1] === 1'b1 ? dbus[0] << 2 : dbus[0];
   assign dbus[2] = writeRegSel[2] === 1'b1 ? dbus[1] << 4 : dbus[1];
   assign out = dbus[2];
endmodule
