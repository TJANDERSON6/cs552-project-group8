// halt_latch module
// CS552 Sp23 Group 8
//
// Ensure that once halt instruction is processed, processor will
// remain halted until reset

module halt_latch(
		  // Inputs
		  Clk, Rst, Opcode,

		  // Outputs
		  Halted
		  );

   input wire Clk;
   input wire Rst;
   input wire [4:0] Opcode;

   output wire	    Halted;

   wire		    ihalt;
   wire		    data;
   wire [5:0]	    ibus;

   // Is halt instruction?
   assign ihalt = ~|Opcode;

   // data in is same as data out, unless Rst is asserted,
   // or a halt instruction is being decoded
   // Note this is inverted with ORs for before data is set
   assign data = ~(Rst | ~(ihalt | data));

   dff dff_inst (
		 .q(Halted),
		 .d(data),
		 .clk(Clk),
		 .rst(Rst)
		 );
endmodule // halt_latch
