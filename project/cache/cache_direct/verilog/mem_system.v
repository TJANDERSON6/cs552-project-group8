/* $Author: karu $ */
/* $LastChangedDate: 2009-04-24 09:28:13 -0500 (Fri, 24 Apr 2009) $ */
/* $Rev: 77 $ */

`default_nettype none
module mem_system(/*AUTOARG*/
   // Outputs
   DataOut, Done, Stall, CacheHit, err,
   // Inputs
   Addr, DataIn, Rd, Wr, createdump, clk, rst
   );
   
   input wire [15:0] Addr;
   input wire [15:0] DataIn;
   input wire        Rd;
   input wire        Wr;
   input wire        createdump;
   input wire        clk;
   input wire        rst;
   
   output wire [15:0] DataOut;
   output wire        Done;
   output wire        Stall;
   output wire        CacheHit;
   output wire        err;


   // additional variables
	
   	wire 		sm_ready; 
	wire 		sm_comp; 
	wire 		sm_wb; 
	wire		sm_mem; 
	wire [2:0]	sm_cache_offset;
	wire [2:0]	sm_mem_offset; 
	wire 		sm_done; 
	wire 		sm_crd; 
	wire		sm_memrd; 
	wire 		sm_cwr; 
	wire		sm_memwr; 
	wire 		sm_err;
	wire		sm_request;
	wire		sm_rd;
	wire		sm_wr;

	wire [2:0]	c0_offset; 
	wire [15:0]	cDataIn; 
	wire 		c0_cacheHit; 
	wire 		c0_dirty; 
	wire [4:0] 	c0_tagOut; 
	wire [15:0]	c0_dataOut; 
	wire 		c0_valid; 
	wire 		c0_err; 


	wire [15:0]	mem_addr; 
	wire [15:0]	mem_data;
	wire 		mem_wait;
	wire 		mem_err; 

	wire [15:0]	DataNew;
	wire [15:0]	AddrNew;
	wire		WrNew;
	wire		RdNew;
	wire [4:0]	TagNew; 
	wire [7:0]	IndexNew; 
	wire [2:0]	OffsetNew;
        wire [4:0]	tag; 
	wire [7:0]	index;
	wire [2:0]	offset;
	wire		request; // A valid request is received

/* may require memsys_reg */


   /* data_mem = 1, inst_mem = 0 *
    * needed for cache parameter */
   parameter memtype = 0;
   cache #(0 + memtype) c0(// Outputs
                          .tag_out              (c0_tagOut),
                          .data_out             (c0_dataOut),
                          .hit                  (c0_cacheHit),
                          .dirty                (c0_dirty),
                          .valid                (c0_valid),
                          .err                  (c0_err),
                          // Inputs
                          .enable               (sm_crd | sm_cwr),
                          .clk                  (clk),
                          .rst                  (rst),
                          .createdump           (createdump),
                          .tag_in               (tag),
                          .index                (IndexNew),
                          .offset               (c0_offset),
                          .data_in              (cDataIn),
                          .comp                 (sm_comp),
                          .write                (sm_cwr),
                          .valid_in             (1'b1));

   cache_sm direct_mapped_cache(// Outputs
   				.ready(sm_ready),
				.comp(sm_comp), 
				.wb(sm_wb), 
				.mem(sm_mem), 
				.coffset(sm_cache_offset), 
				.moffset(sm_mem_offset), 
				.done(sm_done), 
				.crd(sm_crd), 
				.memrd(sm_memrd), 
				.cwr(sm_cwr), 
				.memwr(sm_memwr),
				.request(sm_request),
				.err(sm_err),
				.lwr(sm_wr),
				.lrd(sm_rd),
				// Inputs
				.dirty(c0_dirty), 
				.valid(c0_valid), 
				.cwait(mem_wait),
			        .rd(RdNew),
				.wr(WrNew), 
				.rst(rst), 
				.clk(clk),
				.hit(c0_cacheHit),
				.request_in(request));

   four_bank_mem mem(// Outputs
                     .data_out          (mem_data),
                     .stall             (mem_wait),
                     .busy              (),
                     .err               (mem_err),
                     // Inputs
                     .clk               (clk),
                     .rst               (rst),
                     .createdump        (createdump),
                     .addr              (mem_addr),
                     .data_in           (c0_dataOut),
                     .wr                (sm_memwr),
                     .rd                (sm_memrd));	

	memsys_reg memsys_reg_inst (
				    // Inputs
				    .clk(clk),
				    .rst(rst | Done),
				    .en(sm_ready),
				    .wr_in(Wr),
				    .rd_in(Rd),
				    .addr_in(Addr),
				    .data_in(DataIn),
				    // Outputs
				    .wr_out(WrNew),
				    .rd_out(RdNew),
				    .addr_out(AddrNew),
				    .data_out(DataNew),
				    .req(request),
				    .err()
				    );

	assign cDataIn = (sm_wb | sm_mem) ? mem_data : DataNew;

	assign TagNew = Addr[15:11];
	assign IndexNew = Addr[10:3];
	assign OffsetNew = Addr[2:0];

	assign tag = sm_wb ? c0_tagOut : TagNew; 
	assign index = IndexNew;
        assign c0_offset = (sm_wb | sm_mem) ? sm_cache_offset : OffsetNew; 	
	assign offset = (sm_wb | sm_mem) ? sm_mem_offset : OffsetNew; 
	assign mem_addr = {tag, index, offset};

   	assign CacheHit = c0_cacheHit & c0_valid & sm_request;
	assign DataOut = (sm_wr & sm_rd) ? DataNew : c0_dataOut;
        assign Done = sm_done; 
	assign Stall = ~sm_ready | (sm_rd | sm_wr); 	
	assign err = c0_err | mem_err | sm_err;
   
endmodule // mem_system
`default_nettype wire
// DUMMY LINE FOR REV CONTROL :9:
