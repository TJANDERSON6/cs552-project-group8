/*
 * Direct-Mapped Cache State Machine
 * CS552 Sp23 Group 8
 * 
 * Tyler Anderson and Ryuki Koda
 *
 * State machine to control the memory system
 * 
 * 
 */

`default_nettype none
module cache_sm(
		// Inputs
		dirty, valid, cwait, rd, wr, rst, hit,
		request_in, clk,
		// Outputs
		ready, comp, wb, mem, moffset, coffset, done,
		crd, memrd, request,
		cwr, memwr, err, lwr, lrd
		);

	parameter SN = 4;

	output reg [2:0] moffset;
	output reg [2:0] coffset;
	output reg	 ready;
	output reg	 comp;
	output reg	 wb;
	output reg	 mem;
	output reg	 done;
	output reg	 crd;
	output reg	 memrd;
	output reg	 cwr;
	output reg	 memwr;
	output reg	 err;
	output wire	 request;
	output reg	 lwr;
	output reg	 lrd;

	input wire	 dirty;
	input wire	 valid;
	input wire	 cwait;
	input wire	 rd;
	input wire	 wr;
	input wire	 rst;
	input wire	 hit;
	input wire	 clk;
	input wire	 request_in;

	wire [SN-1:0]	 state;
	wire [SN-1:0]	 pendingstate;
        reg [SN-1:0]	 nextstate;
	wire		 rerr;
	wire		 pclk;
	//wire		 validreq;
	wire [SN-1:0]	 compstate;
	wire [SN-1:0]	 wbwait;
	wire		 checkhit;
	wire		 swr;
	wire		 srd;
	reg		 prequest;

	f_reg #(
		.N(SN)
		) f_reg_inst (
			      .dOut(state),
			      .err(rerr),
			      .clk(clk),
			      .rst(rst),
			      .dIn(nextstate),
			      .wEn(1'b1)
			      );

	dff dff_inst (
		      .q(pclk),
		      .d(~pclk),
		      .clk(clk),
		      .rst(rst)
		      );

	dff dff_req (
		     .q(request),
		     .d(prequest),
		     .clk(clk),
		     .rst(rst)
		     );

	// assign state = validreq ? 4'd1 : pendingstate;
	assign checkhit = valid & hit;
	// assign err = rerr;
	// assign validreq = (wr | rd) & (state == 4'd0);
	assign compstate = checkhit ? 4'd0 : ((swr | (srd & dirty)) & valid ? 4'd2 : 4'd7);
	assign wbwait = cwait ? 4'd06 : 4'd07;

	// store wr and rd when state maching is running
	assign swr = lwr;
	assign srd = lrd;

	always @(state or wr or rd or wbwait or checkhit or compstate)
	  begin
		  casex(state)
		    4'd00:
		      begin
			      // Idle
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b1;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      lwr = wr;
			      lrd = rd;
			      nextstate = (wr | rd) ? 4'd1 : 4'd0;
			      prequest = wr | rd;
			      err = 1'b0;
		      end
		    4'd01:
		      begin
			      // Comp
			      moffset = 3'h0;
			      coffset = 3'h0;
			      // Should ready be 1 on a hit?
			      ready = 1'b0;
			      comp = 1'b1;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = checkhit;
			      crd = srd;
			      memrd = 1'b0;
			      cwr = swr;
			      memwr = 1'b0;
			      nextstate = compstate;
			      prequest = 1'b0;
			      lwr = swr; // checkhit ? 1'b0 : swr;
			      lrd = srd; // checkhit ? 1'b0 : srd;
			      err = 1'b0;
		      end
		    4'd02:
		      begin
			      // WB1 (Write)
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b1;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b1;
			      nextstate = 4'd03;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd03:
		      begin
			      // WB2 (Write)
			      moffset = 3'h2;
			      coffset = 3'h2;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b1;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b1;
			      nextstate = 4'd04;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd04:
		      begin
			      // WB3 (Write)
			      moffset = 3'h4;
			      coffset = 3'h4;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b1;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b1;
			      nextstate = 4'd05;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd05:
		      begin
			      // WB4 (Write)
			      moffset = 3'h6;
			      coffset = 3'h6;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b1;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b1;
			      nextstate = 4'd06;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd06:
		      begin
			      // WB5 (Wait)
			      // This may add extra stall if cwait signal
			      // comes in a cycle state. Replace this with
			      // additional state if so
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = wbwait;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd07:
		      begin
			      // MEM1 (Read memory word 1)
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b1;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd08;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd08:
		      begin
			      // MEM2 (Read memory word 2)
			      moffset = 3'h2;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b1;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd09;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd09:
		      begin
			      // MEM3 (Read memory word 3, write cache word 1)
			      moffset = 3'h4;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b1;
			      cwr = 1'b1;
			      memwr = 1'b0;
			      nextstate = 4'd10;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd10:
		      begin
			      // MEM4 (Read memory word 4, write cache word 2)
			      moffset = 3'h6;
			      coffset = 3'h2;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b1;
			      cwr = 1'b1;
			      memwr = 1'b0;
			      nextstate = 4'd11;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd11:
		      begin
			      // MEM5 (Write cache word 3)
			      moffset = 3'h0;
			      coffset = 3'h4;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b1;
			      memwr = 1'b0;
			      nextstate = 4'd12;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd12:
		      begin
			      // MEM6 (Write cache word 4)
			      moffset = 3'h0;
			      coffset = 3'h6;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b1;
			      memwr = 1'b0;
			      nextstate = 4'd1;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    // All cases below are errors
		    4'd13:
		      begin
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd0;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b1;
		      end
		    4'd14:
		      begin
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd0;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b1;
		      end
		    4'd15:
		      begin
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd0;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b1;
		      end
		    default:
		      begin
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd0;
			      prequest = 1'b0;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b1;
		      end
		  endcase // casex (state)
	  end

endmodule // cache_sm
`default_nettype wire
