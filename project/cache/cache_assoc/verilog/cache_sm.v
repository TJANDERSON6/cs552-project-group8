/*
 * Set associative Cache State Machine
 * CS552 Sp23 Group 8
 * 
 * Tyler Anderson and Ryuki Koda
 *
 * State machine to control the memory system
 * 
 * 
 */

`default_nettype none
module cache_sm(
		// Inputs
		dirty, valid, cwait, rd, wr, rst, hit,
		request_in, clk,
		// Outputs
		ready, comp, wb, mem, moffset, coffset, done,
		crd, memrd, request,
		cwr, memwr, err, lwr, lrd, way, vway
		);

	parameter SN = 4;

	output reg [2:0] moffset;
	output reg [2:0] coffset;
	output reg	 ready;
	output reg	 comp;
	output reg	 wb;
	output reg	 mem;
	output reg	 done;
	output reg	 crd;
	output reg	 memrd;
	output reg	 cwr;
	output reg	 memwr;
	output reg	 err;
	output wire	 request;	// First cycle of wr/rd operation
	output reg	 lwr;		// Local wr storage 
	output reg	 lrd;		// Local rd storage
	output reg	 way;		// Used to index cache signals
	output wire	 vway;		// Way to eject

	input wire [1:0] dirty;
	input wire [1:0] valid;
	input wire	 cwait;
	input wire	 rd;
	input wire	 wr;
	input wire	 rst;
	input wire [1:0] hit;
	input wire	 clk;
	input wire	 request_in;

	wire [SN-1:0]	 state;
	wire		 rerr;
	wire		 pclk;
	wire [SN-1:0]	 compstate;
	wire [SN-1:0]	 wbwait;
	wire		 checkhit;
	wire		 swr;		// Value of the saved wr
	wire		 srd;		// Value of the saved rd
	wire		 sway;		// Value of the saved way
	wire		 selectway;	// Way selected in compare

	// Cache for selected way
	wire		 wdirty;
	wire		 wvalid;
	wire		 whit;
	wire		 hitway;
	wire		 bothinvalid;
	//wire		 nvway;

	reg		 prequest;
	reg		 nextvway;
        reg [SN-1:0]	 nextstate;

	f_reg #(
		.N(SN)
		) f_reg_inst (
			      .dOut(state),
			      .err(rerr),
			      .clk(clk),
			      .rst(rst),
			      .dIn(nextstate),
			      .wEn(1'b1)
			      );

	dff dff_inst (
		      .q(pclk),
		      .d(~pclk),
		      .clk(clk),
		      .rst(rst)
		      );

	dff dff_req (
		     .q(request),
		     .d(prequest),
		     .clk(clk),
		     .rst(rst)
		     );

	// victimway inverts
	dff vway_reg (
		      .q(vway),
		      .d(nextvway),
		      .clk(clk),
		      .rst(rst)
		      );

	// Figure out which way hit
	assign whit = |hit;
	assign hitway = hit[1] & (hit[1] ^ hit[0]);
	assign bothinvalid = ~|valid;

	// Assign indexed cache signals (only needed if hit)
	assign wdirty = selectway ? dirty[1] : dirty[0];
	assign wvalid = selectway ? valid[1] : valid[0];

	// assign state = validreq ? 4'd1 : pendingstate;
	assign checkhit = |(valid & hit);
	// assign err = rerr;
	// assign validreq = (wr | rd) & (state == 4'd0);
	assign compstate = checkhit ? 4'd0 : ((swr | (srd & wdirty)) & wvalid ? 4'd2 : 4'd7);
	assign wbwait = cwait ? 4'd06 : 4'd07;
	assign selectway = checkhit ? hitway : (bothinvalid ? 1'b0 : (valid == 2'h0 ? 1'b0 : (valid == 2'h1 ? 1'b1 : vway))); // (~valid[1] ? 1'b1 : ~valid[0] ? 1'b0 : vway)));

	// store wr and rd when state maching is running
	assign swr = lwr;
	assign srd = lrd;
	assign sway = way;

	// assign vway = ~nvway;

	always @(state or wr or rd or wbwait or checkhit or compstate
		 or vway or selectway)
	  begin
		  casex(state)
		    4'd00:
		      begin
			      // Idle
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b1;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      lwr = wr;
			      lrd = rd;
			      nextstate = (wr | rd) ? 4'd1 : 4'd0;
			      prequest = wr | rd;
			      nextvway = (wr | rd ) ? ~vway : vway;
			      way = 1'b0;
			      err = 1'b0;
		      end
		    4'd01:
		      begin
			      // Comp
			      moffset = 3'h0;
			      coffset = 3'h0;
			      // Should ready be 1 on a hit?
			      ready = 1'b0;
			      comp = 1'b1;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = checkhit;
			      crd = srd;
			      memrd = 1'b0;
			      cwr = swr;
			      memwr = 1'b0;
			      nextstate = compstate;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = selectway;
			      lwr = swr; // checkhit ? 1'b0 : swr;
			      lrd = srd; // checkhit ? 1'b0 : srd;
			      err = 1'b0;
		      end
		    4'd02:
		      begin
			      // WB1 (Write)
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b1;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b1;
			      nextstate = 4'd03;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd03:
		      begin
			      // WB2 (Write)
			      moffset = 3'h2;
			      coffset = 3'h2;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b1;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b1;
			      nextstate = 4'd04;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd04:
		      begin
			      // WB3 (Write)
			      moffset = 3'h4;
			      coffset = 3'h4;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b1;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b1;
			      nextstate = 4'd05;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd05:
		      begin
			      // WB4 (Write)
			      moffset = 3'h6;
			      coffset = 3'h6;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b1;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b1;
			      nextstate = 4'd06;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd06:
		      begin
			      // WB5 (Wait)
			      // This may add extra stall if cwait signal
			      // comes in a cycle state. Replace this with
			      // additional state if so
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b1;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = wbwait;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd07:
		      begin
			      // MEM1 (Read memory word 1)
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b1;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd08;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd08:
		      begin
			      // MEM2 (Read memory word 2)
			      moffset = 3'h2;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b1;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd09;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd09:
		      begin
			      // MEM3 (Read memory word 3, write cache word 1)
			      moffset = 3'h4;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b1;
			      cwr = 1'b1;
			      memwr = 1'b0;
			      nextstate = 4'd10;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd10:
		      begin
			      // MEM4 (Read memory word 4, write cache word 2)
			      moffset = 3'h6;
			      coffset = 3'h2;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b1;
			      cwr = 1'b1;
			      memwr = 1'b0;
			      nextstate = 4'd11;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd11:
		      begin
			      // MEM5 (Write cache word 3)
			      moffset = 3'h0;
			      coffset = 3'h4;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b1;
			      memwr = 1'b0;
			      nextstate = 4'd12;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    4'd12:
		      begin
			      // MEM6 (Write cache word 4)
			      moffset = 3'h0;
			      coffset = 3'h6;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b1;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b1;
			      memwr = 1'b0;
			      nextstate = 4'd1;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b0;
		      end
		    // All cases below are errors
		    4'd13:
		      begin
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd0;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b1;
		      end
		    4'd14:
		      begin
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd0;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b1;
		      end
		    4'd15:
		      begin
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd0;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b1;
		      end
		    default:
		      begin
			      moffset = 3'h0;
			      coffset = 3'h0;
			      ready = 1'b0;
			      comp = 1'b0;
			      wb = 1'b0;
			      mem = 1'b0;
			      done = 1'b0;
			      crd = 1'b0;
			      memrd = 1'b0;
			      cwr = 1'b0;
			      memwr = 1'b0;
			      nextstate = 4'd0;
			      prequest = 1'b0;
			      nextvway = vway;
			      way = sway;
			      lwr = swr;
			      lrd = srd;
			      err = 1'b1;
		      end
		  endcase // casex (state)
	  end

endmodule // cache_sm
`default_nettype wire
