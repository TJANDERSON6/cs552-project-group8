//
//
//	check WAW RAW hazards using ALU, Load/Store, and Branching
//
// 


.init:
	lbi	r0, 0			// r0 = 0		 
	lbi	r1, 0 			// r1 = 0
	lbi	r2, 0 			// r2 = 0
	lbi	r3, 0			// r3 = 0
	lbi	r4, 0 			// r4 = 0

.add_test:
	addi	r0, r0, 5		// r0 = 5
	addi 	r1, r0, 3		// r1 = 8
	addi	r2, r1, 4		// r2 = 12 
	add	r0, r1, r2 		// r0 = 20 

.load_store_test: 
	st	r0, r4, 0 		// mem[0] = 20
	st	r1, r0, 0 		// mem[20] = 8
	st	r2, r1, 2		// mem[10] = 12
	st	r2, r4, 0 		// mem[0] = 12
	ld	r0, r4, 0 		// r0 = 12
	ld	r0, r2, 8 		// r0 = 8

.stu_test:
	stu	r1, r0, -8		// mem[0] = 8, r0 = 0  
	stu	r0, r1, 2		// mem[10] = 0, r1 = 10
	ld	r4, r1, 10 		// r4 = 8 

.branch_test:
	addi	r0, r0, -1		// r0 = -1
	bnez	r0, .finish 		// branch to .finish 

.label: 
	nop
	nop
	nop
	nop
	nop

.finish:
	addi	r0, r0, 1		// r0 = 0
	beqz 	r0, .label		// jump to .label 



   
