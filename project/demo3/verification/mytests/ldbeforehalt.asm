// CS552 Spring 2023
// Group 8
// Tyler J. Anderson and Ryuki Koda
//
// ldbeforehalt.asm.asm
// Check to make sure halts and nops don't stall

.test_start:
	lbi	r0, U.data
	nop
	nop
	nop
	nop
	nop
	slbi	r0, L.data	// r0 = &A[0]
	nop
	nop
	nop
	nop
	nop
	ld	r1, r0, 0	// r1 = A[0]
	nop			// Make sure this nop doesn't stall
	nop
	nop
	nop
	nop
	ld	r2, r0, 0	// r2 = A[0]
	halt			// Make sure this halt doesn't stall
	add	r1, r1, r2	// Random gibberish
	add	r2, r0, r1
	slli	r0, r1, 3
	andni	r2, r2, 0
	subi	r1, r0, 3
	halt
	halt
	halt
	halt
	halt

.data:
	data	0x5ffe		// A[0]
