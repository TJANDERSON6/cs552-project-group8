// CS552 Spring 2023
// Group 8
// Tyler J. Anderson and Ryuki Koda
//
// jumptest.asm
// Test jump pipelining control hazards in a data-hazard-free environment

.test_start:
	lbi	r1, 0x2f
	nop
	nop
	nop
	nop
	nop
	slbi	r1, 0xee	// r1 = 0x2fee
	nop
	nop
	nop
	nop
	nop
	j	.j_check	// Jump to the next test, halt if fail
	halt
	halt
	halt
	halt
	halt
	halt
	halt
	halt

.j_check:
	addi	r1, r1, 1	// r1 = 0x2fef Prove we can operate after jump
	nop
	nop
	nop
	nop
	nop
	jal	.jal_check	// Jump and operate immediately following
	addi	r1, r1, 1	// r1 = 0x2ff2
	nop
	nop
	nop
	nop
	nop
	jr	r7, 0		// Return to jalr

.jal_check:
	addi	r1, r1, 1	// r1 = 0x2ff1
	nop
	nop
	nop
	nop
	nop
	jalr	r7, 0		// Make sure we can come back
	subi	r1, r1, 2	// r1 = 0x2ff0, shouldn't execute until jr
	nop
	nop
	nop
	nop
	nop

	// The following test data hazards + control hazards. skip for now
	halt
.jump_burst:
	jal	.jump_target	// Jump back and forth without break
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	j	.finished

.jump_target:
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	jalr	r7, 0
	halt
	halt
	halt
	halt
	halt
	halt

.finished:
	nop
	nop
	nop
	nop
	nop
	halt		// If we reached here, we are done
	halt
	halt
	halt
	halt
	halt
	halt
