/* 
	CS/ECE 552 Spring '23

	Filename: RtForwarding .v 
	Description: This is a block for RtForwarding in execute block - (ALUInB).  
*/ 
`default_nettype none
module RtForwarding(
			// inputs
			IDEX_Rt, 
			EXMEM_RegWrite, MEMWB_RegWrite, 
			EXMEM_Rd, MEMWB_Rd, EXMEM_Rs,
		    	MEMWB_Rs,
		    	EXMEM_JumpLink, EXMEM_PCAdded,
			EXMEM_ExecRslt, MEMWB_ExecRslt, ReadData2, 
			ExtImm, ALUSRC, WbtoDecodeWriteData,
		    	EXMEM_MemWrite, MEMWB_MemWrite,
			// outputs
			ALUInB
		); 

	// input bus
	input wire [2:0] IDEX_Rt; 
	input wire EXMEM_RegWrite; 
	input wire MEMWB_RegWrite;

	input wire [2:0] EXMEM_Rd; 
	input wire [2:0] MEMWB_Rd;
	input wire [2:0] EXMEM_Rs;
	input wire [2:0] MEMWB_Rs;

	input wire	 EXMEM_JumpLink;
	input wire [15:0] EXMEM_PCAdded;

	input wire [15:0] ExtImm; 
	input wire [15:0] EXMEM_ExecRslt; 
	input wire [15:0] MEMWB_ExecRslt; 
	input wire [15:0] ReadData2;
	input wire	  ALUSRC;
	input wire [15:0] WbtoDecodeWriteData;
	input wire	  EXMEM_MemWrite;
	input wire	  MEMWB_MemWrite;

	output wire [15:0] ALUInB;

	wire [15:0]	   ExExRslt;
	wire [15:0]	   MemExRslt;
	wire		   isMemEx;
	wire		   isExEx;

	assign ALUInB = ALUSRC ? ExtImm :
			(isExEx ? ExExRslt :
			 (isMemEx ? MemExRslt : ReadData2));

	assign ExExRslt = EXMEM_JumpLink ? EXMEM_PCAdded : EXMEM_ExecRslt;
	assign MemExRslt = WbtoDecodeWriteData;

	assign isExEx = EXMEM_RegWrite &
			(((IDEX_Rt == EXMEM_Rd) & ~EXMEM_MemWrite) |
			 ((IDEX_Rt == EXMEM_Rs) & EXMEM_MemWrite));
	assign isMemEx = MEMWB_RegWrite &
			 (((IDEX_Rt == MEMWB_Rd) & ~MEMWB_MemWrite) |
			  ((IDEX_Rt == MEMWB_Rs) & MEMWB_MemWrite));

endmodule 
`default_nettype wire 
