// CS552 Spring 2023
// Project Group 8
// Tyler J. Anderson and Ryuki Koda
//
// RdForwarding.v
//
// EX->EX and MEM->EX forwarding for stores

`default_nettype none
module RdForwarding(
		    // Inputs
		    IDEX_ReadData2, IDEX_MemWrite, IDEX_Rd,
		    EXMEM_Rd, EXMEM_Rs, EXMEM_JumpLink, EXMEM_PCNext,
		    EXMEM_ExecRslt, EXMEM_MemWrite, EXMEM_RegWrite,
		    MEMWB_Rd, MEMWB_Rs, MEMWB_MemWrite, MEMWB_RegWrite,
		    WbtoDecodeWriteData,

		    // Outputs
		    ReadData2Out
		    );

	output wire [15:0] ReadData2Out;

	input wire [15:0]  IDEX_ReadData2;
	input wire	   IDEX_MemWrite;
	input wire [2:0]   IDEX_Rd;

	input wire [2:0]   EXMEM_Rd;
	input wire [2:0]   EXMEM_Rs;
	input wire	   EXMEM_JumpLink;
	input wire [15:0]  EXMEM_PCNext;
	input wire [15:0]  EXMEM_ExecRslt;
	input wire	   EXMEM_MemWrite;
	input wire	   EXMEM_RegWrite;

	input wire [2:0]   MEMWB_Rd;
	input wire [2:0]   MEMWB_Rs;
	input wire	   MEMWB_MemWrite;
	input wire	   MEMWB_RegWrite;

	input wire [15:0]  WbtoDecodeWriteData;

	wire [15:0]	   ExExRslt;
	wire [15:0]	   MemExRslt;
	wire		   isMemEx;
	wire		   isExEx;

	assign ReadData2Out = isExEx ? ExExRslt :
			      (isMemEx ? MemExRslt : IDEX_ReadData2);

	// Link register doesn't save ExecRslt
	assign ExExRslt = EXMEM_JumpLink ? EXMEM_PCNext : EXMEM_ExecRslt;

	// Muxes in Write Back already "do the right thing"
	assign MemExRslt = WbtoDecodeWriteData;

	// ST and STU store whatever is in Rd, so we need to forward
	// that
	assign isExEx = EXMEM_RegWrite & IDEX_MemWrite &
			(((IDEX_Rd == EXMEM_Rd) & ~EXMEM_MemWrite) |	// All other regwrites
			 ((IDEX_Rd == EXMEM_Rs) & EXMEM_MemWrite));	// Special case for from STU ex

	assign isMemEx = MEMWB_RegWrite & IDEX_MemWrite &
			 (((IDEX_Rd == MEMWB_Rd) & ~MEMWB_MemWrite) |	// All other regwrites
			  ((IDEX_Rd == MEMWB_Rs) & MEMWB_MemWrite));	// Special case for from STU mem

endmodule
`default_nettype wire
