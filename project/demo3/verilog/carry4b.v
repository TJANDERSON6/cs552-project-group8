/*
 * Module: carry4b
 * CS552 - Tyler J. Anderson (2023)
 * 
 * Determine carry from generates and propogates
 */

`default_nettype none
module carry4b(g, p, cIn, c, cOut);
   output wire [3:1] c;
   output wire	     cOut;

   input wire [3:0]  g;
   input wire [3:0]  p;
   input wire	     cIn;

   wire [14:0]	     lh;

   // c1 = g0 + (p0 * c0)
   and2 and2_c1 (
		 .out(lh[0]),
		 .in1(p[0]),
		 .in2(cIn)
		 );
   or2 or2_c1 (
	       .out(c[1]),
	       .in1(g[0]),
	       .in2(lh[0])
	       );

   // c2 = g1 + (p1 * g0) + (p1 * p0 * c0)
   and3 and3_c2 (
		 .out(lh[1]),
		 .in1(p[1]),
		 .in2(p[0]),
		 .in3(cIn)
		 );
   and2 and2_c2 (
		 .out(lh[2]),
		 .in1(p[1]),
		 .in2(g[0])
		 );
   or3 or3_c2 (
	       .out(c[2]),
	       .in1(g[1]),
	       .in2(lh[1]),
	       .in3(lh[2])
	       );

   // c3 = g2 + (p2 * g1) + (p2 * p1 * g0) + (p2 * p1 * p0 * cin)
   and2 and2_c3a (
		  .out(lh[3]),
		  .in1(p[0]),
		  .in2(cIn)
		  );
   and3 and3_c3a (
		  .out(lh[4]),
		  .in1(lh[3]),
		  .in2(p[2]),
		  .in3(p[1])
		  );
   and3 and3_c3b (
		  .out(lh[5]),
		  .in1(p[2]),
		  .in2(p[1]),
		  .in3(g[0])
		  );
   and2 and2_c3b (
		  .out(lh[6]),
		  .in1(p[2]),
		  .in2(g[1])
		  );
   or3 or3_c3 (
	       .out(lh[7]),
	       .in1(g[2]),
	       .in2(lh[6]),
	       .in3(lh[5])
	       );
   or2 or2_inst (
		 .out(c[3]),
		 .in1(lh[7]),
		 .in2(lh[4])
		 );

   // cout = g3 + (p3 * g2) + (p3 * p2 * g1) + (p3 * p2 * p1 * g0) + (p3 * p2 * p1 * p0 * cin)
   // lh[9] = (p3 * p2 * p1 * p0 * cin)
   and3 and3_couta (
		    .out(lh[8]),
		    .in1(cIn),
		    .in2(p[0]),
		    .in3(p[1])
		    );
   and3 and3_coutb (
		    .out(lh[9]),
		    .in1(lh[8]),
		    .in2(p[2]),
		    .in3(p[3])
		    );

   // lh[11] = (p3 * p2 * p1 * g0)
   and2 and2_couta (
		    .out(lh[10]),
		    .in1(g[0]),
		    .in2(p[1])
		    );
   and3 and3_coutc (
		    .out(lh[11]),
		    .in1(lh[10]),
		    .in2(p[2]),
		    .in3(p[3])
		    );

   // lh[12] = (p3 * p2 * g1)
   and3 and3_coutd (
		    .out(lh[12]),
		    .in1(p[3]),
		    .in2(p[2]),
		    .in3(g[1])
		    );

   // lh[13] = (p3 * g2)
   and2 and2_coutb (
		    .out(lh[13]),
		    .in1(p[3]),
		    .in2(g[2])
		    );

   // cout = g3 + lh[9] + lh[11] + lh[12] + lh[13]
   or3 or3_couta (
		  .out(lh[14]),
		  .in1(g[3]),
		  .in2(lh[9]),
		  .in3(lh[11])
		  );
   or3 or3_coutb (
		  .out(cOut),
		  .in1(lh[14]),
		  .in2(lh[12]),
		  .in3(lh[13])
		  );

endmodule  
`default_nettype wire
