/* $Author: sinclair $ */
/* $LastChangedDate: 2020-02-09 17:03:45 -0600 (Sun, 09 Feb 2020) $ */
/* $Rev: 46 $ */
`default_nettype none
module proc (/*AUTOARG*/
   // Outputs
   err, 
   // Inputs
   clk, rst
   );

   input wire clk;
   input wire rst;

   output reg err;

   // None of the above lines can be modified

   // OR all the err ouputs for every sub-module and assign it as this
   // err output
   
   // As desribed in the homeworks, use the err signal to trap corner
   // cases that you think are illegal in your statemachines
   
   
   /* your code here -- should include instantiations of fetch, decode, execute, mem and wb modules */

	// Modify these to the actual register sizes
	parameter N_IFID = 35;
	parameter N_IDEX = 123;
	parameter N_EXMEM = 80;
	parameter N_MEMWB = 80;

	// Boot fuse signals
	wire	  boot_Started;

	// Hazard detection signals
	wire	  HD_Stall;
	wire	  HD_Flush;
	wire	  HD_UnalignedHalt;

	// Forwarding signals
	wire	  DecodetoFetchHalted;
	wire [15:0] DecodetoFetchPCAdded;
	wire	    DecodetoFetchJumpSrc;
	wire	    DecodetoFetchJump;
	wire	    DecodetoHazardMemWrite;
	wire [2:0]  DecodetoHazardRd;
	wire [2:0]  WbtoDecodeRd;
	wire [15:0] WbtoDecodeWriteData;
	wire	    WbtoDecodeRegWrite;
	wire [2:0]  DecodetoHazardRs;
	wire [2:0]  DecodetoHazardRt;
	wire	    DecodetoHazardALUSrc;
	wire	    DecodetoHazardBranch;
	wire	    ExecutetoHazardUnaligned;
	wire	    FetchtoHazardFetchDone;

	// IF/ID Pipeline Register
	wire	    IFID_Started;
	wire	    IFID_Halted;
	wire [15:0] IFID_PCNext;
	wire [15:0] IFID_Instruction;
	wire	    IFID_FetchDone;

	// Signals feeding IF/ID pipeline Register
	wire	    Fetch_Halted;
	wire [15:0] Fetch_PCNext;
	wire [15:0] Fetch_Instruction;
	wire	    Fetch_FetchDone;

	// ID/EX Pipeline register
	wire	    IDEX_Halted;
	wire	    IDEX_RegWrite;
	wire	    IDEX_Branch;
	wire	    IDEX_JumpLink;
	wire	    IDEX_JumpSrc;
	wire	    IDEX_MemtoReg;
	wire	    IDEX_MemRead;
	wire	    IDEX_MemWrite;
	wire [15:0] IDEX_PCNext;
	wire [1:0]  IDEX_ALUExt;
	wire	    IDEX_ALUSRC;
	wire	    IDEX_PCSrc;
	wire [1:0]  IDEX_ALUOp;
	wire [15:0] IDEX_PCAdded;
	wire [1:0]  IDEX_Func;
	wire [15:0] IDEX_ReadData1;
	wire [15:0] IDEX_ReadData2;
	wire	    IDEX_Jump;
	wire [15:0] IDEX_ExtImm;
	wire [2:0]  IDEX_Rs;
	wire [2:0]  IDEX_Rt;
	wire [15:0] IDEX_Instruction;
	wire [2:0]  IDEX_Rd;
	wire	    IDEX_ZeroExt;

	// Signals feeding ID/EX Pipeline register
	wire	    Decode_Halted;
	wire	    Decode_RegWrite;
	wire	    Decode_Branch;
	wire	    Decode_JumpLink;
	wire	    Decode_JumpSrc;
	wire	    Decode_MemtoReg;
	wire	    Decode_MemRead;
	wire	    Decode_MemWrite;
	wire [1:0]  Decode_ALUExt;
	wire	    Decode_ALUSRC;
	wire	    Decode_PCSrc;
	wire [1:0]  Decode_ALUOp;
	wire [15:0] Decode_PCAdded;
	wire [1:0]  Decode_Func;
	wire [15:0] Decode_ReadData1;
	wire [15:0] Decode_ReadData2;
	wire	    Decode_Jump;
	wire [15:0] Decode_ExtImm;
	wire [2:0]  Decode_Rs;
	wire [2:0]  Decode_Rt;
	wire [2:0]  Decode_Rd;
	wire	    Decode_ZeroExt;
	wire [15:0] Decode_Instruction;

	// EX/MEM Pipeline register
	wire		  EXMEM_Halted;
	wire		  EXMEM_RegWrite;
	wire		  EXMEM_JumpLink;
	wire		  EXMEM_JumpSrc;
	wire		  EXMEM_MemtoReg;
	wire		  EXMEM_MemRead;
	wire		  EXMEM_MemWrite;
	wire [15:0]	  EXMEM_PCNext;
	wire [15:0]	  EXMEM_ExecRslt;
	wire [2:0]	  EXMEM_Rs;
	wire [2:0]	  EXMEM_Rt;
	wire [15:0]	  EXMEM_ReadData2;
	wire [2:0]	  EXMEM_Rd;
	wire [15:0]	  EXMEM_Instruction;

	// Signals feeding into EX/MEM Pipeline register
	wire [15:0]	  Execute_ExecRslt;
	wire [15:0]	  Execute_ReadData2;

	// MEM/WB Pipeline register
	wire		  MEMWB_Halted;
	wire		  MEMWB_RegWrite;
	wire		  MEMWB_JumpLink;
	wire		  MEMWB_JumpSrc;
	wire		  MEMWB_MemtoReg;
	wire		  MEMWB_MemRead;
	wire		  MEMWB_MemWrite;
	wire [15:0]	  MEMWB_PCNext;
	wire [15:0]	  MEMWB_ReadData;
	wire [15:0]	  MEMWB_ExecRslt;
	wire [2:0]	  MEMWB_Rs;
	wire [2:0]	  MEMWB_Rt;
	wire [2:0]	  MEMWB_Rd;
	wire [15:0]	  MEMWB_Instruction;

	// Signals feeding into MEM/WB Pipeline register
	wire [15:0]	  Memory_ReadData;
	wire		  memStall;

	// Module errors
	wire		  wbErr;
	wire		  memoryErr;
	wire		  executeErr;
	wire		  decodeErr;
	wire		  fetchErr;
	wire		  modErr;

	// Pipeline errors
	wire	    IFID_err;
	wire	    IDEX_err;
	wire	    EXMEM_err;
	wire	    MEMWB_err;
	wire	    boot_err;
	wire	    pRegErr;

	assign FetchtoHazardFetchDone = Fetch_FetchDone;

	// Combine module errors
	assign modErr = wbErr | memoryErr | executeErr | decodeErr | fetchErr;

	// Combine pipeline errors
	assign pRegErr = IFID_err | IDEX_err | EXMEM_err | MEMWB_err | boot_err;

	// Fuse that blows when first instruction is fetched
	f_reg #(
		.N(1)
		) boot_fuse (
			     .dOut(boot_Started),
			     .err(boot_err),
			     .clk(clk),
			     .rst(rst),
			     .dIn(1'b1),
			     .wEn(1'b1)
			     );

	// Hazard Detection system
	hazard_detection hazard_detection_inst (
						.IDEX_PCSrc(IDEX_PCSrc),
						.IDEX_JumpSrc(IDEX_JumpSrc),
						.IDEX_Rd(IDEX_Rd),
						.IDEX_MemtoReg(IDEX_MemtoReg),
						.IDEX_Rs(IDEX_Rs),
						.IDEX_Rt(IDEX_Rt),
						.IDEX_ALUSrc(IDEX_ALUSRC),
						.IDEX_RegWrite(IDEX_RegWrite),
						.IDEX_MemWrite(IDEX_MemWrite),
						.DecodetoFetchJumpSrc(DecodetoFetchJumpSrc),
						.DecodetoHazardMemWrite(DecodetoHazardMemWrite),
						.DecodetoHazardRd(DecodetoHazardRd),
						.DecodetoHazardRs(DecodetoHazardRs),
						.DecodetoHazardRt(DecodetoHazardRt),
						.DecodetoHazardALUSrc(DecodetoHazardALUSrc),
						.DecodetoHazardBranch(DecodetoHazardBranch),
						.DecodetoHazardJump(DecodetoFetchJump),
						.ExecutetoHazardUnaligned(ExecutetoHazardUnaligned),
						.FetchtoHazardFetchDone(FetchtoHazardFetchDone),
						.IFID_FetchDone(IFID_FetchDone),
						.EXMEM_Rd(EXMEM_Rd),
						.EXMEM_MemtoReg(EXMEM_MemtoReg),
						.EXMEM_JumpSrc(EXMEM_JumpSrc),
						.EXMEM_RegWrite(EXMEM_RegWrite),
						.EXMEM_Rs(EXMEM_Rs),
						.EXMEM_MemWrite(EXMEM_MemWrite),
						.MEMWB_Rd(MEMWB_Rd),
						.MEMWB_RegWrite(MEMWB_RegWrite),
						.Clk(clk),
						.Rst(rst),
						.Stall(HD_Stall),
						.Flush(HD_Flush),
						.UnalignedHalt(HD_UnalignedHalt),
						.Err(),
						.memStall(memStall)
						);

	// fetch block
	fetch fetchBlock (
			  .Rst(rst),
			  .HaltedIn(DecodetoFetchHalted),
			  .PCAdded(DecodetoFetchPCAdded),
			  .BrPCAdded(IDEX_PCAdded),
			  .PCSrc(IDEX_PCSrc),
			  .JumpSrc(DecodetoFetchJumpSrc),
			  .Jump(DecodetoFetchJump),
			  .Stall(HD_Stall | memStall),
			  .clk(clk),
			  .JrJumpSrc(EXMEM_JumpSrc),
			  .JrRslt(EXMEM_ExecRslt),
			  .HD_UnalignedHalt(HD_UnalignedHalt),
			  .HaltedOut(Fetch_Halted),
			  .PCNext(Fetch_PCNext),
			  .Instruction(Fetch_Instruction),
			  .FetchtoHazardFetchDone(Fetch_FetchDone),
			  .err(fetchErr)
			  );

	// The IF/ID Register
	ifid_reg #(
		   .N(N_IFID)
		   ) ifid_reg_inst (
				    .InStarted(boot_Started),
				    .InHalted(Fetch_Halted),
				    .InPCNext(Fetch_PCNext),
				    .InInstruction(Fetch_Instruction),
				    .InFetchDone(Fetch_FetchDone),
				    .wEn(~(HD_Stall | memStall) | (Fetch_FetchDone & IDEX_PCSrc) | (HD_Stall & HD_Flush)),
				    .rst(rst),
				    .clk(clk),
				    .OutStarted(IFID_Started),
				    .OutHalted(IFID_Halted),
				    .OutPCNext(IFID_PCNext),
				    .OutInstruction(IFID_Instruction),
				    .OutFetchDone(IFID_FetchDone),
				    .err(IFID_err)
				    );

	// decode block (Sourcing control signals)
	decode decode_inst (
			    .Halted(IFID_Halted),
			    .PCNext(IFID_PCNext),
			    .InInstruction(IFID_Instruction),
			    .Flush(HD_Flush),
			    .WbtoDecodeRd(WbtoDecodeRd),
			    .WbtoDecodeWriteData(WbtoDecodeWriteData),
			    .WbtoDecodeRegWrite(WbtoDecodeRegWrite),
			    .Clk(clk),
			    .Rst(rst | (~IFID_Started)),
			    .HaltedOut(Decode_Halted),
			    .RegWrite(Decode_RegWrite),
			    .Branch(Decode_Branch),
			    .JumpLink(Decode_JumpLink),
			    .JumpSrc(Decode_JumpSrc),
			    .MemtoReg(Decode_MemtoReg),
			    .MemRead(Decode_MemRead),
			    .MemWrite(Decode_MemWrite),
			    .ALUExt(Decode_ALUExt),
			    .ALUSRC(Decode_ALUSRC),
			    .PCSrc(Decode_PCSrc),
			    .ALUOp(Decode_ALUOp),
			    .PCAdded(Decode_PCAdded),
			    .Func(Decode_Func),
			    .ReadData1(Decode_ReadData1),
			    .ReadData2(Decode_ReadData2),
			    .Jump(Decode_Jump),
			    .ExtImm(Decode_ExtImm),
			    .Rs(Decode_Rs),
			    .Rt(Decode_Rt),
			    .Rd(Decode_Rd),
			    .ZeroExt(Decode_ZeroExt),
			    .OutInstruction(Decode_Instruction),
			    .DecodetoFetchHalted(DecodetoFetchHalted),
			    .DecodetoFetchPCAdded(DecodetoFetchPCAdded),
			    .DecodetoFetchJumpSrc(DecodetoFetchJumpSrc),
			    .DecodetoFetchJump(DecodetoFetchJump),
			    .DecodetoHazardMemWrite(DecodetoHazardMemWrite),
			    .DecodetoHazardRd(DecodetoHazardRd),
			    .DecodetoHazardRs(DecodetoHazardRs),
			    .DecodetoHazardRt(DecodetoHazardRt),
			    .DecodetoHazardALUSrc(DecodetoHazardALUSrc),
			    .DecodetoHazardBranch(DecodetoHazardBranch),
			    .Err(decodeErr)
			    );

	// The ID/EX Pipeline register
	idex_reg #(
		   .N(N_IDEX)
		   ) idex_reg_inst (
			   	    .wEn(~(memStall | (HD_Stall & IDEX_PCSrc))), 
				    .InHalted(Decode_Halted),
				    .InRegWrite(Decode_RegWrite),
				    .InBranch(Decode_Branch),
				    .InJumpLink(Decode_JumpLink),
				    .InJumpSrc(Decode_JumpSrc),
				    .InMemtoReg(Decode_MemtoReg),
				    .InMemRead(Decode_MemRead),
				    .InMemWrite(Decode_MemWrite),
				    .InPCNext(IFID_PCNext),
				    .InALUExt(Decode_ALUExt),
				    .InALUSRC(Decode_ALUSRC),
				    .InPCSrc(Decode_PCSrc),
				    .InALUOp(Decode_ALUOp),
				    .InPCAdded(Decode_PCAdded),
				    .InFunc(Decode_Func),
				    .InReadData1(Decode_ReadData1),
				    .InReadData2(Decode_ReadData2),
				    .InJump(Decode_Jump),
				    .InExtImm(Decode_ExtImm),
				    .InRs(Decode_Rs),
				    .InRt(Decode_Rt),
				    .InInstruction(Decode_Instruction),
				    .InRd(Decode_Rd),
				    .InZeroExt(Decode_ZeroExt),
				    .rst(rst | ((HD_Stall | HD_UnalignedHalt) & ~(memStall | IDEX_PCSrc))),
				    .clk(clk),
				    .OutHalted(IDEX_Halted),
				    .OutRegWrite(IDEX_RegWrite),
				    .OutBranch(IDEX_Branch),
				    .OutJumpLink(IDEX_JumpLink),
				    .OutJumpSrc(IDEX_JumpSrc),
				    .OutMemtoReg(IDEX_MemtoReg),
				    .OutMemRead(IDEX_MemRead),
				    .OutMemWrite(IDEX_MemWrite),
				    .OutPCNext(IDEX_PCNext),
				    .OutALUExt(IDEX_ALUExt),
				    .OutALUSRC(IDEX_ALUSRC),
				    .OutPCSrc(IDEX_PCSrc),
				    .OutALUOp(IDEX_ALUOp),
				    .OutPCAdded(IDEX_PCAdded),
				    .OutFunc(IDEX_Func),
				    .OutReadData1(IDEX_ReadData1),
				    .OutReadData2(IDEX_ReadData2),
				    .OutJump(IDEX_Jump),
				    .OutExtImm(IDEX_ExtImm),
				    .OutRs(IDEX_Rs),
				    .OutRt(IDEX_Rt),
				    .OutInstruction(IDEX_Instruction),
				    .OutRd(IDEX_Rd),
				    .OutZeroExt(IDEX_ZeroExt),
				    .err(IDEX_err)
				    );

	// execute block
	execute executeBlock (
			      .ALUExt(IDEX_ALUExt),
			      .ALUSRC(IDEX_ALUSRC),
			      .ALUOp(IDEX_ALUOp),
			      .PCAdded(IDEX_PCAdded),
			      .Func(IDEX_Func),
			      .ReadData1(IDEX_ReadData1),
			      .ReadData2(IDEX_ReadData2),
			      .ExtImm(IDEX_ExtImm),
			      .Rs(IDEX_Rs),
			      .Rt(IDEX_Rt),
			      .Instruction(IDEX_Instruction),
			      .ZeroExt(IDEX_ZeroExt),
			      .IDEX_Rs(IDEX_Rs),
			      .IDEX_Rt(IDEX_Rt),
			      .IDEX_MemRead(IDEX_MemRead),
			      .IDEX_MemWrite(IDEX_MemWrite),
			      .EXMEM_RegWrite(EXMEM_RegWrite),
			      .EXMEM_PCAdded(EXMEM_PCNext),
			      .MEMWB_RegWrite(MEMWB_RegWrite),
			      .EXMEM_MemWrite(EXMEM_MemWrite),
			      .EXMEM_Rd(EXMEM_Rd),
			      .MEMWB_Rd(MEMWB_Rd),
			      .JumpLink(IDEX_JumpLink),
			      .JumpSrc(IDEX_JumpSrc),
			      .EXMEM_JumpLink(EXMEM_JumpLink),
			      .WbtoDecodeWriteData(WbtoDecodeWriteData),
			      .EXMEM_Rs(EXMEM_Rs),
			      .MEMWB_Rs(MEMWB_Rs),
			      .MEMWB_MemWrite(MEMWB_MemWrite),
			      .EXMEM_ExecRslt(EXMEM_ExecRslt),
			      .MEMWB_ExecRslt(MEMWB_ExecRslt),
			      .ExecutetoHazardUnaligned(ExecutetoHazardUnaligned),
			      .Rst(rst),
			      .Clk(clk),
			      .ExecRslt(Execute_ExecRslt),
			      .err(executeErr)
			      );

	// Forward Rd for stores currently in Execute
	RdForwarding RdForwarding_inst (
					.IDEX_ReadData2(IDEX_ReadData2),
					.IDEX_MemWrite(IDEX_MemWrite),
					.IDEX_Rd(IDEX_Rd),
					.EXMEM_Rd(EXMEM_Rd),
					.EXMEM_Rs(EXMEM_Rs),
					.EXMEM_JumpLink(EXMEM_JumpLink),
					.EXMEM_PCNext(EXMEM_PCNext),
					.EXMEM_ExecRslt(EXMEM_ExecRslt),
					.EXMEM_MemWrite(EXMEM_MemWrite),
					.EXMEM_RegWrite(EXMEM_RegWrite),
					.MEMWB_Rd(MEMWB_Rd),
					.MEMWB_Rs(MEMWB_Rs),
					.MEMWB_MemWrite(MEMWB_MemWrite),
					.MEMWB_RegWrite(MEMWB_RegWrite),
					.WbtoDecodeWriteData(WbtoDecodeWriteData),
					.ReadData2Out(Execute_ReadData2)
					);

	// The EX/MEM Pipeline Register
	exmem_reg #(
		    .N(N_EXMEM)
		    ) exmem_reg_inst (
			    	      .wEn(~memStall),
				      .InHalted(IDEX_Halted),
				      .InRegWrite(IDEX_RegWrite),
				      .InJumpLink(IDEX_JumpLink),
				      .InJumpSrc(IDEX_JumpSrc),
				      .InMemtoReg(IDEX_MemtoReg),
				      .InMemRead(IDEX_MemRead),
				      .InMemWrite(IDEX_MemWrite),
				      .InPCNext(IDEX_PCNext),
				      .InExecRslt(Execute_ExecRslt),
				      .InRs(IDEX_Rs),
				      .InRt(IDEX_Rt),
				      .InReadData2(Execute_ReadData2),
				      .InRd(IDEX_Rd),
				      .InInstruction(IDEX_Instruction),
				      .rst(rst | HD_UnalignedHalt),
				      .clk(clk),
				      .OutHalted(EXMEM_Halted),
				      .OutRegWrite(EXMEM_RegWrite),
				      .OutJumpLink(EXMEM_JumpLink),
				      .OutJumpSrc(EXMEM_JumpSrc),
				      .OutMemtoReg(EXMEM_MemtoReg),
				      .OutMemRead(EXMEM_MemRead),
				      .OutMemWrite(EXMEM_MemWrite),
				      .OutPCNext(EXMEM_PCNext),
				      .OutExecRslt(EXMEM_ExecRslt),
				      .OutRs(EXMEM_Rs),
				      .OutRt(EXMEM_Rt),
				      .OutReadData2(EXMEM_ReadData2),
				      .OutRd(EXMEM_Rd),
				      .OutInstruction(EXMEM_Instruction),
				      .err(EXMEM_err)
				      );

	// memory block
	memory memory_inst (
			    .Halted(EXMEM_Halted),
			    .MemRead(EXMEM_MemRead),
			    .MemWrite(EXMEM_MemWrite),
			    .ExecRslt(EXMEM_ExecRslt),
			    .ReadData2(EXMEM_ReadData2),
			    .Rd(EXMEM_Rd),
			    .MEMWB_ReadData(MEMWB_ReadData),
			    .MEMWB_MemtoReg(MEMWB_MemtoReg),
			    .MEMWB_Rd(MEMWB_Rd),
			    .MEMWB_ExecRslt(MEMWB_ExecRslt),
			    .MEMWB_RegWrite(MEMWB_RegWrite),
			    .MEMWB_MemWrite(MEMWB_MemWrite),
			    .MEMWB_Rs(MEMWB_Rs),
			    .WbtoDecodeWriteData(WbtoDecodeWriteData),
			    .memStall(memStall), 
			    .clk(clk),
			    .rst(rst),
			    .ReadData(Memory_ReadData),
			    .err(memoryErr)
			    );

	// The MEM/WB Pipeline Register
	memwb_reg #(
		    .N(N_MEMWB)
		    ) memwb_reg_inst (
			    	      .wEn(~memStall),
				      .InHalted(EXMEM_Halted),
				      .InRegWrite(EXMEM_RegWrite),
				      .InJumpLink(EXMEM_JumpLink),
				      .InJumpSrc(EXMEM_JumpSrc),
				      .InMemtoReg(EXMEM_MemtoReg),
				      .InMemRead(EXMEM_MemRead),
				      .InMemWrite(EXMEM_MemWrite),
				      .InPCNext(EXMEM_PCNext),
				      .InReadData(Memory_ReadData),
				      .InExecRslt(EXMEM_ExecRslt),
				      .InRs(EXMEM_Rs),
				      .InRt(EXMEM_Rt),
				      .InRd(EXMEM_Rd),
				      .InInstruction(EXMEM_Instruction),
				      .rst(rst),
				      .clk(clk),
				      .OutHalted(MEMWB_Halted),
				      .OutRegWrite(MEMWB_RegWrite),
				      .OutJumpLink(MEMWB_JumpLink),
				      .OutJumpSrc(MEMWB_JumpSrc),
				      .OutMemtoReg(MEMWB_MemtoReg),
				      .OutMemRead(MEMWB_MemRead),
				      .OutMemWrite(MEMWB_MemWrite),
				      .OutPCNext(MEMWB_PCNext),
				      .OutReadData(MEMWB_ReadData),
				      .OutExecRslt(MEMWB_ExecRslt),
				      .OutRs(MEMWB_Rs),
				      .OutRt(MEMWB_Rt),
				      .OutRd(MEMWB_Rd),
				      .OutInstruction(MEMWB_Instruction),
				      .err(MEMWB_err)
				      );

	// writeback block
	wb wb_inst (
		    .wEn(~memStall),
		    .Halted(MEMWB_Halted),
		    .RegWrite(MEMWB_RegWrite),
		    .MemWrite(MEMWB_MemWrite),
		    .JumpLink(MEMWB_JumpLink),
		    .JumpSrc(MEMWB_JumpSrc),
		    .MemtoReg(MEMWB_MemtoReg),
		    .PCNext(MEMWB_PCNext),
		    .ReadData(MEMWB_ReadData),
		    .ExecRslt(MEMWB_ExecRslt),
		    .Rs(MEMWB_Rs),
		    .Rd(MEMWB_Rd),
		    .clk(clk),
		    .rst(rst),
		    .WbtoDecodeRd(WbtoDecodeRd),
		    .WbtoDecodeWriteData(WbtoDecodeWriteData),
		    .WbtoDecodeRegWrite(WbtoDecodeRegWrite),
		    .err(wbErr)
		    );

always @(pRegErr, modErr)
  begin
	  casex(pRegErr | modErr)
	    1'b0:
	      begin
		      err = 1'b0;
	      end
	    1'b1:
	      begin
		      err = 1'b1;
	      end
	    default:
	      begin
		      err = 1'b1;
	      end
	  endcase // casex (pRegErr | modErr)
  end // always @ (pRegErr, modErr)
endmodule // proc
`default_nettype wire
// DUMMY LINE FOR REV CONTROL :0:
