/*
*
	* Two-Way Set Associative Cache Instance
*
*/

`default_nettype none
module sa_cache(
		// Outputs
		hit, dirty, tag_out, data_out, valid, err,
		
		// Inputs
		enable, index, offset, comp, write, tag_in, data_in, valid_in, clk, rst, createdump, way_in
); 

	output wire [1:0]	hit; 
	output wire [1:0]	dirty; 
	output wire [4:0]	tag_out; 
	output wire [15:0]	data_out; 
	output wire [1:0]	valid; 
	output wire 		err; 

	input wire 		enable; 
	input wire [7:0]	index; 
	input wire [2:0]	offset; 
	input wire 		comp; 
	input wire 		write; 
	input wire [4:0]	tag_in; 
	input wire [15:0]	data_in; 
	input wire 		valid_in; 
	input wire 		clk; 
	input wire 		rst; 
	input wire 		createdump; 
	input wire 		way_in; 

	// Additional variables
	wire 			hit0; 
	wire 			dirty0; 
	wire [4:0]		tag_out0;
	wire [15:0]		data_out0; 
	wire 			valid0; 
	wire			cerr0; 

	wire 			hit1; 
	wire 			dirty1; 
	wire [4:0]		tag_out1; 
	wire [15:0] 		data_out1; 
	wire 			valid1; 
	wire 			cerr1; 

	parameter memtype = 0;
	cache #(0 + memtype) c0(
				// Outputs
				.hit(hit0), 
				.dirty(dirty0), 
				.tag_out(tag_out0), 
				.data_out(data_out0), 
				.valid(valid0), 
				.err(cerr0), 
				
				// Inputs
				.enable(enable & (comp | ~way_in)), 
				.index(index), 
				.offset(offset), 
				.comp(comp), 
				.write((~way_in | comp) & write), 
				.tag_in(tag_in), 
				.data_in(data_in), 
				.valid_in(valid_in), 
				.clk(clk), 
				.rst(rst), 
				.createdump(createdump)); 
	
	cache #(2 + memtype) c1(
				// Outputs
				.hit(hit1), 
				.dirty(dirty1), 
				.tag_out(tag_out1), 
				.data_out(data_out1), 
				.valid(valid1), 
				.err(cerr1),

				// INputs
				.enable(enable & (comp | way_in)), 
				.index(index), 
				.offset(offset), 
				.comp(comp), 
				.write(write & (comp | way_in)), 
				.tag_in(tag_in), 
				.data_in(data_in), 
				.valid_in(valid_in), 
				.clk(clk), 
				.rst(rst), 
				.createdump(createdump)); 

	

	

	assign hit = {hit1, hit0};
	assign dirty = {dirty1, dirty0};
	assign tag_out = way_in ? tag_out1 : tag_out0; 
	assign data_out = way_in ? data_out1 : data_out0; 
	assign valid = {valid1, valid0};
	assign err = cerr0 | cerr1; 

endmodule
`default_nettype wire
