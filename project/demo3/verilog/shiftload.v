/*
   CS/ECE 552 Spring '23
  
   Filename        : shiftload.v
   Description     : This is the module for the shiftload block in Execute stage.
*/
`default_nettype none
module shiftload (ZeroExt, ALUInA, ExtImm, ShiftLoadRslt);

	input wire ZeroExt;
	input wire [15:0] ALUInA; 
	input wire [15:0] ExtImm; 
	output wire [15:0] ShiftLoadRslt; 

	wire [15:0] updateRslt; 

	assign updateRslt[15:8] = ALUInA[7:0];
	assign updateRslt[7:0] = ExtImm[7:0];

	assign ShiftLoadRslt = ZeroExt ? updateRslt : ExtImm;

endmodule
`default_nettype wire