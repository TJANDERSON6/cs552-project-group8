module memwb_reg (
		  // Inputs
		  InHalted,
		  InRegWrite,
		  InJumpLink,
		  InJumpSrc,
		  InMemtoReg,
		  InMemRead,
		  InMemWrite,
		  InPCNext,
		  InReadData,
		  InExecRslt,
		  InRs,
		  InRt,
		  InRd,
		  InInstruction,
		  rst,
		  clk,
		  wEn, 	

		  // Outputs
		  OutHalted,
		  OutRegWrite,
		  OutJumpLink,
		  OutJumpSrc,
		  OutMemtoReg,
		  OutMemRead,
		  OutMemWrite,
		  OutPCNext,
		  OutReadData,
		  OutExecRslt,
		  OutRs,
		  OutRt,
		  OutRd,
		  OutInstruction,
		  err
		  );

	parameter N = 128;

	input 	wire InHalted;
	input 	wire InRegWrite;
	input 	wire InJumpLink;
	input 	wire InJumpSrc;
	input 	wire InMemtoReg;
	input 	wire InMemRead;
	input 	wire InMemWrite;
	input 	wire [15:0] InPCNext;
	input 	wire [15:0] InReadData;
	input 	wire [15:0] InExecRslt;
	input 	wire [2:0]  InRs;
	input 	wire [2:0]  InRt;
	input 	wire [2:0]  InRd;
	input wire [15:0]   InInstruction;
	input wire	    rst;
	input wire	    clk;
	input wire 		wEn; 

	output 	wire	    OutHalted;
	output 	wire	    OutRegWrite;
	output 	wire	    OutJumpLink;
	output 	wire	    OutJumpSrc;
	output 	wire	    OutMemtoReg;
	output 	wire	    OutMemRead;
	output 	wire	    OutMemWrite;
	output 	wire [15:0] OutPCNext;
	output 	wire [15:0] OutReadData;
	output 	wire [15:0] OutExecRslt;
	output 	wire [2:0]  OutRs;
	output 	wire [2:0]  OutRt;
	output 	wire [2:0]  OutRd;
	output wire [15:0]  OutInstruction;
	output wire	    err;

	f_reg #(
		.N(N)
		) f_reg_inst (
			      .dOut(
				    {
				     OutHalted,
				     OutRegWrite,
				     OutJumpLink,
				     OutJumpSrc,
				     OutMemtoReg,
				     OutMemRead,
				     OutMemWrite,
				     OutPCNext,
				     OutReadData,
				     OutExecRslt,
				     OutRs,
				     OutRt,
				     OutRd,
				     OutInstruction
				     }
				    ),
			      .err(err),
			      .clk(clk),
			      .rst(rst),
			      .dIn(
				   {
				    InHalted,
				    InRegWrite,
				    InJumpLink,
				    InJumpSrc,
				    InMemtoReg,
				    InMemRead,
				    InMemWrite,
				    InPCNext,
				    InReadData,
				    InExecRslt,
				    InRs,
				    InRt,
				    InRd,
				    InInstruction
				    }
				   ),
			      .wEn(wEn)
			      );

endmodule // memwb_reg
