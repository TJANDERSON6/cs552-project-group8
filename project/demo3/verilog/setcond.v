/*
   CS/ECE 552 Spring '23
  
   Filename        : setcond.v
   Description     : This is the module for the setcond block in Execute stage.
*/
`default_nettype none
module setcond(zero, MSB, cOut, A, B, control, SetRslt); 

	input wire zero;		// Set if ALURslt is 0 
	input wire MSB; 		// MSB of ALURslt 
	input wire cOut; 		// Check if generated
	input wire A;			// MSB of A
	input wire B; 			// MSB of B
	input wire [1:0] control;	// Control Signal from Instruction
	output wire [15:0] SetRslt;	// Set value to 16 bits

	wire muxRslt; 

	assign muxRslt = 	(control == 2'b00) ? zero : (
				(control == 2'b01) ? ((A == 1'b1 & B == 1'b0) | ((A == B) & (MSB == 1'b0))) & ~zero : (
				(control == 2'b10) ? zero | (A == 1'b1 & B == 1'b0) | ((A == B) & (MSB == 1'b0)) : (
				(control == 2'b11) ? cOut : 1'b0)));

	assign SetRslt = {15'b0, muxRslt};

endmodule
`default_nettype wire
