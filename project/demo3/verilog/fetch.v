/*
   CS/ECE 552 Spring '23

   Filename        : fetch.v
   Description     : This is the module for the overall fetch stage of the processor.
*/
`default_nettype none
module fetch (
		// inputs
			Rst, HaltedIn, PCAdded, BrPCAdded, PCSrc, JumpSrc, Jump, Stall, clk, JrJumpSrc, JrRslt,
	      		HD_UnalignedHalt,
		// outputs
			HaltedOut, PCNext, Instruction,
			FetchtoHazardFetchDone,
			err
		);

	// Input
	input wire 		Rst;
	input wire 		HaltedIn;
	input wire [15:0] 	PCAdded;
	input wire [15:0] 	BrPCAdded;
	input wire	  	PCSrc;
	input wire	  	JumpSrc;
	input wire	  	Jump;
	input wire	  	Stall;
	input wire	  	clk;
	input wire	  	JrJumpSrc;
	input wire [15:0] 	JrRslt;
	input wire		HD_UnalignedHalt;

	// Outputs
	output wire	  	HaltedOut;
	output wire [15:0] 	PCNext;
	output wire [15:0] 	Instruction;
	output wire		FetchtoHazardFetchDone;
	output wire	   	err;

	// additional variables
	wire [15:0]	   	PC;		// result of mux from left to right
	wire [15:0]	   	PCRead;	// Value of PC read from register
	wire [15:0]		PCSave;		// Save the Next PC if memory is ready
	wire	    		cOut;		// Indicates i-mem overflow error
	wire [15:0] 		InstrNext;
	wire	    		PcErr;		// Driven by PC reg
	wire	    		PCEn;
	wire [15:0] 		d2_muxAddr, d2_muxExMem, newPC;
	wire 			muxSelect;
	wire unaligned;
	wire InstUnaligned;
	wire			InstructionStall;
	wire 			CacheHit;
	wire [15:0]		NewPC;
	wire			MemDone;
	wire			MemStall;

	// Combine error conditions
	assign err = PcErr | cOut;

	// fetch next instruction
	cla16b #(
		 .N(16)
		 ) cla16b_inst (
				.sum(PCNext),
				.cOut(cOut), // Overflow err if set (out of i-mem)
				.inA(PC),
				.inB(16'h2),
				.cIn(1'b0)
				);

	// Program counter register:
	//
	// - Will only update if PCEn is set
	// - Will reset to zero if RST is asserted
	f_reg #(
		.N(16)
		) pc_reg (
			  .dOut(PCRead),
			  .err(PcErr),
			  .clk(clk),
			  .rst(Rst),
			  .dIn(PCSave),
			  .wEn(PCEn)
			  );

	mem_system #(
		     .N(1)
		     ) imem (
			     // Outputs
			     .DataOut(InstrNext),
			     .Done(MemDone),
			     .Stall(MemStall),
			     .CacheHit(CacheHit),
			     .err(),
			     // Inputs
			     .Addr(PC),
			     .DataIn(16'h0),
			     .Rd(1'b1),
			     .Wr(1'b0),
			     .createdump(1'b0),
			     .clk(clk),
			     .rst(Rst)
			     );

	// Check for data or instruction unaligned access
	assign unaligned = HD_UnalignedHalt | InstUnaligned;
	assign InstUnaligned = PC[0] == 1;

	// Fetch must stall if instruction isn't ready
	assign InstructionStall = ~MemDone;

	// Forward done signal to hazard detection for branch-taken
	assign FetchtoHazardFetchDone = MemDone;

	// If halted, send NOP, not next instruction, or if unaligned access,
	// send halt
	assign Instruction = unaligned ? 16'h0 : (~PCEn | InstructionStall ? 16'h0800 : InstrNext);

	assign muxSelect = (~JumpSrc & Jump) | JrJumpSrc;
	assign d2_muxAddr = PCSrc ? BrPCAdded : PCRead;
	assign d2_muxExMem = JrJumpSrc ? JrRslt : PCAdded;
	assign PC = muxSelect ? d2_muxExMem : d2_muxAddr;

	// Need to save branch/Jump PC if memory is not ready
	assign PCEn = ~(HaltedIn | Stall | InstructionStall | JumpSrc) |
		      (muxSelect | PCSrc) & InstructionStall;

	assign HaltedOut = Rst ? 1'b0 : HaltedIn;

	// Save branch jump pc if mem is stalling
	assign PCSave = (muxSelect | PCSrc) & (InstructionStall | (MemStall & JrJumpSrc)) ? PC : PCNext;

endmodule
`default_nettype wire
