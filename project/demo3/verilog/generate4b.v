/*
 * Module: generate4b 
 * CS552 - Tyler J. Anderson (2023)
 * 
 * Calculate upper level generates and propogates for
 *  4bin CLA modules
 */

`default_nettype none
module generate4b(gIn, pIn, GOut, POut);
   input wire [3:0] gIn;
   input wire [3:0] pIn;

   output wire	    GOut;
   output wire	    POut;

   wire [4:0]	    gp; // General net bus for generate
   wire		    pp; // General net bus for propogate

   // GENERATE

   // G0 = g3 + (p3*g2) + (p3*p2*g1) + (p3*p2*p1*g0)
   // gp[1] = (p3*p2*p1*g0)
   and3 and3_Ga (
		 .out(gp[0]),
		 .in1(pIn[3]),
		 .in2(pIn[2]),
		 .in3(pIn[1])
		 );
   and2 and2_Ga (
		 .out(gp[1]),
		 .in1(gp[0]),
		 .in2(gIn[0])
		 );

   // gp[2] = (p3*p2*g1)
   and3 and3_Gb (
		 .out(gp[2]),
		 .in1(pIn[3]),
		 .in2(pIn[2]),
		 .in3(gIn[1])
		 );

   // gp[3] = (p3*g2)
   and2 and2_Gb (
		 .out(gp[3]),
		 .in1(pIn[3]),
		 .in2(gIn[2])
		 );

   // GOut = g3 + gp[3] + gp[2] + gp[1]
   or3 or3_G (
	      .out(gp[4]),
	      .in1(gIn[3]),
	      .in2(gp[3]),
	      .in3(gp[2])
	      );
   or2 or2_G (
	      .out(GOut),
	      .in1(gp[4]),
	      .in2(gp[1])
	      );

   // PROPOGATE

   // P0 = and(p[3:0])
   and3 and3_P (
		.out(pp),
		.in1(pIn[0]),
		.in2(pIn[1]),
		.in3(pIn[2])
		);
   and2 and2_P (
		.out(POut),
		.in1(pp),
		.in2(pIn[3])
		);
endmodule // generate4b
`default_nettype wire
