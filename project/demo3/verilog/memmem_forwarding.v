`default_nettype none
module memmem_forwarding(
			 // inputs
			 EXMEM_ReadData2, EXMEM_Rd, EXMEM_MemWrite, EXMEM_ExecRslt,
			 MEMWB_ReadData, MEMWB_MemtoReg, MEMWB_Rd, MEMWB_ExecRslt,
			 MEMWB_RegWrite, MEMWB_MemWrite, MEMWB_Rs, WbtoDecodeWriteData,
			 Clk, Rst, Err,
			 // outputs
			 ReadData2Out
			 );

	input wire [15:0] EXMEM_ReadData2;
	input wire [2:0]  EXMEM_Rd;
	input wire	  EXMEM_MemWrite;
	input wire [15:0] EXMEM_ExecRslt;
	input wire [15:0] MEMWB_ReadData;
	input wire	  MEMWB_MemtoReg;
	input wire [2:0]  MEMWB_Rd;
	input wire [15:0] MEMWB_ExecRslt;
	input wire	  MEMWB_RegWrite;
	input wire	  MEMWB_MemWrite;
	input wire [2:0]  MEMWB_Rs;
	input wire [15:0] WbtoDecodeWriteData;
	input wire	  Clk;
	input wire	  Rst;
	input wire	  Err;

	output wire [15:0] ReadData2Out;

	wire		   isMemtoMem;

	assign Err = 1'b0;

	// Chose forwarded value if conditions match
	assign ReadData2Out = isMemtoMem ? WbtoDecodeWriteData : EXMEM_ReadData2;

	assign isMemtoMem = EXMEM_MemWrite & MEMWB_RegWrite &
			    (((EXMEM_Rd == MEMWB_Rd) & ~MEMWB_MemWrite) |
			     ((EXMEM_Rd == MEMWB_Rs) & MEMWB_MemWrite));
endmodule // memmem_forwarding
`default_nettype wire
