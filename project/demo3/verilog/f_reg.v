module f_reg(dOut, err, clk, rst, dIn, wEn);

   parameter N = 16;

   output wire [N-1:0]	dOut;
   output wire		err;

   input wire		clk;
   input wire		rst;
   input wire [N-1:0]	dIn;
   input wire		wEn;

   wire [N-1:0]		dffIn;
   wire [N-1:0]		dffState;		

   // Each bit will be a flip-flop
   dff dff_inst[N-1:0] (
			.q(dOut),
			.d(dffIn),
			.clk(clk),
			.rst(rst)
			);

   // Enable input to DFF if write is enabled
   assign dffIn = wEn ? dIn : dOut;

   // Assert err if input is unknown
   assign err = (&{dIn, rst, wEn}) === 1'bx ? 1'b1 : 1'b0;
endmodule
