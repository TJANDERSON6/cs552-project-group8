/*
 * Hazard Detection
 * CS552 Sp23 Group 8
 * 
 * Tyler Anderson and Ryuki Koda
 *
 * Pipeline Hazards Detection and Mitigation
 * 
 * 
 */

`default_nettype none
module hazard_detection (
			 // Inputs
			 DecodetoFetchJumpSrc, DecodetoHazardMemWrite,
			 DecodetoHazardRd, DecodetoHazardRs, DecodetoHazardRt,
			 DecodetoHazardALUSrc, DecodetoHazardBranch,
			 DecodetoHazardJump,
			 ExecutetoHazardUnaligned, FetchtoHazardFetchDone,
			 IFID_FetchDone,
			 IDEX_JumpSrc, IDEX_PCSrc, IDEX_Rd, IDEX_MemtoReg,
			 IDEX_Rs, IDEX_Rt, IDEX_ALUSrc, IDEX_RegWrite, IDEX_MemWrite,
			 EXMEM_Rd, EXMEM_MemtoReg, EXMEM_JumpSrc, EXMEM_RegWrite,
			 EXMEM_Rs, EXMEM_MemWrite,
			 MEMWB_RegWrite, MEMWB_Rd,
			 Clk, Rst, memStall,
			 // Outputs
			 Stall, Flush, UnalignedHalt, Err
			 );

	parameter NS = 8;
	parameter NF = 2;

	input wire DecodetoFetchJumpSrc;
	input wire DecodetoHazardMemWrite;
	input wire [2:0] DecodetoHazardRd;
	input wire [2:0] DecodetoHazardRs;
	input wire [2:0] DecodetoHazardRt;
	input wire	 DecodetoHazardALUSrc;
	input wire	 DecodetoHazardBranch;
	input wire	 DecodetoHazardJump;
	input wire	 ExecutetoHazardUnaligned;
	input wire	 FetchtoHazardFetchDone;
	input wire	 IFID_FetchDone;
	input wire IDEX_JumpSrc;
	input wire IDEX_PCSrc;
	input wire [2:0] IDEX_Rd;
	input wire	 IDEX_MemtoReg;
	input wire [2:0] IDEX_Rs;
	input wire [2:0] IDEX_Rt;
	input wire	 IDEX_ALUSrc;
	input wire	 IDEX_RegWrite;
	input wire	 IDEX_MemWrite;
	input wire [2:0] EXMEM_Rd;
	input wire	 EXMEM_MemtoReg;
	input wire	 EXMEM_JumpSrc;
	input wire	 EXMEM_RegWrite;
	input wire [2:0] EXMEM_Rs;
	input wire	 EXMEM_MemWrite;
	input wire	 MEMWB_RegWrite;
	input wire [2:0] MEMWB_Rd;
	input wire Clk;
	input wire Rst;
	input wire memStall;

	output wire Stall;
	output wire Flush;
	output wire UnalignedHalt;
	output wire Err;

	wire	    jumpRegActive;

	// Stall cases to OR together
	wire [NS - 1:0]	scase;

	// Flush cases to OR together
	wire [NF - 1:0]	fcase;

	// Latch if there is a jump register
	f_reg #(
		.N(1)
		) f_reg_inst (
			      .dOut(jumpRegActive),
			      .err(),
			      .clk(Clk),
			      .rst(Rst | IFID_FetchDone),
			      .dIn(1'b1),
			      .wEn(EXMEM_JumpSrc)
			      );

	// TODO: Add hazard detection cases
	// Case for jump register (stall 2 cycles, flush)
	assign scase[0] = IDEX_JumpSrc | (jumpRegActive & FetchtoHazardFetchDone);
	assign fcase[1] = jumpRegActive;

	// LD followed by ST with 1 instruction in between (1 stall)
	assign scase[1] = DecodetoHazardMemWrite &
			  (DecodetoHazardRd === EXMEM_Rd) &
			  EXMEM_MemtoReg;

	// Use-after-load will require 2 stall for MEM/EX forwarding
	assign scase[2] = ((IDEX_MemtoReg &
			    ((DecodetoHazardRs == IDEX_Rd) |
			     ((DecodetoHazardRt == IDEX_Rd) & ~DecodetoHazardALUSrc)) &
			    ~DecodetoHazardBranch) |
			   (EXMEM_MemtoReg &
			    ((DecodetoHazardRs == EXMEM_Rd) |
			     ((DecodetoHazardRt == EXMEM_Rd) & ~DecodetoHazardALUSrc)))) &
			  (~DecodetoHazardJump |
			   (DecodetoHazardJump & DecodetoFetchJumpSrc));

	// Case for Branch-taken (Flush next decode)
	assign fcase[0] = IDEX_PCSrc;

	// Stall if Branch is taken and I memory is stalling
	assign scase[3] = IDEX_PCSrc & ~IFID_FetchDone;

	// Branch wait for decision
	assign scase[7] = DecodetoHazardBranch &
			  (((DecodetoHazardRs == IDEX_Rd) & IDEX_RegWrite) |
			   ((DecodetoHazardRs == EXMEM_Rd) & EXMEM_RegWrite));
	assign scase[6:4] = 4'b0; // Reserved

	// dummy implementation
	assign Stall = Rst ? 1'b0 : (|scase | memStall); // & ~UnalignedHalt;

	// Need to flush all regs to stop an unaligned access
	assign Flush = Rst ? 1'b0 : (|fcase) | ExecutetoHazardUnaligned;

	// Tell Wb to halt on next cycle
	assign UnalignedHalt = ExecutetoHazardUnaligned & ~memStall;
	
	assign Err = 1'b0;

endmodule // hazard_detection
`default_nettype wire
