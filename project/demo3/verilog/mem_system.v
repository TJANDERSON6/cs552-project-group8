/* $Author: karu $ */
/* $LastChangedDate: 2009-04-24 09:28:13 -0500 (Fri, 24 Apr 2009) $ */
/* $Rev: 77 $ */

`default_nettype none
module mem_system(/*AUTOARG*/
   // Outputs
   DataOut, Done, Stall, CacheHit, err,
   // Inputs
   Addr, DataIn, Rd, Wr, createdump, clk, rst
   );

	parameter N = 1;
   
   input wire [15:0] 	Addr;
   input wire [15:0] 	DataIn;
   input wire        	Rd;
   input wire        	Wr;
   input wire        	createdump;
   input wire        	clk;
   input wire        	rst;
   
   output wire [15:0] 	DataOut;
   output wire        	Done;
   output wire        	Stall;
   output wire      	CacheHit;
   output wire     	err;

   // additional variables 
   
   	wire 		sm_ready; 
	wire		sm_comp; 
	wire 		sm_wb; 
	wire 		sm_mem;
	wire [2:0] 	sm_cache_offset; 
	wire [2:0]	sm_mem_offset; 
	wire 		sm_done; 
	wire 		sm_crd;
	wire 		sm_memrd; 
	wire		sm_cwr; 
	wire 		sm_memwr; 
	wire 		sm_err;
	wire 		sm_request;
       	wire		sm_wr; 
	wire 		sm_rd;
	wire		sm_read;

	wire [15:0]	cDataIn; 
	wire [1:0]	sa_cacheHit; 
	wire [1:0]	sa_dirty; 
	wire [4:0]	sa_tagVictim;
	wire [15:0]	sa_dataOut; 
	wire [1:0]	sa_valid;
	wire 		sa_err;
	wire [2:0]	cacheOffset; 
		
	wire [15:0] 	mem_addr; 
	wire [15:0]	mem_dataOut; 
	wire 		mem_wait; 
	wire		mem_err; 

	wire [15:0]	DataNew;
	wire [15:0]	AddrNew; 
	wire 		WrNew; 
	wire		RdNew; 
	wire 		VictimWay; 
	wire [4:0]	TagNew; 
	wire [7:0]	IndexNew; 
	wire [2:0] 	OffsetNew; 
	wire [4:0]	tag; 
	wire [7:0]	index; 
	wire [2:0]	offset;
	wire 		request; 	
	wire 		WrWay; 
	wire		WrHit;  

   /* data_mem = 1, inst_mem = 0 *
    * needed for cache parameter */

   cache_sm cache_set_associative(
				// Outputs
				.ready(sm_ready), 
				.comp(sm_comp), 
				.wb(sm_wb), 
				.mem(sm_mem), 
				.coffset(sm_cache_offset),
				.moffset(sm_mem_offset), 
				.way(WrWay), 
				.done(sm_done), 
				.crd(sm_crd), 
				.memrd(sm_memrd), 
				.cwr(sm_cwr),
				.memwr(sm_memwr), 
				.err(sm_err),
				.request(sm_request),
				.lwr(sm_wr),
				.lrd(sm_rd), 
				.vway(VictimWay),
				.read(sm_read),
				// Inputs
				.dirty(sa_dirty), 
				.valid(sa_valid), 
				.cwait(mem_wait), 
				.rd(Rd), 
				.wr(Wr), 
				.rst(rst), 
				.clk(clk), 
				.hit(sa_cacheHit),
				.request_in(request)); 


   sa_cache sa_cache_inst(
   				// Outputs
				.hit(sa_cacheHit), 
				.dirty(sa_dirty),
				.tag_out(sa_tagVictim), 
				.data_out(sa_dataOut), 
				.valid(sa_valid), 
				.err(sa_err), 

				// Inputs
				.enable(sm_crd | sm_cwr), 
				.index(IndexNew), 
				.offset(cacheOffset), 
				.comp(sm_comp), 
				.write(sm_cwr), 
				.tag_in(TagNew), 
				.data_in(cDataIn), 
				.valid_in(1'b1), 
				.clk(clk), 
				.rst(rst), 
				.createdump(createdump),
				.way_in(WrWay)); 
	
   four_bank_mem mem(// Outputs
                     .data_out          (mem_dataOut),
                     .stall             (mem_wait),
                     .busy              (),
                     .err               (mem_err),
                     // Inputs
                     .clk               (clk),
                     .rst               (rst),
                     .createdump        (createdump),
                     .addr              (mem_addr),
                     .data_in           (sa_dataOut),
                     .wr                (sm_memwr),
                     .rd                (sm_memrd));
  
   memsys_reg memsys_reg_inst(
   			// Outputs 
			.wr_out(WrNew), 
			.rd_out(RdNew), 
			.addr_out(AddrNew), 
			.data_out(DataNew), 
			.req(request), 
			.err(),
			// Inputs 
			.clk(clk), 
			.rst(rst), 
			.en(sm_read), 
			.wr_in(Wr), 
			.rd_in(Rd), 
			.addr_in(Addr), 
			.data_in(DataIn));
   


   // your code here
	assign cDataIn = (sm_wb | sm_mem) ? mem_dataOut : DataIn; 
   
	assign TagNew = AddrNew[15:11];
	assign IndexNew = AddrNew[10:3];
	assign OffsetNew = AddrNew[2:0];

	// Only send addr if actually reading or writing (avoids
	// random unaligned accesses false-triggering
	assign cacheOffset = ~(sm_crd | sm_cwr) ? 3'b0 :
			     ((sm_wb | sm_mem) ? sm_cache_offset : OffsetNew);

	assign tag = sm_wb ? sa_tagVictim : TagNew; 
	assign index = IndexNew; 
	assign offset = (sm_wb | sm_mem) ? sm_mem_offset : OffsetNew; 
	assign mem_addr = {tag, index, offset};

	assign WrHit = WrWay ? sa_cacheHit[1] : sa_cacheHit[0];

	assign CacheHit = (|(sa_cacheHit & sa_valid)) & sm_request; 
	assign DataOut = (sm_wr & sm_rd) ? DataNew : sa_dataOut; 
	assign Stall = ~sm_ready & (sm_rd | sm_wr); 
	assign Done = sm_done; 
	assign err = sm_err | sa_err | mem_err; 



endmodule // mem_system
`default_nettype wire
// DUMMY LINE FOR REV CONTROL :9:
