/*
   CS/ECE 552, Spring '23
   Homework #3, Problem #1
  
   This module creates a 16-bit register file.  It has 1 write port, 2 read
   ports, 3 register select inputs, a write enable, a reset, and a clock
   input.  All register state changes occur on the rising edge of the
   clock. 
*/
`default_nettype none
module rf (
           // Outputs
           read1OutData, read2OutData, err,
           // Inputs
           clk, rst, read1RegSel, read2RegSel, writeRegSel, writeInData, writeEn
           );

   parameter N = 16;
   parameter RI = 3;
   parameter NR = 8;

   input wire clk, rst;
   input wire [RI-1:0] read1RegSel;
   input wire [RI-1:0] read2RegSel;
   input wire [RI-1:0] writeRegSel;
   input wire [N-1:0]  writeInData;
   input wire	       writeEn;

   output wire [N-1:0] read1OutData;
   output wire [N-1:0] read2OutData;
   output wire	       err;

   wire [N*NR-1:0]     regRead;
   // wire [N-1:0]	       regWrite[NR-1:0];
   wire [NR-1:0]       regWrEn;
   wire		       badinput;
   wire [NR-1:0]       regErr;

   /* YOUR CODE HERE */
   // Our array of registers, note bus order very important
   f_reg #(
	   .N(N)
	   ) f_reg_inst [NR-1:0] (
				  .dOut(regRead),
				  .err(regErr), /* TODO: Deal with error Stuff */
 				  .clk(clk),
				  .rst(rst),
				  .dIn(writeInData),
				  .wEn(regWrEn)
				  );

   // Select read reg 1 and read reg 2. The encode and out must be in
   // the same order. Will almost certainly break if change RI or NR
   regread_mux #(
		 .N(N),
		 .P(NR),
		 .E(RI)
		 ) regread_mux_inst [1:0] (
					   .out({read2OutData, read1OutData}),
					   .encode({read2RegSel, read1RegSel}),
					   .in(regRead)
					   );

   // Shift bits for the register to write to pass into the register
   // array.  Also will break if RI is changed
   regwrite_decode #(
		     .N(NR),
		     .RI(RI)
		     ) regwrite_decode_inst (
					     .out(regWrEn),
					     .writeEn(writeEn),
					     .writeRegSel(writeRegSel)
					     );

   // Check inputs for unknown-ness
   assign err = |regErr;

endmodule
`default_nettype wire
