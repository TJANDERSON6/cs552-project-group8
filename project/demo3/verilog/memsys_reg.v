/*
 * Memory system register
 * CS552 Sp23 Group 8
 * 
 * Tyler Anderson and Ryuki Koda
 *
 * Store data for the current write or read operation
 * 
 */

`default_nettype none
module memsys_reg(
		// inputs
		clk, rst, en,
		wr_in, rd_in, addr_in, data_in,
		// outputs
		wr_out, rd_out, addr_out, data_out, req,
		err
		);

	output wire [15:0] data_out;
	output wire [15:0] addr_out;
	output wire	   rd_out;
	output wire	   wr_out;
	output wire	   err;
	output wire	   req;

	input wire [15:0]  data_in;
	input wire [15:0]  addr_in;
	input wire	   rd_in;
	input wire	   wr_in;
	input wire	   en;
	input wire	   rst;
	input wire	   clk;

	wire		   rd_save;
	wire		   wr_save;
	wire [15:0]	   data_save;
	wire [15:0]	   addr_save;
	wire		   req_save;
	wire		   req_in;
	wire [3:0]	   rerr;

	f_reg #(
		.N(16)
		) data_reg (
			    .dOut(data_save),
			    .err(rerr[0]),
			    .clk(clk),
			    .rst(rst),
			    .dIn(data_in),
			    .wEn(en)
			    );

	f_reg #(
		.N(16)
		) addr_reg (
			    .dOut(addr_save),
			    .err(rerr[1]),
			    .clk(clk),
			    .rst(rst),
			    .dIn(addr_in),
			    .wEn(en)
			    );

	f_reg #(
		.N(1)
		) wr_reg (
			  .dOut(wr_save),
			  .err(rerr[2]),
			  .clk(clk),
			  .rst(rst),
			  .dIn(wr_in),
			  .wEn(en)
			  );

	f_reg #(
		.N(1)
		) rd_reg (
			  .dOut(rd_save),
			  .err(rerr[3]),
			  .clk(clk),
			  .rst(rst),
			  .dIn(rd_in),
			  .wEn(en)
			  );

	f_reg #(
		.N(1)
		) req_reg (
			   .dOut(req),
			   .err(),
			   .clk(clk),
			   .rst(rst),
			   .dIn(rd_in),
			   .wEn(en)
			   );

	// Bypass saved values if set
	assign wr_out = en ? wr_in : wr_save;
	assign rd_out = en ? rd_in : rd_save;
	assign data_out = (wr_in | rd_in) & en ? data_in : data_save;
	assign addr_out = (wr_in | rd_in) & en ? addr_in : addr_save;

	// Indicate request by checking en
	assign req_in = wr_in | rd_in;

	assign err = |rerr;

endmodule // cache_sm
`default_nettype wire
