module fetch_bench();

   // Our time keepers
   wire clk, err, rst;

   // Simulated control signals
   reg	halted, dump, pcsrc, pcen;
   reg  halted_set, dump_set, pcsrc_set, pcen_set;
   reg [15:0] pcbr, pcbr_set;

   // Wires to connect regs
   wire   halted_c, dump_c, pcsrc_c, pcen_c;
   wire [15:0] pcbr_c;

   // The results of fetch
   wire [15:0] pcnext, instruction;
   wire        fetchErr;

   // Simulation meta
   reg	       fail;
   reg [15:0]  pcnext_exp;
   reg [15:0]  pcnow;
   reg [15:0]  instruction_exp;

   assign err = fetchErr;
   assign {halted_c, dump_c, pcsrc_c, pcen_c, pcbr_c} = {halted, dump, pcsrc, pcen, pcbr};

   clkrst clkrst_inst (
		       .clk(clk),
		       .rst(rst),
		       .err(err)
		       );

   fetch fetch_inst (
		     .RST(rst),
		     .Halted(halted_set),
		     .Dump(dump_set),
		     .PCBr(pcbr_set),
		     .PCSrc(pcsrc_set),
		     .PCEn(pcen_set),
		     .clk(clk),
		     .PCNext(pcnext),
		     .Instruction(instruction),
		     .err(fetchErr)
		     );

   initial
     begin
	halted = 0;
	dump = 0;
	pcbr = 16'h0;
	pcsrc = 0;
	pcen = 1;
	fail = 0;
	pcnow = 16'h0;
	halted_set = 0;
	dump_set = 0;
	pcbr_set = 16'h0;
	pcsrc_set = 0;
	pcen_set = 1;
        instruction_exp = 16'hX;

	// Test instruction incrementation
	$display("Incrementing pc for normal instructions");

	#1000
	  begin
	     // Test branch instructions
	     $display("Branching PC");
	     halted = 0;
	     dump = 0;
	     pcbr = 16'h0; // randomize in always
	     pcsrc = 1; // Indicate branch
	     pcen = 1; // Allow PC to update
	  end // for normal

	#2000
	  begin
	     // Test halt
	     $display("Testing HALT");
	     halted = 1; // Halt signal is raised
	     dump = 1; // Dump memory image
	     pcsrc = 0; // TODO test that this value doesn't matter
	     pcen = 1; // PC can update for execution of HALT
	  end
	

	#3000
	  begin
	     // Test nop after halt signal
	     $display("Operation after halt executes");
	     halted = 1; // halted latches after execution
	     dump = 0; // Dump will not be executed on NOPs
	     pcsrc = 0; // Shouldn't matter
	     pcen = 0; // Will be deasserted when Control sees NOP
	  end

	#4000
	  if (fail == 0)
	    begin
	       $display("TEST SUCCEEDED");
	    end
	  else
	    begin
	       $display("TEST FAILED");
	    end
	$finish;
     end // initial begin

   always @(posedge clk)
     begin
	if (rst == 1'b1)
	  begin
		  // If rst asserted, PC stuck at 0x0
		  pcnext_exp = 16'h2;
	  end
	if (pcen_set == 1'b0)
	  begin
	     // PC should not increment
	     pcnext_exp = pcnow;
	  end
	else if ((~halted_set & pcsrc_set) == 1'b1)
	  begin
	     // Branching, PC should be the random pcbr
	     pcbr[14:1] = $random;
	     pcnext_exp = pcbr_set + 2;
	  end
	else
	  begin
	     // Normal situation, pc increments +2
	     pcnext_exp = pcnow + 2;
	  end // else: !if((~halted & pcsrc) == 1'b1)
     end // always @ (posedge clk)

   always @(negedge clk)
     begin
	     if (rst == 1'b0)
	       begin
		       pcnow = pcnext_exp; // Set current pc
	       end
	     else
	       begin
		       pcnow = 16'h0; // reset PC on reset
	       end
	// TODO Test for pre-loaded instructions
	if (instruction != instruction_exp)
	  begin
		  fail = 1'b1;
		  $display("ERROR: instruction: 0x%x, instruction_exp: 0x%x",
			   instruction, instruction_exp);
	  end
	if (pcnext !== pcnext_exp)
	  begin
	     fail = 1'b1;
	     $display("ERROR: pcnext_exp: 0x%x, pcnext: 0x%x, halted: %d, pcen: %d, pcsrc: %d, dump: %d, pcbr: 0x%x",
		      pcnext_exp, pcnext, halted, pcen, pcsrc, dump, pcbr);
	  end

	// Set signals for next cycle
	halted_set = halted_c;
	dump_set = dump_c;
	pcbr_set = pcbr_c;
	pcsrc_set = pcsrc_c;
	pcen_set = pcen_c;

	// If we can deduce instruction from control, set that
	if (halted_c === 1'b1 && pcen_c === 1'b0)
	  begin
		  // Should be NOP when halted and PC disabled
		  instruction_exp = 16'h0800;
	  end
     end
endmodule // fetch_bench
