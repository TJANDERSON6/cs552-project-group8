/*
   CS/ECE 552 Spring '23
  
   Filename        : memory.v
   Description     : This module contains all components in the Memory stage of the 
                     processor.
*/
`default_nettype none
module memory (
		// inputs 
			PCAdded, ExecRslt, JumpSrc, Jump, BrRslt, Branch, Dump, MemWrite, MemRead, ReadData2, clk, rst,
		// outputs
			PCBr, PCSrc, ReadData
		);

   	input wire [15:0] PCAdded; 
	input wire [15:0] ExecRslt; 
	input wire JumpSrc; 
	input wire Jump; 
	input wire BrRslt; 
	input wire Branch; 
	input wire Dump; 
	input wire MemWrite; 
	input wire MemRead; 
	input wire [15:0] ReadData2; 
	input wire clk;
	input wire rst; 
	
	output wire [15:0] PCBr; 
	output wire PCSrc; 
	output wire [15:0] ReadData; 

	// additional variables
	wire [15:0] jumpMux; 

	assign jumpMux = JumpSrc ? ExecRslt : PCAdded;

	assign PCBr = Jump ? jumpMux : PCAdded;

	and2 pcsrc(.out(PCSrc), .in1(Branch), .in2(BrRslt)); 

	/* Haven't configure memory block. 
		Not yet understand memory2c.v and memory2c.syn.v
	*/
	memory2c mem(.data_out(ReadData), .data_in(ReadData2), .addr(ExecRslt), .enable(MemRead|MemWrite), .wr(MemWrite), .createdump(Dump), .clk(clk), .rst(rst)); 
  
endmodule
`default_nettype wire
