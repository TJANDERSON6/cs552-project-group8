/*
   CS/ECE 552 Spring '23
  
   Filename        : alu.v
   Description     : This is the module for the alu block in Execute stage.
*/
`default_nettype none
module alu(ALUInA, ALUInB, ALUOp, Func, zero, cOut, ALURslt, A, B); 

	input wire [15:0] ALUInA;	// ALU input A 
	input wire [15:0] ALUInB; 	// ALU input B
	input wire [1:0] ALUOp; 	// ALU Opcode 
	input wire [1:0] Func; 		// Function code
	output wire zero; 		// ALURslt == '0 set to 1 else 0 
	output wire cOut; 		// Additional Carry bit
	output wire [15:0] ALURslt; 	// ALU Result
	output wire A; 			// MSB of Input A
	output wire B;			// MSB of Input B

	// additional variables
	wire [15:0] arithRslt; 		// R-format Arithmetic Result
	wire [15:0] shroRslt; 		// Shift Rotator Result
	wire parity; 			// unused 

	// R-Format Calculation
	bitwise rformat(.InA(ALUInB), .InB(ALUInA), .ALUOp(ALUOp[1:0]), .parity(parity), .Oper(Func[1:0]), .Out(arithRslt), .cOut(cOut));
	
	// Shifter Rotator
	shifter shift_rotate(.InBS(ALUInA), .ShAmt(ALUInB[3:0]), .ShiftOper(Func[1:0]), .OutBS(shroRslt));

	assign ALURslt = (ALUOp == 2'b11) ? shroRslt : arithRslt; 
	assign zero = (ALURslt == 16'h0) ? 1'b1 : 1'b0;
	assign A = ALUInA[15];
	assign B = ALUInB[15];

 
endmodule
`default_nettype wire
