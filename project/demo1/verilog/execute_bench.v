module execute_bench;
	
	// generate random input 
	reg [15:0] PCNext; 			// Random PCNext input
	reg [15:0] ALUInA;			// Random ALUInA input
	reg [15:0] ReadData2;  			// Random ReadData2 input
	reg [15:0] ExtImm; 			// Random ExtImm input
	
	// assign control signals 
	reg ALUSrc; 				// ALUSrc control signal to select ALUInB
	reg [1:0] ALUOp; 			// ALUOp control signal - Add, subtract, R-format, Shift
	reg [1:0] Func; 			// Func control signal - Add, subtract, XOR, ANDN
	reg ZeroExt; 				// ZeroExt control signal to select ShiftLoadRslt
	reg [1:0] ALUExt; 			// Mux4 to select final output of ExecRslt; 		
	reg [15:0] Instruction; 		// Instruction control signal for brcond and setcond

	// additional variables
	reg [15:0] ALUInB;			// ALUInB mux selected - register value vs. immediate
	reg [3:0] operation; 			// Combination of ALUOp and Func - case purpose for ALU
	reg [1:0] set_operation; 		// Intruction[12:11] for set block
	reg [16:0] tmpALURslt; 			// SCO Add Carry bit Generation

	// generated output 
	wire [15:0] PCAdded;			// Generated PCadded output
	wire [15:0] ALURslt; 			// Generated ALU calculation output
	wire zero; 
	wire cOut; 
	wire [15:0] BtrRslt; 			// Generated BitReverse output
	wire [15:0] SetRslt; 			// Generated Set Condition output
	wire [15:0] ShiftLoad; 			// Generated ShiftLoad output 
	wire BrRslt; 
	wire [15:0] ExecRslt; 
	wire executeErr; 

	// desired output
	reg [15:0] desiredPCAdded;		// Desired PCAdded output 
	reg desiredZero; 
	reg [16:0] desiredALURslt; 		// Desired ALU calculation output
	reg [15:0] desiredBtrRslt; 		// Desired BitReverse output
	reg [15:0] desiredSetRslt;		// desired Set Condition output
	reg [15:0] desiredShiftLoad; 		// Desired ShiftLoad output 
	reg desiredBrRslt; 
	reg [15:0] desiredExecRslt; 
		
	// dummy wires
	wire Clk; 
	wire rst; 
	wire err; 

	// fail check
	reg error; 

	clkrst my_clkrst( .clk(Clk), .rst(rst), .err(err));
   	execute exe(	.PCNext(PCNext), 
			.ALUInA(ALUInA), 
			.ReadData2(ReadData2), 
			.ExtImm(ExtImm), 
			.ALUSrc(ALUSrc), 
			.ALUOp(ALUOp), 
			.Func(Func), 
			.ZeroExt(ZeroExt),
			.ALUExt(ALUExt), 
			.Instruction(Instruction), 
			.PCAdded(PCAdded), 
			.BrRslt(BrRslt), 
			.ExecRslt(ExecRslt), 
			.err(executeErr)
			); 

/*
	setcond sc(	.zero(zero), 
			.cOut(cOut), 
			.MSB(ALURslt[15]), 
			.control(set_operation), 
			.SetRslt(SetRslt)
			);
*/

	alu alu_sim(	.ALUInA(ALUInA), 
			.ALUInB(ALUInB), 
			.ALUOp(ALUOp), 
			.Func(Func), 
			.zero(zero), 
			.cOut(cOut), 
			.ALURslt(ALURslt)
			); 

	bitreverse br(	.ALUInA(ALUInA), 
			.BtrRslt(BtrRslt)
			);

	shiftload sl(	.ZeroExt(ZeroExt), 
			.ALUInA(ALUInA), 
			.ExtImm(ExtImm), 
			.ShiftLoadRslt(ShiftLoad)
			);

	// instantiate all input datapath signals and input control signals
   	initial begin
      		PCNext = '0;
		ALUInA = '0; 
		ReadData2 = '0; 
		ExtImm = '0; 

		#10000;
		if (error)
            		$display("TEST FAILED");
        	else
            		$display("YAHOOO!!  TEST PASSED!");	
	 	$stop; 
   	end
   
	// generate random input datapath signals
   	always@(posedge Clk) begin
      		PCNext = $random; 
		ALUInA = $random; 
		ReadData2 = $random; 
		ExtImm = $random;
		Instruction = $random; 

		ZeroExt = $random; 
		ALUSrc = $random; 
		ALUOp = $random; 
		Func = $random;
		operation = {ALUOp, Func};
		set_operation = Instruction[12:11];

		ALUInB = ALUSrc ? ExtImm : ReadData2; 
   	end

	// test small modules in execute block
	always @(negedge Clk) begin
		// PC branch 
		desiredPCAdded = PCNext + ExtImm; 
		$display("[PC Branch] PCNext: 0x%x, ExtImm: 0x%x, desiredPCAdded: 0x%x, PCAdded: 0x%x", PCNext, ExtImm, desiredPCAdded, PCAdded);
		if(desiredPCAdded !== PCAdded) begin
			$display("PCAdded Error");
			error = 1; 
		end

		// bitreverse
		desiredBtrRslt = {	ALUInA[0], ALUInA[1], ALUInA[2], ALUInA[3], 
					ALUInA[4], ALUInA[5], ALUInA[6], ALUInA[7], 
					ALUInA[8], ALUInA[9], ALUInA[10], ALUInA[11], 
					ALUInA[12], ALUInA[13], ALUInA[14], ALUInA[15]
					};
		$display("[Bit Reverse] ALUInA: 0x%x, desiredBtrRslt: 0x%x, BtrRslt: 0x%x", ALUInA, desiredBtrRslt, BtrRslt);
		if(desiredBtrRslt !== BtrRslt) begin
			$display("bitreverse Error");
			error = 1; 
		end

		// shiftload8
		if (ZeroExt) begin
			desiredShiftLoad = {ALUInA[7:0], ExtImm[7:0]};
		end 
		else begin
			desiredShiftLoad = ExtImm; 
		end
		$display("[ShiftLoad 8] ALUInA: 0x%x, ExtImm: 0x%x, ZeroExt: 0x%x, desiredShiftLoad: 0x%x, ShiftLoad: 0x%x", ALUInA, ExtImm, ZeroExt, desiredShiftLoad, ShiftLoad);
		if(desiredShiftLoad !== ShiftLoad) begin
			$display("shiftload8 Error");
			error = 1; 
		end
	end
/*
	// Set Condition Block
	always @(negedge Clk) begin
		case(set_operation)
			2'b00: begin
				desiredSetRslt = ((ALUInA ^ ReadData2) == '0) ? 16'h0001 : '0;
				$display("[SEQ] Rs: 0x%x, Rt: 0x%x, Set_Operation: 0x%x, desiredSetRslt: 0x%x, setRslt: 0x%x", 
						ALUInA, ReadData2, set_operation, desiredSetRslt, SetRslt); 
				if(desiredSetRslt !== SetRslt) begin
					$display("SEQ Error");
					error = 1;
				end
			end

			2'b01: begin
				desiredSetRslt = (ALUInA < ReadData2) ? 16'h0001 : '0;
				$display("[SLT] Rs: 0x%x, Rt: 0x%x, Set_Operation: 0x%x, desiredSetRslt: 0x%x, setRslt: 0x%x", 
						ALUInA, ReadData2, set_operation, desiredSetRslt, SetRslt); 
				if(desiredSetRslt !== SetRslt) begin
					$display("SLT Error");
					error = 1;
				end
			end

			2'b10: begin
				desiredSetRslt = (ALUInA <= ReadData2) ? 16'h0001 : '0;
				$display("[SLE] Rs: 0x%x, Rt: 0x%x, Set_Operation: 0x%x, desiredSetRslt: 0x%x, setRslt: 0x%x", 
						ALUInA, ReadData2, set_operation, desiredSetRslt, SetRslt); 
				if(desiredSetRslt !== SetRslt) begin
					$display("SLE Error");
					error = 1;
				end
			end
			
			2'b11: begin
				tmpALURslt = ALUInA + ReadData2;
				desiredSetRslt = tmpALURslt[16] ? 16'h0001 : '0;
				$display("[SCO] Rs: 0x%x, Rt: 0x%x, Set_Operation: 0x%x, desiredSetRslt: 0x%x, setRslt: 0x%x", 
						ALUInA, ReadData2, set_operation, desiredSetRslt, SetRslt); 
				if(desiredSetRslt !== SetRslt) begin
					$display("SCO Error");
					error = 1;
				end
			end


			default: begin
				error = 1;
				$display("[Set Condition] Error - No operation matched. Set_Operation: 0x%x", set_operation);
			end
		endcase
	end
*/
	// ALU block 
	always @(negedge Clk) begin
		casex(operation) 
			4'b00xx: begin 							// Add
				desiredALURslt = ALUInA + ALUInB;
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU ADD] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x, desiredcOut: 0x%x, cOut: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero, desiredALURslt[16], cOut); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero || desiredALURslt[16] !== cOut) begin
					$display("ALU Addition Error");
					error = 1; 
				end
			end		
			4'b01xx: begin 							// Subtract
				desiredALURslt = ALUInA - ALUInB;
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU SUB] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x, desiredcOut: 0x%x, cOut: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero, desiredALURslt[16], cOut); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero || desiredALURslt[16] !== cOut) begin
					$display("ALU Subtraction Error");
					error = 1; 
				end
			end
			4'b1000: begin 							// RFormat - Add
				desiredALURslt = ALUInA + ALUInB;
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU ADD] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x, desiredcOut: 0x%x, cOut: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero, desiredALURslt[16], cOut); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero || desiredALURslt[16] !== cOut) begin
					$display("ALU Subtraction Error");
					error = 1; 
				end
			end
			4'b1001: begin 							// RFormat - Subtract
				desiredALURslt = ALUInA - ALUInB;
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU SUB] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x, desiredcOut: 0x%x, cOut: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero, desiredALURslt[16], cOut); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero || desiredALURslt[16] !== cOut) begin
					$display("ALU Subtraction Error");
					error = 1; 
				end
			end
			4'b1010: begin 							// RFormat - XOR
				desiredALURslt = ALUInA ^ ALUInB;
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU XOR] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero) begin
					$display("ALU XOR Error");
					error = 1; 
				end
			end
			4'b1011: begin 							// RFormat - ANDN
				desiredALURslt = ~(ALUInA & ALUInB);
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU ANDN] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero) begin
					$display("ALU ANDN Error");
					error = 1; 
				end
			end
			4'b1100: begin 							// Shift Left
				desiredALURslt = ALUInA << ALUInB[3:0];
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU SLL] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero) begin
					$display("ALU SLL Error");
					error = 1; 
				end
			end
			4'b1101: begin 							// Shift Right
				desiredALURslt = ALUInA >> ALUInB[3:0];
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU SRL] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero) begin
					$display("ALU SRL Error");
					error = 1; 
				end
			end

			4'b1110: begin 							// Rotate Left
				desiredALURslt = (ALUInA << ALUInB[3:0]) | (ALUInA >> (16-ALUInB[3:0]));
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU ROL] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero) begin
					$display("ALU ROL Error");
					error = 1; 
				end
			end
			4'b1111: begin 							// Rotate Right
				desiredALURslt = (ALUInA >> ALUInB[3:0]) | (ALUInA << (16-ALUInB[3:0]));
				desiredZero = (desiredALURslt == '0) ? 1'b1 : 1'b0; 
				$display("[ALU ROR] ALUInA: 0x%x, ALUInB: 0x%x, Operation: 0x%x, desiredALURslt: 0x%x, ALURslt: 0x%x, desiredZero: 0x%x, zero: 0x%x", 
						ALUInA, ALUInB, operation, desiredALURslt[15:0], ALURslt, desiredZero, zero); 
				if(desiredALURslt[15:0] !== ALURslt || desiredZero !== zero) begin
					$display("ALU ROR Error");
					error = 1; 
				end
			end
			default: begin
				error = 1; 
				$display("[ALU] Error - No operation matched. Operation: 0x%x", operation); 	
			end
		endcase
	end
	
endmodule 