/*
   CS/ECE 552 Spring '23
  
   Filename        : execute.v
   Description     : This is the overall module for the execute stage of the processor.
*/
`default_nettype none
module execute (
		// inputs 
			PCNext, ExtImm, ALUInA, ReadData2, ALUSrc, ALUOp, Func, ALUExt, Instruction, ZeroExt,
		// outputs
			PCAdded, BrRslt, ExecRslt, err
		);

	input wire [15:0] PCNext; 
	input wire [15:0] ExtImm;
	input wire [15:0] ALUInA; 
	input wire [15:0] ReadData2; 
	input wire ALUSrc; 
	input wire [1:0] ALUOp;			
	input wire [1:0] Func;
	input wire [1:0] ALUExt; 
	input wire [15:0] Instruction; 
	input wire ZeroExt; 

	output wire [15:0] PCAdded; 
	output wire BrRslt; 
	output wire [15:0] ExecRslt; 		// 16 bit address, on document only shows 1 bit
	output wire	   err;			// Combined module errors

	// additional variables
	wire [15:0] ALUInB;
	wire [14:0] carry; 
	wire zero; 
	wire cOut;
	wire brOF;				// Overflow for branch incrementer
	wire [15:0] ALURslt; 
	wire [15:0] SetRslt;
	wire [15:0] BtrRslt; 
	wire [15:0] ShiftLoadRslt;
	wire A;
        wire B;	

	// Combine errors from module
	assign err = brOF;
   	assign brOF = 1'b0;

	// get added instruction for Branches/J/JAL
	cla16b #(
		 .N(16)
		 ) FA (
		       .sum(PCAdded),
		       .cOut(), // This triggers an error if set
		       .inA(PCNext),
		       .inB(ExtImm),
		       .cIn(1'b0)
		       );

	// configure ALUInB
	assign ALUInB = ALUSrc ? ExtImm : ReadData2; 

	// configure ALU block
	alu ALU(.ALUInA(ALUInA), .ALUInB(ALUInB), .ALUOp(ALUOp), .A(A), .B(B), .Func(Func), .zero(zero), .cOut(cOut), .ALURslt(ALURslt));

	// configure brcond block 
	brcond BrCond(.ALUInA(ALUInA), .control(Instruction[12:11]), .BrRslt(BrRslt)); 
	
	// configure setcond block
	setcond SetCond(.zero(zero), .MSB(ALURslt[15]), .cOut(cOut), .A(A), .B(B), .control(Instruction[12:11]), .SetRslt(SetRslt)); 

	// configure bitreverse block
	bitreverse BitReverse(.ALUInA(ALUInA), .BtrRslt(BtrRslt));

	// configure shiftload block
	shiftload ShiftLoad(.ZeroExt(ZeroExt), .ALUInA(ALUInA), .ExtImm(ExtImm), .ShiftLoadRslt(ShiftLoadRslt));
	
	// configure executed result
	assign ExecRslt = ALUExt[1] ? (ALUExt[0] ? ShiftLoadRslt : BtrRslt) : (ALUExt[0] ? SetRslt : ALURslt); 
	

	
   
endmodule
`default_nettype wire
