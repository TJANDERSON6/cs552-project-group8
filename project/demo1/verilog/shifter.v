/*
    CS/ECE 552 Spring '23
    Homework #2, Problem 1
    
    A barrel shifter module.  It is designed to shift a number via rotate
    left, shift left, shift right arithmetic, or shift right logical based
    on the 'Oper' value that is passed in.  It uses these
    shifts to shift the value any number of bits.
 */
`default_nettype none
module shifter (InBS, ShAmt, ShiftOper, OutBS);

    // declare constant for size of inputs, outputs, and # bits to shift
    parameter OPERAND_WIDTH = 16;
    parameter SHAMT_WIDTH   =  4;
    parameter NUM_OPERATIONS = 2;

    input wire [OPERAND_WIDTH -1:0] InBS;  // Input operand
    input wire [SHAMT_WIDTH   -1:0] ShAmt; // Amount to shift/rotate
    input wire [NUM_OPERATIONS-1:0] ShiftOper;  // Operation type
    output wire [OPERAND_WIDTH -1:0] OutBS;  // Result of shift/rotate

	wire [15:0] mux1, mux2, mux3, out1, mux4, mux5, mux6, out2, mux7, mux8, mux9, out3, mux10, mux11, mux12, out4;

	// Shift Left Logical
	assign mux1 = ShAmt[0] ? {InBS[14:0], 1'b0} : InBS;		
	assign mux2 = ShAmt[1] ? {mux1[13:0], 2'b0} : mux1;		
	assign mux3 = ShAmt[2] ? {mux2[11:0], 4'b0} : mux2;		
	assign out1 = ShAmt[3] ? {mux3[7:0], 8'b0} : mux3;		 

	// Shift Right Logical
	assign mux4 = ShAmt[0] ? {1'b0, InBS[15:1]} : InBS;		
	assign mux5 = ShAmt[1] ? {2'b0, mux4[15:2]} : mux4;		
	assign mux6 = ShAmt[2] ? {4'b0, mux5[15:4]} : mux5;		
	assign out2 = ShAmt[3] ? {8'b0, mux6[15:8]} : mux6;	
	
	// Rotate Left
	assign mux7 = ShAmt[0] ? {InBS[14:0], InBS[15]} : InBS;		
	assign mux8 = ShAmt[1] ? {mux7[13:0], mux7[15:14]} : mux7;		
	assign mux9 = ShAmt[2] ? {mux8[11:0], mux8[15:12]} : mux8;	
	assign out3 = ShAmt[3] ? {mux9[7:0], mux9[15:8]} : mux9;	

	// Rotate Right
	assign mux10 = ShAmt[0] ? {InBS[0], InBS[15:1]} : InBS;		
	assign mux11 = ShAmt[1] ? {mux10[1:0], mux10[15:2]} : mux10;		
	assign mux12 = ShAmt[2] ? {mux11[3:0], mux11[15:4]} : mux11;		
	assign out4 = ShAmt[3] ? {mux12[7:0], mux12[15:8]} : mux12;

	// Get Result
	assign OutBS = (ShiftOper[1] == 1'b0) ? ((ShiftOper[0] == 1'b0) ? out1 : out2) : ((ShiftOper[0] == 1'b0) ? out3 : out4);

endmodule
`default_nettype wire



