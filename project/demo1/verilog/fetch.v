/*
   CS/ECE 552 Spring '23
  
   Filename        : fetch.v
   Description     : This is the module for the overall fetch stage of the processor.
*/
`default_nettype none
module fetch (
		// inputs
			RST, Halted, PCBr, PCSrc, PCEn, Dump, clk, Jump,
		// outputs
			PCNext, Instruction, err
		);

	input wire RST; 
	input wire Halted; 
	input wire [15:0] PCBr; 
	input wire PCSrc;
	input wire PCEn; 
	input wire Dump; 
	input wire clk;
	input wire Jump;

	output wire [15:0] PCNext; 
	output wire [15:0] Instruction;
	output wire	   err;

	// additional variables
	wire [15:0] mux1, PC;		// result of mux from left to right
	wire	    cOut;		// Indicates i-mem overflow error
	wire [15:0] InstrNext;
	wire	    PcErr;		// Driven by PC reg

	// Combine error conditions
	assign err = PcErr | cOut;

	// fetch next instruction
	cla16b #(
		 .N(16)
		 ) cla16b_inst (
				.sum(PCNext),
				.cOut(cOut), // Overflow err if set (out of i-mem)
				.inA(PC),
				.inB(16'h2),
				.cIn(1'b0)
				);

	// before PC increment
	assign mux1 = PCSrc | Jump ? PCBr : PCNext; 

	// Program counter register:
	//
	// - Will only update if PCEn is set
	// - Will reset to zero if RST is asserted
	f_reg #(
		.N(16)
		) pc_reg (
			  .dOut(PC),
			  .err(PcErr),
			  .clk(clk),
			  .rst(RST),
			  .dIn(mux1),
			  .wEn(PCEn)
			  );

	// Use provided single-cycle mem module for instruction memory
	// Not sure if instruction memory needs to be dumped, but say yes for now
	memory2c memory2c_inst (
				.data_out(InstrNext),
				.data_in(16'h0), // These data will never be written
				.addr(PC),
				.enable(1'b1),
				.wr(1'b0), // ROM
				.createdump(1'b0),
				.clk(clk),
				.rst(RST)
				);

	// If halted, send NOP, not next instruction
	assign Instruction = /*Halted ? 16'h0800 :*/ InstrNext; 
   
endmodule
`default_nettype wire
