/*
    CS/ECE 552 Spring '23
    Homework #1, Problem 2
    
    a 4-bit CLA module
*/
`default_nettype none
module cla4b(sum, cOut, inA, inB, cIn, g, p);

    // declare constant for size of inputs, outputs (N)
    parameter   N = 4;

    output wire [N-1:0] sum;
    output wire         cOut;
    input wire [N-1: 0] inA, inB;
    input wire          cIn;

   output wire [3:0]	g; // generate
   output wire [3:0]	p; // propogate
   wire [3:1]		c; // Inner carries

   // YOUR CODE HERE
   fullAdder1b fullAdder1b_inst0(
				 .s(sum[0]),
				 .cOut(),
				 .inA(inA[0]),
				 .inB(inB[0]),
				 .cIn(cIn)
				 );
   fullAdder1b fullAdder1b_inst1(
				 .s(sum[1]),
				 .cOut(),
				 .inA(inA[1]),
				 .inB(inB[1]),
				 .cIn(c[1])
				 );
   fullAdder1b fullAdder1b_inst2(
				 .s(sum[2]),
				 .cOut(),
				 .inA(inA[2]),
				 .inB(inB[2]),
				 .cIn(c[2])
				 );
   fullAdder1b fullAdder1b_inst3(.s(sum[3]),
				 .cOut(),
				 .inA(inA[3]),
				 .inB(inB[3]),
				 .cIn(c[3])
				 );

   // gi = ai * bi
   and2 and2_g0 (
		 .out(g[0]),
		 .in1(inA[0]),
		 .in2(inB[0])
		 );
   and2 and2_g1 (
		 .out(g[1]),
		 .in1(inA[1]),
		 .in2(inB[1])
		 );
   and2 and2_g2 (
		 .out(g[2]),
		 .in1(inA[2]),
		 .in2(inB[2])
		 );
   and2 and2_g3 (
		 .out(g[3]),
		 .in1(inA[3]),
		 .in2(inB[3])
		 );

   // pi = ai + bi
   or2 or2_p0 (
		 .out(p[0]),
		 .in1(inA[0]),
		 .in2(inB[0])
		 );
   or2 or2_p1 (
		 .out(p[1]),
		 .in1(inA[1]),
		 .in2(inB[1])
		 );
   or2 or2_p2 (
		 .out(p[2]),
		 .in1(inA[2]),
		 .in2(inB[2])
		 );
   or2 or2_p3 (
		 .out(p[3]),
		 .in1(inA[3]),
		 .in2(inB[3])
		 );

   // Find our carries
   carry4b carry4b_inst (
			 .g(g),
			 .p(p),
			 .cIn(cIn),
			 .c(c),
			 .cOut(cOut)
			 );
endmodule
`default_nettype wire
