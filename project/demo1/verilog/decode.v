/*
   CS/ECE 552 Spring '23
  
   Filename        : decode.v
   Description     : This is the module for the overall decode stage of the processor.
*/
`default_nettype none
module decode (
		// inputs 
			Instruction, WriteData, Clk, Rst,
		// outputs
			ReadData1, ReadData2, ExtImm, RegDst, Func, ZeroExt, MemtoReg,
	       		ImmLen, RegWrite, JumpSrc, MemRead, Jump, MemWrite,
			JumpLink, Branch, ALUExt, ALUOp, Halted, PCEn, Dump, Err, ALUSRC
		);

	input wire [15:0] Instruction; 
	input wire [15:0] WriteData;
	input wire Clk;
	input wire Rst;

	output wire [15:0] ReadData1; 
	output wire [15:0] ReadData2; 
	output wire [15:0] ExtImm;

	// Control Signals, both internally and for export
	output wire [1:0]  RegDst;
	output wire [1:0]  Func;
	output wire	   ALUSRC;
	output wire	   ZeroExt;
	output wire	   MemtoReg;
	output wire [1:0]  ImmLen;
	output wire	   RegWrite;
	output wire	   JumpSrc;
	output wire	   MemRead;
	output wire	   Jump;
	output wire	   MemWrite;
	output wire	   JumpLink;
	output wire	   Branch;
	output wire [1:0]  ALUExt;
	output wire [1:0]  ALUOp;
	output wire	   Halted;
	output wire	   PCEn;
	output wire	   Dump;
	output wire	   Err;


	// additional variables
	wire [2:0] WriteReg; 
	wire [15:0] SignExtend;
	wire [15:0] SignExtend8_16; 
	wire [15:0] SignExtend11_16; 
	wire [15:0] ZeroExtend5_16; 
	wire [15:0] ZeroExtend8_16; 	
	wire [15:0] SignExtendValue; 	// Selected Value from Sign Extension
	wire [15:0] ZeroExtendValue; 	// Selected Value from Zero Extension
	
	// configure write data
	assign WriteReg = RegDst[1] ? (RegDst[0] ? 3'h7 : Instruction[4:2]) : (RegDst[0] ? Instruction[10:8] : Instruction[7:5]);

        // Register file with 8 registers (r[7:0]) and 3 bit input
	rf #(
	     .N(16),
	     .RI(3),
	     .NR(8)
	     ) rf_inst (
			.read1OutData(ReadData1),
			.read2OutData(ReadData2),
			.err(),
			.clk(Clk),
			.rst(Rst),
			.read1RegSel(Instruction[10:8]),
			.read2RegSel(Instruction[7:5]),
			.writeRegSel(WriteReg),
			.writeInData(WriteData),
			.writeEn(RegWrite)
			);
	
	// configure extensions
	assign SignExtend = {{11{Instruction[4]}}, Instruction[4:0]};
	assign SignExtend8_16 = {{8{Instruction[7]}}, Instruction[7:0]};
	assign SignExtend11_16 = {{5{Instruction[10]}}, Instruction[10:0]};
	assign ZeroExtend5_16 = {11'b0, Instruction[4:0]};
	// Old verison of datapath dwg called out incorrect bit index,
	// below is correct:
	assign ZeroExtend8_16 = {8'b0, Instruction[7:0]};

	assign SignExtendValue = ImmLen[1] ? (ImmLen[0] ?  SignExtendValue : SignExtend11_16) : (ImmLen[0] ? SignExtend8_16 : SignExtend);
	assign ZeroExtendValue = ImmLen[0] ? ZeroExtend8_16 : ZeroExtend5_16; 
	assign ExtImm = ZeroExt ? ZeroExtendValue : SignExtendValue;

	// Control block to feed to all the other stages
	// Eventually will be fed to Dec/Ex Pipeline Register
	control_block control_block_inst (
					  .Clk(Clk),
					  .Rst(Rst),
					  .Instruction(Instruction),
					  .RegDst(RegDst),
					  .Func(Func),
					  .ALUSRC(ALUSRC),
					  .ZeroExt(ZeroExt),
					  .MemtoReg(MemtoReg),
					  .ImmLen(ImmLen),
					  .RegWrite(RegWrite),
					  .JumpSrc(JumpSrc),
					  .MemRead(MemRead),
					  .Jump(Jump),
					  .MemWrite(MemWrite),
					  .JumpLink(JumpLink),
					  .Branch(Branch),
					  .ALUExt(ALUExt),
					  .ALUOp(ALUOp),
					  .Halted(Halted),
					  .PCEn(PCEn),
					  .Dump(Dump),
					  .Err(Err)
					  );
   
endmodule
`default_nettype wire
