`default_nettype none
module bitwise (InA, InB, ALUOp, Oper, Out, cOut, parity); // mightneed ofl

    parameter OPERAND_WIDTH = 16;    
    parameter NUM_OPERATIONS = 2;
       
    input wire  [OPERAND_WIDTH -1:0] InA ; 	// Input wire operand A
    input wire  [OPERAND_WIDTH -1:0] InB ; 	// Input wire operand B
    input wire  [NUM_OPERATIONS-1:0] ALUOp; 	// overwrites function operation 
    input wire  [NUM_OPERATIONS-1:0] Oper; 	// Operation type - Func in ALU
    output wire [OPERAND_WIDTH -1:0] Out ; 	// Result of comput wireation
    output wire cOut; 				// Additional cOut	
    output wire parity; 			// Unused 

    wire [OPERAND_WIDTH - 1:0] ADD, complement, XOR, ANDN;
    wire [NUM_OPERATIONS - 1:0] overwriteOper; 	// ALUOp overwrites Func 
    wire subCarry; 				// Two's complement carry bit
    wire tmpCarry; 				// cOut detection (relatively unused) 
	
    assign overwriteOper = (ALUOp[1] == 1'b0) ? ALUOp : Oper;	// Overwrite Operation - get correct operation in bitwise calculation
    assign complement = overwriteOper[0] ? ~InB : InB; 		// Inverse B if subtraction
    assign subCarry = overwriteOper[0] ? 1'b1 : 1'b0; 		// Two's complemented add 1

    cla16b fAdd(.inA(InA), .inB(complement), .cIn(subCarry), .sum(ADD), .cOut(tmpCarry));
    fullXor fXor[15:0](.InA(InA), .InB(InB), .o(XOR));
    fullAndn fAndN[15:0](.InA(InB), .InB(InA), .o(ANDN));

    assign Out = (overwriteOper[1] == 1'b0) ? ADD : ( (overwriteOper == 2'b10) ? XOR :  ( (overwriteOper == 2'b11) ? ANDN : InA) );
	assign cOut = tmpCarry;
  	assign parity = InA[15] ^ InB[15];

	
	/*
    wire opCh, notSign, one, a15, b15, c15, anbnc, nabc, opSign, two, three, overflow; 
    nor2 nor1_2(.out(opCh), .in1(Oper[1]), .in2(Oper[0])); 
    not1 not1Sign(.out(notSign), .in1(sign)); 
    and3 first(.out(one), .in1(opCh), .in2(notSign), .in3(c[15])); 

    not1 add15(.out(c15), .in1(ADD[15])); 
    not1 inA15(.out(a15), .in1(InA[15])); 
    not1 inB15(.out(b15), .in1(InB[15])); 
	    
    and2 operSign(.out(opSign), .in1(opCh), .in2(sign)); 
    and3 abc1(.out(anbnc), .in1(ADD[15]), .in2(a15), .in3(b15)); 
    and3 abc2(.out(nabc), .in1(c15), .in2(InA[15]), .in3(InB[15])); 

    and2 second(.out(two), .in1(opSign), .in2(anbnc));
    and2 third(.out(three), .in1(opSign), .in2(nabc));  

    or3 oflCh(.out(overflow), .in1(one), .in2(two), .in3(three)); 
 
    assign Ofl = overflow ? 1'b1 : 1'b0; 
    */
endmodule
`default_nettype wire
