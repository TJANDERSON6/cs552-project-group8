/*
    CS/ECE 552 Spring '23
    Homework #1, Problem 2
    
    a 16-bit CLA module
*/
`default_nettype none
module cla16b(sum, cOut, inA, inB, cIn);

    // declare constant for size of inputs, outputs (N)
    parameter   N = 16;

    output wire [N-1:0] sum;
    output wire         cOut;
    input wire [N-1: 0] inA, inB;
    input wire          cIn;

   wire [N-1:0]		g;
   wire [N-1:0]		p;
   wire [3:0]		G;
   wire [3:0]		P;
   wire [3:1]		C;
   wire [31:0]		lh; // For inner connections

    // Arithmetic Unit
   
   cla4b cla4b_inst0 (
		      .sum(sum[3:0]),
		      .cOut(),
		      .inA(inA[3:0]),
		      .inB(inB[3:0]),
		      .cIn(cIn),
		      .g(g[3:0]),
		      .p(p[3:0])
		      );
   cla4b cla4b_inst1 (
		      .sum(sum[7:4]),
		      .cOut(),
		      .inA(inA[7:4]),
		      .inB(inB[7:4]),
		      .cIn(C[1]),
		      .g(g[7:4]),
		      .p(p[7:4])
		      );
   cla4b cla4b_inst2 (
		      .sum(sum[11:8]),
		      .cOut(),
		      .inA(inA[11:8]),
		      .inB(inB[11:8]),
		      .cIn(C[2]),
		      .g(g[11:8]),
		      .p(p[11:8])
		      );
   cla4b cla4b_inst3 (
		      .sum(sum[15:12]),
		      .cOut(),
		      .inA(inA[15:12]),
		      .inB(inB[15:12]),
		      .cIn(C[3]),
		      .g(g[15:12]),
		      .p(p[15:12])
		      );

   // Carry-Lookahead Unit
   
   // Calculate top level propogates (P)
   // P0 = and(p[3:0])
   // and3 and3_P0 (
   // 		 .out(lh[0]),
   // 		 .in1(p[0]),
   // 		 .in2(p[1]),
   // 		 .in3(p[2])
   // 		 );
   // and2 and2_P0 (
   // 		 .out(P[0]),
   // 		 .in1(lh[0]),
   // 		 .in2(p[3])
   // 		 );

   // // P1 = and(p[7:4])
   // and3 and3_P1 (
   // 		 .out(lh[1]),
   // 		 .in1(p[4]),
   // 		 .in2(p[5]),
   // 		 .in3(p[6])
   // 		 );
   // and2 and2_P1 (
   // 		 .out(P[1]),
   // 		 .in1(lh[1]),
   // 		 .in2(p[7])
   // 		 );

   // // P2 = and(p[11:8])
   // and3 and3_P2 (
   // 		 .out(lh[2]),
   // 		 .in1(p[8]),
   // 		 .in2(p[9]),
   // 		 .in3(p[10])
   // 		 );
   // and2 and2_P2 (
   // 		 .out(P[2]),
   // 		 .in1(lh[2]),
   // 		 .in2(p[1])
   // 		 );

   // // P3 = and(p[15:12])
   // and3 and3_P3 (
   // 		 .out(lh[3]),
   // 		 .in1(p[12]),
   // 		 .in2(p[13]),
   // 		 .in3(p[14])
   // 		 );
   // and2 and2_P3 (
   // 		 .out(P[3]),
   // 		 .in1(lh[3]),
   // 		 .in2(p[15])
   // 		 );

   // Generate and Propogate
   // G0 = g3 + (p3*g2) + (p3*p2*g1) + (p3*p2*p1*g0)
   // G1 = g7 + (p7*g6) + (p7*p6*g5) + (p7*p6*p5*g4)
   // G2 = g11 + (p11 * g10) + (p11*p10*g9) + (p11*p10*p9*g8)
   // G3 = g15 + (p15*g14)
   generate4b generate4b_u0 (
    .gIn(g[3:0]),
    .pIn(p[3:0]),
    .GOut(G[0]),
    .POut(P[0])
);
   generate4b generate4b_u1 (
    .gIn(g[7:4]),
    .pIn(p[7:4]),
    .GOut(G[1]),
    .POut(P[1])
);
   generate4b generate4b_u2 (
    .gIn(g[11:8]),
    .pIn(p[11:8]),
    .GOut(G[2]),
    .POut(P[2])
);
   generate4b generate4b_u3 (
    .gIn(g[15:12]),
    .pIn(p[15:12]),
    .GOut(G[3]),
    .POut(P[3])
);

   // Process our carries
   carry4b carry4b_inst (
    .g(G),
    .p(P),
    .cIn(cIn),
    .c(C),
    .cOut(cOut)
);

endmodule
`default_nettype wire
