/* $Author: sinclair $ */
/* $LastChangedDate: 2020-02-09 17:03:45 -0600 (Sun, 09 Feb 2020) $ */
/* $Rev: 46 $ */
`default_nettype none
module proc (/*AUTOARG*/
   // Outputs
   err, 
   // Inputs
   clk, rst
   );

   input wire clk;
   input wire rst;

   output reg err;

   // None of the above lines can be modified

   // OR all the err ouputs for every sub-module and assign it as this
   // err output
   
   // As desribed in the homeworks, use the err signal to trap corner
   // cases that you think are illegal in your statemachines
   
   
   /* your code here -- should include instantiations of fetch, decode, execute, mem and wb modules */

	// control signals
	wire Halted; 
	wire PCSrc; 
	wire Dump;
	wire RegWrite;
	wire [1:0] RegDst; 
	wire [1:0] ImmLen; 
	wire ZeroExt;
	wire [15:0] ExtImm; 
	wire ALUSrc; 
	wire [1:0] ALUOp; 	// should be 2 bits, design shows 3 bits
	wire [1:0] Func; 
	wire [1:0] ALUExt; 
	wire JumpSrc; 
	wire Jump; 
	wire Branch;
	wire MemWrite; 
	wire MemRead; 
	wire JumpLink;
	wire PCEn;
	wire MemtoReg;

	// datapath 
	wire [15:0] PCBr; 
	wire [15:0] PCNext; 
	wire [15:0] Instruction; 
	wire [15:0] WriteData;
	wire [15:0] ReadData1; 
	wire [15:0] ReadData2; 
	wire [15:0] PCAdded; 
	wire BrRslt; 
	wire [15:0] ExecRslt; 
	wire [15:0] ReadData;

	// Module errors
	wire	    decodeErr;
	wire	    fetchErr;

	// OR module errors together (might break rules, why err a reg?)

	// fetch block
	fetch fetchBlock (
			  .RST(rst),
			  .Halted(Halted),
			  .Dump(Dump),
			  .PCBr(PCBr),
			  .PCSrc(PCSrc),
			  .PCEn(PCEn),
			  .clk(clk),
			  .Jump(Jump),
			  .PCNext(PCNext),
			  .Instruction(Instruction),
			  .err(fetchErr)
			  );

	// decode block (Sourcing control signals)
	decode decode_inst (
			    .Instruction(Instruction),
			    .WriteData(WriteData),
			    .Clk(clk),
			    .Rst(rst),
			    .ReadData1(ReadData1),
			    .ReadData2(ReadData2),
			    .ExtImm(ExtImm),
			    .RegDst(RegDst),
			    .Func(Func),
			    .ZeroExt(ZeroExt),
			    .MemtoReg(MemtoReg), // NC for now, may change for Phase II
			    .ImmLen(ImmLen),
			    .RegWrite(RegWrite),
			    .JumpSrc(JumpSrc),
			    .MemRead(MemRead),
			    .Jump(Jump),
			    .ALUSRC(ALUSrc),
			    .MemWrite(MemWrite),
			    .JumpLink(JumpLink),
			    .Branch(Branch),
			    .ALUExt(ALUExt),
			    .ALUOp(ALUOp),
			    .Halted(Halted),
			    .PCEn(PCEn),
			    .Dump(Dump),
			    .Err(decodeErr)
			    );
	
	// execute block
 	execute executeBlock(.PCNext(PCNext), .ExtImm(ExtImm), .ALUInA(ReadData1), .ReadData2(ReadData2), .ALUSrc(ALUSrc), .ALUOp(ALUOp), .Func(Func), 
				.ALUExt(ALUExt), .Instruction(Instruction), .PCAdded(PCAdded), .BrRslt(BrRslt), .ExecRslt(ExecRslt), .err(), .ZeroExt(ZeroExt)); 

	// memory block
	memory memoryBlock(.PCAdded(PCAdded), .ExecRslt(ExecRslt), .JumpSrc(JumpSrc), .Jump(Jump), .BrRslt(BrRslt), .Branch(Branch),
				.MemWrite(MemWrite), .MemRead(MemRead), .ReadData2(ReadData2), .PCBr(PCBr), .PCSrc(PCSrc), .ReadData(ReadData), .rst(rst), .clk(clk), .Dump(Dump));
	// writeback block
	wb wbBlock(.PCNext(PCNext), .ReadData(ReadData), .ExecRslt(ExecRslt), .MemtoReg(MemtoReg), .JumpLink(JumpLink), .WriteData(WriteData));

always @(decodeErr, fetchErr)
  begin
	  casex({decodeErr, fetchErr})
	    2'b00:
	      begin
		      err = 1'b0;
	      end
	    2'b01:
	      begin
		      err = 1'b1;
	      end
	    2'b10:
	      begin
		      err = 1'b1;
	      end
	    2'b11:
	      begin
		      err = 1'b1;
	      end
	    default:
	      begin
		      err = 1'b1;
	      end
	  endcase // casex ({decodeErr, fetchErr})
  end // always @ (decodeErr, fetchErr)
endmodule // proc
`default_nettype wire
// DUMMY LINE FOR REV CONTROL :0:
