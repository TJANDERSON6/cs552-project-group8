/*
   CS/ECE 552 Spring '23
  
   Filename        : wb.v
   Description     : This is the module for the overall Write Back stage of the processor.
*/
`default_nettype none
module wb (
		// inputs 
			PCNext, ReadData, ExecRslt, MemtoReg, JumpLink, 
		// outputs
			WriteData
		);

	input wire [15:0] PCNext; 
	input wire [15:0] ReadData; 
	input wire [15:0] ExecRslt; 
	input wire MemtoReg; 
	input wire JumpLink;
	
	output wire [15:0] WriteData; 

	// additional variables
	wire [15:0] wbData; 

	assign wbData = MemtoReg ? ReadData : ExecRslt; 
	assign WriteData = JumpLink ? PCNext : wbData; 

	/* mentioned in diagram 
		wb PC + 2 on JAL and JALR
		not yet implemented
	*/ 	

   
endmodule
`default_nettype wire
