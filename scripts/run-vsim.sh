#! /usr/bin/env bash

## Script to open the waveforms for the last successful test

scriptdir=$PWD/$(dirname $0)
rootdir=${scriptdir}/..
objdir=${rootdir}/obj
macrodir=${rootdir}/macros

macrofile=${MACROFILE:-"${macrodir}/wave-pipeline.do"}
dumpfile="dump.wlf"

vsflags=${VSFLAGS:-""}

print_usage() {
    echo "Usage:"
    echo "$0 [-dD]"
    echo "$0 [-h]"
    echo ""
    echo "Open waveform from last successful test"
    echo ""
    echo "Options:"
    echo "-D           Launch with pipeline register macros (default)"
    echo "-d           Launch without pipeline register macros"
}

## Use macros by default
Dflag=1

while getopts "Dd" opts; do
    case $opts in
	D)
	    Dflag=1
	    ;;
	d)
	    unset Dflag
	   ;;
	h)
	    print_usage
	    exit 0
	    ;;
	\?)
	    echo "Unrecognized option"
	    print_usage
	    exit 1
	    ;;
    esac
done

if [ Dflag ]; then
    vsflags="-do ${macrofile} ${vsflags}"
fi

## Need to run vsim from the obj directory
cd $objdir
vsim -gui -view dataset="${dumpfile}" ${vsflags} &
sleep 5
cd $OLDPWD
echo "Done"
exit 0
