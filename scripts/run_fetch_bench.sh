#! /usr/bin/env bash

demo_num=${1:-demo1}
script_loc=$(dirname $0)
demo_loc=${script_loc}/../project/${demo_num}/verilog
tests_loc=${script_loc}/../tests

for file in $(cat ${tests_loc}/fetch_bench_list); do
	dep_files="${dep_files:-""} ${demo_loc}/${file}"
done

wsrun.pl -prog ${tests_loc}/p1-1.asm fetch_bench ${demo_loc}/fetch_bench.v ${dep_files}
