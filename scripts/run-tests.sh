#! /usr/bin/env bash

## CS552 Spring 2023
## Project Group 8
## Tyler J. Anderson and Ryuki Koda
##
## Script to run assembly files or the assembly list
## Usage:
## path/to/script/run-tests.sh [-s] [test-file.asm]

print_usage() {
    echo "Usage:"
    echo "$0 [-spmu] [-1 | -2 | -3] [-S suite-name] [test-file.asm]"
    echo "$0 -l [-S suite-name]"
    echo "$0 -h"
    echo ""
    echo "Run tests on processor demos. If the test-file.asm argument is"
    echo "supplied, the provided .asm test will be compiled and run on the"
    echo "demo processor. If the argument is not supplied all tests in the"
    echo "all.list for the suite will be run"
    echo ""
    echo "Options:"
    echo "-s             Silent mode, don't append assembly to output"
    echo "-p             Run for pipelined processors"
    echo "-m             Use tests from mytests in local tree"
    echo "-1 | -2 | -3   Select demo1, demo2 or demo3. Demo1 is default"
    echo "               except in pipelined mode (-p), demo2 is default"
    echo "-u             Allow unaligned accesses"
    echo "-l             List available suites, or .asm in suite if the -S"
    echo "               or -m flags are given"
    echo "-S suite-name  Select the test suite to use. Default is inst-tests"
}

scriptdir=${PWD}/$(dirname $0)
rootdir=${scriptdir}/..
objdir=$rootdir/obj
coursedir=/u/s/i/sinclair/public/html/courses/cs552/spring2023
binsdir=${coursedir}/handouts/bins
testsdir=${TESTSDIR:-${coursedir}/handouts/testprograms/public}
benchmod=proc_hier_bench
oldtrace=$objdir/archsim.trace
newtrace=$objdir/verilogsim.trace
sflag=0
listname=${LISTNAME:-all}
demo=demo3

wsrun=$binsdir/wsrun.pl
wsflags=${WSFLAGS:-""}

## Parse command line options
while getopts "spmS:123ulh" opts; do
    case $opts in
	s)
	    sflag=1
	    ;;
	p)
	    pflag=1
	    ;;
	m)
	    mflag=1
	    ;;
	S)
	    suite=${OPTARG}
	    ;;
	1)
	    demo="demo1"
	    ;;
	2)
	    demo="demo2"
	    ;;
	3)
	    demo="demo3"
	    ;;
	u)
	    uflag=1
	    ;;
	l)
	    lflag=1
	    ;;
	h)
	    print_usage
	    exit 0
	    ;;
	\?)
	    echo "Unrecognized option"
	    print_usage
	    exit 0
	    ;;
    esac
done

## Get the argument
while [ $OPTIND -gt 1 ]; do
    shift 1
    OPTIND=$(($OPTIND - 1))
done

asmfile=${1:-""}

## Use pflag for demo 2 and demo 3
if [ "${demo}" != demo1 ]; then
    pflag=1
fi

## Allow unaligned accesses if -u
if [ ! $uflag ]; then
    wsflags="-align ${wsflags}"
fi

## Overrides for pipeline tests
if [ $pflag ]; then
    wsflags="-pipe ${wsflags}"
    demo=${demo:-demo3}
    benchmod=proc_hier_pbench
    oldtrace=$objdir/archsim.ptrace
    newtrace=$objdir/verilogsim.ptrace
fi

## Use the requested demo directory, or default to demo1
srcdir=$rootdir/project/${demo:-"demo1"}/verilog

## If a directory with tests is not given, default to inst_tests
testsdir=${testsdir}/${suite:-inst_tests}

## Use tests in local tree instead
if [ $mflag ]; then
    testsdir=$rootdir/project/${demo:-"demo2"}/verification/mytests
fi

## Print list and exit if lflag
if [ $lflag ]; then
    if [[ $suite || $mflag ]]; then
	ls $testsdir | grep -E '.asm$'
    else
	ls $testsdir/..
    fi

    exit 0
fi

mkdir -p $objdir
cd $objdir

if [ "${asmfile}" != "" ]; then
    $wsrun $wsflags -prog ${testsdir}/"${asmfile}" ${benchmod} ${srcdir}/*.v

    ## Don't display src and color diff if silent
    if [ $sflag -eq 0 ]; then
	cat ${testsdir}/"${asmfile}"
	diff -yw --color $oldtrace $newtrace
    fi
else
    ## Run all tests if no argument and save log so it won't be overwritten
    $wsrun $wsflags -list ${testsdir}/${listname}.list ${benchmod} ${srcdir}/*.v | \
	tee $objdir/$(date "+%Y%m%d-%H_%M-all.log")
fi
