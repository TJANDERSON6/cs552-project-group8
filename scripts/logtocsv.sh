#! /usr/bin/env bash

logdir=${1:-obj/verify/tmp_testing/demo3/verification/results}

echo "suite,testname,result,cpi,cycles,icount,ihitrate,dhitrate"
cat ${logdir}/*.log | grep 'SUCCESS' | sed -E 's/.*\/([[:alnum:]_.-]+)\/([[:alnum:]_.-]+\.asm) ([[:upper:]-]+) CPI:([[:digit:].]+) CYCLES:([[:digit:]]+) ICOUNT:([[:digit:]]+) IHITRATE: ([[:digit:].]+) DHITRATE: ([[:digit:].]+)/\1,\2,\3,\4,\5,\6,\7,\8/'
