#! /usr/bin/env bash

## Script for packaging demo2 for submission

coursedir=/u/s/i/sinclair/public/html/courses/cs552/spring2023/handouts
scriptdir=$PWD/$(dirname $0)
gitroot=$scriptdir/..
objdir=$gitroot/obj
prjarchive=$objdir/prj.tar
archivedemodir=$objdir/prj/project/cache
demodir=$objdir/cache
demosrc1=$objdir/cache/cache_direct/verilog
demosrc2=$objdir/cache/cache_assoc/verilog
tarname=cache_demo.tgz

echo "Building Package hierarchy"
cd $gitroot
mkdir -p $objdir

## Clear any old files
rm -Rf $objdir/prj.tar $objdir/prj $objdir/cache \
   $objdir/summary.log $objdir/vcheck.log $objdir/$tarname

## Sanitize output by extracting archive produced by git
git archive -o $prjarchive --prefix prj/ HEAD
cd $objdir
tar -xf $prjarchive
cp -R $archivedemodir $demodir

if [ $? -eq 0 ]; then
    echo "Package hierarchy expanded at $demodir"
else
    echo "Failed to build package hierarchy"
    exit 1
fi

rules_check()
{
    ## Next need to do the convention checks
    echo "Checking files with vcheck (Output in obj/vcheck.log)"
    cd $1
    vcheck-all.sh | tee $objdir/vcheck.log

    echo "Performing names convention check (Output in obj/names.log)"
    name-convention-check | tee $objdir/vcheck.log
}

rules_check $demosrc1
rules_check $demosrc2

echo "Ready to run tests? Enter to continue"
read

## Package tarball
cd $objdir
tar -czf $tarname cache

## Verify submission
echo "Verifying submission"
rm -Rf $objdir/verify
mkdir -p $objdir/verify
cp $objdir/$tarname $objdir/verify/
cd $objdir/verify
$coursedir/scripts/project/phase2.3/verify_submission_format.sh $tarname

echo "Done"
