#! /usr/bin/env bash

## Script to print results of tests performed during packaging

scriptdir=$(dirname $0)
rsltdir=$scriptdir/../obj/verify/tmp_testing/demo3/verification/results

## Early exit if no results
if [ ! -d $rsltdir ]; then
    exit 1
fi

## Print only failing lines
cat ${rsltdir}/*.log | grep -v SUCCESS

## Stats
totalnum=$(cat ${rsltdir}/*.log | wc -l)
passed=$(cat ${rsltdir}/*.log | grep -c SUCCESS)
echo "Passed: ${passed}, out of: ${totalnum}"
