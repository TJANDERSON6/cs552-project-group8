#! /usr/bin/env bash

## Script for packaging demo2 for submission

coursedir=/u/s/i/sinclair/public/html/courses/cs552/spring2023/handouts
scriptdir=$PWD/$(dirname $0)
gitroot=$scriptdir/..
objdir=$gitroot/obj
prjarchive=$objdir/prj.tar
archivedemodir=$objdir/prj/project/demo2
demodir=$objdir/demo2
demoresults=$objdir/demo2/verification/results
demotests=$objdir/demo2/verification/mytests
demosrc=$objdir/demo2/verilog

echo "Building Package hierarchy"
cd $gitroot
mkdir -p $objdir

## Clear any old files
rm -Rf $objdir/prj.tar $objdir/prj $objdir/demo2 \
   $objdir/summary.log $objdir/vcheck.log

## Sanitize output by extracting archive produced by git
git archive -o $prjarchive --prefix prj/ HEAD
cd $objdir
tar -xf $prjarchive
cp -R $archivedemodir $demodir

if [ $? -eq 0 ]; then
    echo "Package hierarchy expanded at $demodir"
else
    echo "Failed to build package hierarchy"
    exit 1
fi

## Next need to do the convention checks
echo "Checking files with vcheck (Output in obj/vcheck.log)"
cd $demosrc
vcheck-all.sh | tee $objdir/vcheck.log

echo "Performing names convention check (Output in obj/names.log)"
name-convention-check | tee $objdir/vcheck.log

echo "Ready to run tests? Enter to continue"
read

## Run the local tests
echo "Running the local tests"
cd $gitroot
scripts/run-tests.sh -pm
cp $objdir/summary.log $demoresults/mytests.summary.log
echo "Local test complete"

## Run course tests
echo "Running almost all tests"
cd $archivedemodir/verilog
run-phase2-almostAll.sh
mv *.summary.log $demoresults/

## Create timeline pdf
echo "Creating timeline pdf"
cd $archivedemodir/verification
pandoc -i instruction_timeline.org -o instruction_timeline.pdf
mv instruction_timeline.pdf $demodir/verification/

## Package tarball
cd $objdir
tar -czf demo2.tgz demo2

## Verify submission
echo "Verifying submission"
rm -Rf $objdir/verify
mkdir -p $objdir/verify
cp $objdir/demo2.tgz $objdir/verify/
cd $objdir/verify
$coursedir/scripts/project/phase2/verify_submission_format.sh demo2.tgz

echo "Done"
