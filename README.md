# CS552 Spring 2023 Group 8 Project

## Directories

The root directory containes the project instructions document with
the ISA specifications of the WISC-SP23 processor.

/project contains the unpacked project template provided by the
instructor.

/isa includes the documentation for how the processor applies the
WISC-SP23 ISA.

## Contributing Guidelines

Each contributor should create topic branches for each topic they
are working on. Each contributor can have a permanent dev branch
that tracks the _Main_ branch. All contributors must review and
approve any merges to the _Main_ branch, likely after a
successful pull into a contributor's _Main_ - tracking branch.
